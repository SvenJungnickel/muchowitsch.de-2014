<?
// Einbinden der Conig
include("../config/config.php");

// Einbinden der Klasse
include("class.gmapper.php");

// Neue Instanz der Klasse erzeugen
$karte = new gmap($googlemapsapi_key);
?>
<!-- Folgendes ist wichtig, um Google-Map im Internet Explorer richtig darstellen zu k�nnen-->
<html xmlns:v="urn:schemas-microsoft-com:vml">
<head>
	<?
	//	Ausf�hren der Funktion headjs() im HEAD-Bereich der Seite
	//	Sie ben�tigt als Argument den Google Maps API Key
	$karte->headjs();
	?>
</head>
<body onUnload="GUnload()" style="margin: 0px;">
<div align="center">
	<? 
	// Ausgabe des HTML-Element f�r die Karte mit Gr��enangaben. Hier wird die Karte sp�ter angezeigt.
	if($_REQUEST['width'] != '' && is_numeric($_REQUEST['height']) && $_REQUEST['height'] != '' && is_numeric($_REQUEST['height'])) $karte->mapdiv($_REQUEST['width'], $_REQUEST['height']);
	else $karte->mapdiv('700', '500');
	?>
</div>
<?
if($_REQUEST['address'] != '') {
	
	// Koordinaten ermitteln
	$point = $karte->getGeoPoint($_REQUEST['address']);

	// Dann den Javascript-Body Laden
	$karte->bodyjs();
	
	// Dann die Karte erstellen
	$karte->map(14, $point[0], $point[1], "normal", 10, 17, "large", 1, 0, 1, 1, 1, 1);
	
	// Und zum Schluss den Marker setzen
	$karte->markstart();
	$karte->marker($point[0], $point[1]);
	$karte->markend();
}
?>
</body>
</html>