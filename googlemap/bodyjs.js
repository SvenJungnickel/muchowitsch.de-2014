var gmarkers = [];
var htmls = [];
var punkt_lat = [];
var punkt_lng = [];
var punkt_ini_zoom = [];
var i = 0;

function openwin(id){
gmarkers[id].openInfoWindowHtml(htmls[id]);
}

function drawCircle(point,radius,color,line,image,fill){
    var zoom = map.getZoom();

    var centerPt = map.fromLatLngToDivPixel(point);
   // var radiusPt = map.fromLatLngToDivPixel(rpoint);
 	var radiusPt = centerPt;
	rpt1 = radiusPt.x + radius;
	rpt2 = radiusPt.y + radius;
radiusPt = new GPoint(rpt1,rpt2)

	var circlePoints = Array();
    with (Math) {
        radius = floor(sqrt(pow((centerPt.x-radiusPt.x),2) + pow((centerPt.y-radiusPt.y),2)));

        for (var a = 0 ; a < 361 ; a+=10 ) {
            var aRad = a*(PI/180);
            var y = centerPt.y + radius * sin(aRad)
            var x = centerPt.x + radius * cos(aRad)
            var p = new GPoint(x,y);
			var mypt = map.fromDivPixelToLatLng(p, zoom);
            circlePoints.push(mypt);
			if(image != "")
			{
			var mark = createMapMarker(mypt,image);
			map.addOverlay(mark);
			}
	    }
		if(fill != "")
		{
		circleLine = new GPolygon(circlePoints,color,line,0.8,fill,0.5);
        }
        else
        {
        circleLine = new GPolygon(circlePoints,color,line,0.8);
        }
		map.addOverlay(circleLine);
    }
}
function createMarker(point, image)
{
	var icon = new GIcon();
	icon.image = image[0];
	icon.iconSize = new GSize(image[1], image[2]);
	subt = image[2] / 100 * 10;
	subt = Math.ceil(subt);
	var anchor = image[2] - subt;
	icon.iconAnchor= new GPoint(10, anchor);
	//{icon: icon, draggable: true}
	var marker = new GMarker(point,icon);

	return marker;
}

function createMapMarker(point,image)
{
	var icon = new GIcon();
	icon.image = image[0];
	icon.iconSize = new GSize(image[1], image[2]);
	subt = image[2] / 100 * 10;
	subt = Math.ceil(subt);
	var anchor = image[2] - subt;
	icon.iconAnchor= new GPoint(10, anchor);
	var infoanchor = image[2] / 3;
	infoanchor = Math.floor(infoanchor);
	icon.infoWindowAnchor = new GPoint(infoanchor, 2);
	var marker = new GMarker(point,icon);
	GEvent.addListener(marker, "click", function() {map.showMapBlowup(marker.getPoint());});
	return marker;
}


function createTabMarker(point, tabs,image)
{
	var icon = new GIcon();
	icon.image = image[0];
	icon.iconSize = new GSize(image[1], image[2]);
	subt = image[2] / 100 * 11;
	subt = Math.ceil(subt);
	var anchor = image[2] - subt;
	icon.iconAnchor= new GPoint(10, anchor);
	var infoanchor = image[2] / 3;
	icon.infoWindowAnchor = new GPoint(infoanchor, 2);
	var marker = new GMarker(point,icon);
	GEvent.addListener(marker, "click", function() {
	  marker.openInfoWindowTabsHtml(tabs);
	});
	return marker;
}

function createLinkMarker(point, link,image)
{
	var icon = new GIcon();
	icon.image = image[0];
	icon.iconSize = new GSize(image[1], image[2]);
	subt = image[2] / 100 * 11;
	subt = Math.ceil(subt);
	var anchor = image[2] - subt;
	icon.iconAnchor= new GPoint(10, anchor);
	var infoanchor = image[2] / 3;
	icon.infoWindowAnchor = new GPoint(infoanchor, 2);
	var marker = new GMarker(point,icon);

	GEvent.addListener(marker, "click", function() {
	  window.location.href = link;
	});
	return marker;
}

function createOtherMarker(point, number,image)
{
	var icon = new GIcon();
	icon.image = image[0];
	icon.iconSize = new GSize(image[1], image[2]);
	subt = image[2] / 100 * 11;
	subt = Math.ceil(subt);
	var anchor = image[2] - subt;
	icon.iconAnchor= new GPoint(10, anchor);
	var infoanchor = image[2] / 3;
	infoanchor = Math.floor(infoanchor);
	icon.infoWindowAnchor = new GPoint(infoanchor, 2);
	var marker = new GMarker(point,icon);
	var html = number;
	GEvent.addListener(marker, "click", function() {marker.openInfoWindowHtml(html);});

	return marker;
}

      var arrowIcon = new GIcon();
      arrowIcon.iconSize = new GSize(24,24);
      arrowIcon.shadowSize = new GSize(1,1);
      arrowIcon.iconAnchor = new GPoint(12,12);
      arrowIcon.infoWindowAnchor = new GPoint(0,0);
      var degreesPerRadian = 180.0 / Math.PI;
      function bearing( from, to ) {
        var lat1 = from.latRadians();
        var lon1 = from.lngRadians();
        var lat2 = to.latRadians();
        var lon2 = to.lngRadians();

        var angle = - Math.atan2( Math.sin( lon1 - lon2 ) * Math.cos( lat2 ), Math.cos( lat1 ) * Math.sin( lat2 ) - Math.sin( lat1 ) * Math.cos( lat2 ) * Math.cos( lon1 - lon2 ) );
        if ( angle < 0.0 )
	 angle  += Math.PI * 2.0;

        angle = angle * degreesPerRadian;
        angle = angle.toFixed(1);

        return angle;
      }

      function arrowHead(points) {
        var p1=points[points.length-1];
        var p2=points[points.length-2];
        var dir = bearing(p2,p1);
        var dir = Math.round(dir/3) * 3;
        while (dir >= 120) {dir -= 120;}
        arrowIcon.image = "/img/dir_"+dir+".png";
        map.addOverlay(new GMarker(p1, arrowIcon));
      }

      function midArrows(points) {
        for (var i=1; i < points.length-1; i++) {
          var p1=points[i-1];
          var p2=points[i+1];
          var dir = bearing(p1,p2);

          var dir = Math.round(dir/3) * 3;
          while (dir >= 120) {dir -= 120;}
          arrowIcon.image = "/img/dir_"+dir+".png";
          map.addOverlay(new GMarker(points[i], arrowIcon));
        }
      }

			function geocodeAddress(address,typ,marker,zoom) {
  geocoder.getLatLng(
    address,
    function(point) {
      if (!point) {
        alert(address + " nicht gefunden");
      } else {
	 var lat = point.lat();
	 var lng = point.lng();
	 map.setCenter(point,zoom);
	 if(typ == "otherMarker")
	 {
	marker = createOtherMarker(point,address,marker);
	}
	else if(typ == "marker")
	{
	marker = createMarker(point,marker);
	}
	else if(typ == "mapMarker")
	{
	marker = createMapMarker(point,marker);
	}
	map.addOverlay(marker);
		}
    }
  );
}

	function getAdress()
	{
		var adress = document.getElementById('adresse').value;
		return adress;
	}

	function tilechk()
	{
		paragraphs = map.getContainer().getElementsByTagName('p').length;
		if(paragraphs>4)
		{
		map.zoomOut(false,true);
		}
	}
	function wheelevent(e)
    {
        if (!e) e = window.event;
        if (e.preventDefault) e.preventDefault();
        e.returnValue = false;
    }