<?
// Einbinden der Conig
include("../config/config.php");
?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
        html { height: 100% }
        body { height: 100%; margin: 0; padding: 0 }
        #map_canvas { height: 100% }
    </style>
    <script type="text/javascript"
            src="http://maps.googleapis.com/maps/api/js?key=<?=$googlemapsapi_key?>&sensor=true">
    </script>
    <script type="text/javascript">
        var geocoder;
        var map;

        // Adresse per Geocoder ermitteln
        function codeAddress(address) {
            geocoder = new google.maps.Geocoder();
            geocoder.geocode( { 'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location
                    });
                    return results[0].geometry.location;
                } else {
                    alert("Geocode was not successful for the following reason: " + status);
                    return false;
                }
            });
        }

        function initialize() {
            var mapOptions = {
                center: codeAddress('<?=$_REQUEST['address']?>'),
                zoom: 15,
                disableDefaultUI: false,					// Karten Funktionsbuttons
                draggable: false,							// Karte "greifbar machen"
                zoomControl: true,						    // Karten Zoom
                scrollwheel: false,						    // Karten Zoom per Mausrad
                disableDoubleClickZoom: true,				// Karten Zoom per doppelklick
                mapTypeId: google.maps.MapTypeId.ROADMAP 	// Kartentyp festlegen
            };
            map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
        }
    </script>
</head>
<body onload="initialize();">
<div id="map_canvas" style="width:100%; height:100%"></div>
</body>
</html>