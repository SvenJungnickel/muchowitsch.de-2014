![Muchowitsch.de Logo](http://www.muchowitsch.de//templates/images/muchowitsch-logo.png "Muchowitsch.de Logo")

# Muchowitsch.de 2014

Old version of www.muchowitsch.de from 2014

Developed an own small cms system based on PHP and integrated a bunch of libraries and plugins:

- [jQuery](https://jquery.com/)
- [Smarty](http://www.smarty.net)
- [Google Recaptcha](https://www.google.com/recaptcha)
- [Google Maps](https://developers.google.com/maps/)
- [swfupload](https://sourceforge.net/projects/swfupload.mirror/)
- [phpmailer](https://github.com/PHPMailer/PHPMailer)
- [ckeditor](http://ckeditor.com/)

I also integrated Google Analytics for visitor tracking purposes.
