<?php
/*
 * Funktion zum erstellen einer Mail
 *
 * by Sven Jungnickel
 */

include_once(BASEDIR."/class/phpmailer/Class.Phpmailer.php");

function email($email, $name, $subject, $message, $fromemail = "", $fromname = "") {
	
	global $sendmail_address, $sendmail_name;
	
	$mail = new phpmailer();
	
	// Versänderdaten
	$mail->From     = ($fromemail == "") ? ($sendmail_address == "") ? "info@".str_replace('www.','',$_SERVER['HTTP_HOST']) : $sendmail_address : $fromemail ;
	$mail->FromName = ($fromname == "") ? ($sendmail_name == "") ? "info@".str_replace('www.','',$_SERVER['HTTP_HOST']) : $sendmail_name : $fromname ;
	
	// Empfängerdaten
	$mail->AddAddress($email,$name);
	
	// Betreff
	$mail->Subject  =  $subject;
	
	// HTML-Text
	$mail->IsHTML(true);		
	$mail->Body = $message;
	
	// Attachment anhängen
	if($attachment != '') {
		if(is_array($attachment)) {
			foreach($attachment as $line) {
				$mail->AddAttachment($line, basename($line));
			}
		}
		else {
			$mail->AddAttachment($attachment, basename($attachment));
		}
	}
	
	// Mail versenden
	$mail->Send();
}
?>