<?
/*
 * Funktion zum Löschen des Caches
 *
 * by Sven Jungnickel
 */

function clearcache($path) {

	if(is_dir($path)) {
		if($fp = opendir($path)) {
			while(($file = readdir($fp)) !== false) {
				if($file != '.' && $file != '..') unlink($path.$file);
			}
			closedir($fp);
			return true;
		}
	}
	return false;
}
?>