<?
/*
 * Einbinden aller Funktionen des Verzeichnisses ./functions
 *
 * by Sven Jungnickel
 */
 
if(!defined("BASEDIR")) exit;

$dir = BASEDIR."/functions/";

$dh  = opendir($dir);
while (false !== ($filename = readdir($dh))) {
	if($filename != "." && $filename != ".." && $filename != 'Func.Init.php') {
		include_once($dir.$filename);
	}
}
?>