<?
/*
 * Funktion zur Erzeugung der Seiten-Navigation
 *
 * by Sven Jungnickel
 */

function pagenav_tpl($count_pages, $link, $count_before, $count_after, $admin = 0) {

	global $tmpl, $domainname;
	
	
	if(defined("ADMINDIR")) $tmpl->compile_dir = ADMINDIR.'/temp';
	else $tmpl->compile_dir = BASEDIR.'/temp';
	
	$tmpl->assign('maxsites', $count_pages);
	
	$site = ($_REQUEST['page'] == '') ? 1 : escs($_REQUEST['page']);
	$tmpl->assign('site', $site);
	
	$pages = array();
	if($site > $count_pages-$count_after) $anz = $count_before+($site+$count_after-$count_pages);
	else $anz = $count_before;
	for($i = $anz; $i > 0; $i--) {
		$current_site = $site-$i;
		if($current_site >= 1) {
			$current_link = str_replace("{s}", $current_site, $link);
			array_push($pages, array('current_site' => $current_site, 'link' => $current_link));
		}
	}
	array_push($pages, array('current_site' => $site, 'link' => ''));
	$anz = $count_after+$count_before-count($pages)+1;
	for($i = 1; $i <= $anz; $i++) {
		$current_site = $site+$i;
		if($current_site <= $count_pages) {
			$current_link = str_replace("{s}", $current_site, $link);
			array_push($pages, array('current_site' => $current_site, 'link' => $current_link));
		}
	}
	$tmpl->assign('pages', $pages);
	
	$tmpl->assign('link_first', str_replace("{s}", 1, $link));
	$tmpl->assign('link_next', str_replace("{s}", $site+1, $link));
	$tmpl->assign('link_prev', str_replace("{s}", $site-1, $link));
	$tmpl->assign('link_last', str_replace("{s}", $count_pages, $link));
	
	$tmpl->assign('domainname', $domainname);
	
	// Seiten-Navigation nicht cachen
	$tmpl->clear_cache('pagenav.tpl');
    return $tmpl->fetch("pagenav.tpl");
}
?>