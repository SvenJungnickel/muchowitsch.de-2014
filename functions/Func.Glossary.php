<?
/*
 * Funktion für Glossareinträge
 *
 * by Sven Jungnickel
 */

// Glossar-Übersicht anzeigen
function showGlossary() {
	global $db, $tmpl, $domainname;
	
	// Array aller Buchstaben erstellen
	$alphabet = array('Alle');
	for($i = 'A'; $i < 'Z'; $i++) {
		array_push($alphabet, $i);
	}
	array_push($alphabet, 'Z');
	$tmpl->assign('alphabet', $alphabet);
	
	// Sortieren
	if($_REQUEST['sort'] != '' && $_REQUEST['sort'] != 'Alle' && $_REQUEST['sort'] != 'ALLE') {
		$andsort = " AND UPPER(title) LIKE '".strtoupper(escs($_REQUEST['sort']))."%'";
	}
	
	// Anzahl Glossareinträge für Navigation ermitteln
	$sql_num = $db->Query("SELECT id FROM ".PREFIX."_glossary WHERE active = 1 $andsort");
	$num = $sql_num->numrows();
	$tmpl->assign('num', $num);
	
	$limit = ($_REQUEST['pp'] == '') ? 20 : escs($_REQUEST['pp']);
	$page = ($_REQUEST['page'] == '') ? 1 : escs($_REQUEST['page']);
	$a = $page * $limit - $limit;
	
	// Navigation erzeugen
	if($num > $limit) {
		$link = $domainname."index.php?p=page&amp;id=".$_REQUEST['id']."&amp;pp=".$limit."&amp;page={s}&amp;sort=".escs($_REQUEST['sort']);
		$seiten = ceil($num / $limit);
		$tmpl->assign('navi', pagenav_tpl($seiten, $link, 4, 4, 1));
	}
	
	// Glossareinträge ermitteln
	$sql = $db->Query("SELECT * FROM ".PREFIX."_glossary WHERE active = 1 $andsort ORDER BY title ASC LIMIT $a, $limit");
	$glossary = array();
	while($row = $sql->fetchrow()) {
		// Nach Anfangsbuchstaben gruppieren
		$firstletter = substr(strtoupper($row->title),0,1);
		if($firstletter != $fist_old) $row->firstletter = 1;
		$fist_old = $firstletter;
		
		array_push($glossary, $row);
	}
	$tmpl->assign('glossary', $glossary);

	return $tmpl->fetch("glossary.tpl");
}

// Text nach Glossareinträgen durchsuchen und markieren für Infotext
function parseGlossary($text) {
	global $db, $tmpl, $domainname;
		
	$sql = $db->Query("SELECT * FROM ".PREFIX."_glossary WHERE active = 1");
	$glossary = array();
	while($row = $sql->fetchrow()) {
		$row->descr = str_replace(chr('10'),'|',$row->descr);
		#$text = str_ireplace($row->title, '<a href="index.php?p=page&amp;id=41&amp;sort='.$row->title.'" class="glossary" style="background-color: #FF9" title="'.$row->title.'|'.$row->descr.'">'.$row->title.'</a>', $text);
		$text = preg_replace('/'.$row->title.'/', '<a href="'.$domainname.'index.php?p=page&amp;id=41&amp;sort='.$row->title.'" class="glossary" style="background-color: #FF9" title="'.$row->title.'|'.$row->descr.'">'.$row->title.'</a>', $text, 1);
	}	
	
	return $text;
}
?>