<?php
/*
 * Funktion zum Maskieren eines Strings zur Benutzung in mysql_query
 *
 * by Sven Jungnickel
 */
 
function escs($text) {
    #if(function_exists('mysql_real_escape_string')) return mysql_real_escape_string($text);
	#else return mysql_escape_string($text);
	return mysql_escape_string($text);
} 
?>