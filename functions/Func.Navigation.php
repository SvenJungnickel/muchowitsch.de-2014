<?
/*
 * Funktion zur Erzeugung der Navigation
 *
 * by Sven Jungnickel
 */


function navigation() {
	global $db, $tmpl;

	// Navigationsinhalte ermitteln
	$sql = $db->Query("SELECT id, title, link FROM ".PREFIX."_navigation WHERE parent = 0 AND display = 1 AND active = 1 ORDER BY pos ASC");
	$navigation = array();
	while($row = $sql->fetchrow()) {
		// Unternavigation ermitteln
		$row->subnavi = subnavi($row->id);		
		
		// Überprüfen, welcher Menüpunkt geklickt ist
		/*$link = str_replace('&', '&amp;', $_SERVER['QUERY_STRING']);
		$link = str_replace('p=page&amp;page=', '', $link);
		if($link == $row->link) $row->linked2 = true;*/
		if($_REQUEST['id'] == $row->id) $row->linked2 = true;
		
		// Überprüfen, welche Unternavigation geöffnet bleiben soll
		$row->linked = checklink($row->id);
		
		array_push($navigation, $row);
	}
	$tmpl->assign('navigation', $navigation);
	
	// Test
	#echo '<pre>', print_r($navigation), '</pre>';
	
	// Navigation nicht cachen
	$tmpl->clear_cache('navigation/navigation.tpl');
    return $tmpl->fetch("navigation/navigation.tpl");
}

// Funktion zum Ermitteln der Unternavigation
function subnavi($parent) {
	global $db;
	
	if(!defined("ADMINDIR")) $anddisplay = "AND display = 1 AND active = 1";
	$sql = $db->Query("SELECT * FROM ".PREFIX."_navigation WHERE parent = ".$parent." $anddisplay ORDER BY pos ASC");
	$subnavi = array();
	while($row = $sql->fetchrow()) {		
		// Eigene Funktion rekursiv anwenden
		$row->subnavi = subnavi($row->id);
		
		// Überprüfen, welcher Menüpunkt geklickt ist
		/*$link = str_replace('&', '&amp;', $_SERVER['QUERY_STRING']);
		$link = str_replace('p=page&amp;page=', '', $link);
		if($link == $row->link) $row->linked2 = true;*/
		if($_REQUEST['id'] == $row->id) $row->linked2 = true;
		
		// Überprüfen, welche Unternavigation geöffnet bleiben soll
		$row->linked = checklink($row->id);
		
		// Content kürzen (für Admin)
		$row->content = substr(htmlentities($row->content),0,150);
	
		array_push($subnavi, $row);
	}
	return $subnavi;
}

//  Funktion zum Anzeigen der Unternavigation
function subnavi_show($param) {
	global $db, $tmpl;
	
	$id = $param['id'];
	
	if(!defined("ADMINDIR")) {
		$anddisplay = "AND display = 1 AND active = 1";
		$template = "navigation/subnavi.tpl";
	}
	else {
		if($_REQUEST['action'] == 'overview') $template = "pages/subpages.tpl";
		else $template = "pages/subnaviform.tpl";
	}
	
	$sql = $db->Query("SELECT * FROM ".PREFIX."_navigation WHERE id = ".$id." $anddisplay ORDER BY pos ASC");
	$row = $sql->fetchrow();
	
	// Unternavigation ermitteln
	$row->subnavi = subnavi($id);
	
	// Überprüfen, welche Unternavigation geöffnet bleiben soll
	$row->linked = checklink($row->id);
		
	$tmpl->assign('subnavi', $row);
	
	$tmpl->clear_cache($template);
	return $tmpl->fetch($template);
}

// Funktion zum Überprüfen, welche Unternavigation geöffnet bleiben soll
function checklink($id) {
	global $db;
		
	if($id != '' && $id != 0 && isset($_REQUEST['id']) && $_REQUEST['id'] != '' && $_REQUEST['id'] != 0 && is_numeric($_REQUEST['id'])) {
		/*$link = str_replace('&', '&amp;', $_SERVER['QUERY_STRING']);
		$link = str_replace('p=page&amp;page=', '', $link);*/

		#$sql = $db->Query("SELECT * FROM ".PREFIX."_navigation WHERE (parent = ".$id." OR id = ".$id.") AND link = '".$link."' AND active = 1");
		$sql = $db->Query("SELECT * FROM ".PREFIX."_navigation WHERE id = ".$id." AND id = ".escs($_REQUEST['id'])." AND active = 1");
		if($sql->numrows() > 0) {
			return true;
		}
		else {
			// Überprüfen, ob der Link in einem Untermenü bestehen könnte
			$sql_sub = $db->Query("SELECT id FROM ".PREFIX."_navigation WHERE parent = ".$id);
			while($row_sub = $sql_sub->fetchrow()) {
				$return = checklink($row_sub->id);
				if($return) return true;
			}
		}
	}
	else return false;
}

// Funktion zum Erzeugen der Krümelpfad-Navigation
function navipath($id, &$array) {
	global $db;
	
	$sql = $db->Query("SELECT id, title, parent FROM ".PREFIX."_navigation WHERE id = ".$id);
	$row = $sql->fetchrow();	
	#array_push($array, array('title' => $row->title, 'link' => $row->link));
	array_push($array, array('id' => $row->id, 'title' => $row->title));
	if($row->parent != 0) {
		navipath($row->parent, $array);
	}

	return $path;
}

// Funktion zum Anzeigen der Krümelpfad-Navigation
/*function navipath_show() {
	global $db, $tmpl;
	
	if($_REQUEST['p'] != '') {		
		$link = str_replace('&', '&amp;', $_SERVER['QUERY_STRING']);
		$link = str_replace('p=page&amp;page=', '', $link);
		
		$sql = $db->Query("SELECT title, parent FROM ".PREFIX."_navigation WHERE link = '".$link."'");
		$row = $sql->fetchrow();
		$path = array(array('title' => $row->title));
		if($row->parent != 0) {
			navipath($row->parent, $path);
		}
		arsort ($path);
		
		// Test
		#echo '<pre>', print_r($path), '</pre>';
		
		if(count($path) > 1) $tmpl->assign('path', $path);
	}
	
	$tmpl->clear_cache('navigation/navipath.tpl');
	return $tmpl->fetch("navigation/navipath.tpl");
}*/
?>