<?
/*
 * Funktion für Presse und Medieninhalte
 *
 * by Sven Jungnickel
 */

// Presse-Übersicht anzeigen
function showPrintMedia() {
	global $db, $tmpl, $domainname;

	// Anzahl Presseeinträge für Navigation ermitteln
	$sql_num = $db->Query("SELECT id FROM ".PREFIX."_printmedia WHERE active = 1 $andsort");
	$num = $sql_num->numrows();
	$tmpl->assign('num', $num);
	
	$limit = ($_REQUEST['pp'] == '') ? 20 : escs($_REQUEST['pp']);
	$page = ($_REQUEST['page'] == '') ? 1 : escs($_REQUEST['page']);
	$a = $page * $limit - $limit;
	
	// Navigation erzeugen
	if($num > $limit) {
		$link = $domainname."index.php?p=page&amp;id=".$_REQUEST['id']."&amp;pp=".$limit."&amp;page={s}&amp;sort=".escs($_REQUEST['sort']);
		$seiten = ceil($num / $limit);
		$tmpl->assign('navi', pagenav_tpl($seiten, $link, 4, 4, 1));
	}
	
	// Presseeinträge ermitteln
	$sql = $db->Query("SELECT * FROM ".PREFIX."_printmedia WHERE active = 1 ORDER BY date DESC LIMIT $a, $limit");
	$printmedia = array();
	while($row = $sql->fetchrow()) {
		// Link kürzen
		if($row->link != '') {
			$tmp = explode('/', $row->link);
			$row->shortlink = $tmp[2];
		}
		array_push($printmedia, $row);
	}
	$tmpl->assign('printmedia', $printmedia);

	return $tmpl->fetch("printmedia/overview.tpl");
}
?>