<?
/*
 * Funktion zum Anzeigen der Seminartermine
 *
 * by Sven Jungnickel
 */

// nächsten Termine auf der Startseite anzeigen
function showNextSeminars($param) {
	global $db, $tmpl, $domainname;
	
	$limit = ($param['limit'] == '') ? 3 : $param['limit'];
	
	// nächsten Seminare ermitteln
	$sql = $db->Query("SELECT * FROM ".PREFIX."_seminars WHERE active = 1 AND DATE_FORMAT(FROM_UNIXTIME(time_end),'%Y%m%d') >= ".strftime('%Y%m%d')." ORDER BY time_start ASC LIMIT $limit");
	$seminars = array();
	while($row = $sql->fetchrow()) {
		// Stadt und Bundesland ermitteln
		$sql_city = $db->Query("SELECT c.title AS city, s.title AS state FROM ".PREFIX."_seminars_city AS c JOIN ".PREFIX."_seminars_state AS s ON (s.id = c.sid) WHERE c.id = ".$row->cid);
		$row_city = $sql_city->fetchrow();
		$row->city = $row_city->city.' ('.$row_city->state.')';
					
		// Typ ermitteln
		$sql_type = $db->Query("SELECT title, shorttitle FROM ".PREFIX."_seminars_type WHERE id = ".$row->tid);
		$row_type = $sql_type->fetchrow();
		if($row_type->shorttitle != '') $row->type = $row_type->shorttitle;
		else $row->type = $row_type->title;
		
		array_push($seminars, $row);
	}
	$tmpl->assign('seminars', $seminars);

	return $tmpl->fetch("seminars/show_next_seminars.tpl");
}

function showSeminars() {
	global $db, $tmpl, $domainname;
	
	// Alle Bundesländer ermitteln
	$sql = $db->Query("SELECT * FROM ".PREFIX."_seminars_state ORDER BY title ASC");
	$states = array();
	while($row = $sql->fetchrow()) {
		// Alle Städte ermitteln
		$sql_cities = $db->Query("SELECT * FROM ".PREFIX."_seminars_city WHERE sid = ".$row->id." ORDER BY title ASC");
		$cities = array();
		$num_seminars = 0;
	
		while($row_cities = $sql_cities->fetchrow()) {
			// Alle Seminare der Stadt, gruppiert nach Seminartyp ermitteln
			$sql_seminars = $db->Query("SELECT * FROM ".PREFIX."_seminars AS s JOIN ".PREFIX."_seminars_type AS t ON (s.tid = t.id) WHERE s.cid = ".$row_cities->id." AND s.active = 1 AND DATE_FORMAT(FROM_UNIXTIME(s.time_end),'%Y%m%d') >= ".strftime('%Y%m%d')." ORDER BY t.title ASC, s.time_start ASC");
			$seminars = array();
			$type = '';
			$tmp_type = '';
			$num_seminars2 = 0;
			while($row_seminars = $sql_seminars->fetchrow()) {
				// Nach Seminartyp gruppieren
				$type = $row_seminars->title;
				if($type != $tmp_type) $row_seminars->showtype = 1;
				$tmp_type = $type;
				
				array_push($seminars, $row_seminars);
				$num_seminars++;
				$num_seminars2++;
			}
			
			$row_cities->seminars = $seminars;
			if($num_seminars > 0 && $num_seminars2 > 0) array_push($cities, $row_cities);
		}
		$row->cities = $cities;
		$row->num_seminars = $num_seminars;
		
		if($num_seminars > 0) array_push($states, $row);
	}
	$tmpl->assign('states', $states);
	
	return $tmpl->fetch("seminars/overview.tpl");
}
?>