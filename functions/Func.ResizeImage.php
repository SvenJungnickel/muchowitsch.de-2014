<?
// =====================================================
// Funktionen zum berechnen der Neuen Bildgröße 
//
// Funktionsweise: es wird eine JPG-Grafik generiert, egal
// welcher Dateityp das Originalbild hat
//
// $img = Dateiname des Bildes
// $path_ori = alter Dateipfad des zu bearbeitenden Bildes
// $path_images = neuer Dateipfad des neuberechneten Bildes
// $x_size = Breite des neuen Bildes
// $y_size = Höhe des neuen Bildes
// $quali = Qualität des neuen Bildes 
// $output = Wenn auf 0 gesetzt wird das Bild nur gespeichert, 
//			 wenn auf 1 gesetzt wird das neue Bild abgespeichert und zurückgegeben, 
// 			 wenn auf 2 gesetzt wird nur das Bild zurückgegeben, ohne es abzuspeichern
// ==============================


function resize_img($img, $path_ori, $path_images, $x_size, $y_size, $quali , $output = 0)
{
	$file = $path_ori.$img;
	$infos = @getimagesize($file);
	if($infos[2] == 2) {
		// jpeg
		$src_img =  @imagecreatefromjpeg($file);
	} elseif($infos[2] == 3) {
		// png
		$src_img = @imagecreatefrompng($file);
	} elseif($infos[2] == 1) {
		// gif
		$src_img = @imagecreatefromgif($file);
	}
	if ($src_img != '')
	{
		$imageinfo = @getimagesize($file);
		$new_w=$imageinfo[0];
		$new_h=$imageinfo[1];
		while ($new_w>=$x_size || $new_h>=$y_size){
			$new_w=$new_w*0.99;
			$new_h=$new_h*0.99;
		}
		$new_w=@round($new_w); 
		$new_h=@round($new_h);	
		
		$dst_img = @imagecreatetruecolor($new_w,$new_h);
		@imagecopyresampled($dst_img, $src_img, 0, 0, 0, 0, $new_w,$new_h, @imagesx($src_img), @imagesy($src_img));
		
		if($output == 0) {
			// Speichern
			@imagejpeg($dst_img,$path_images.$img,$quali);
			@chmod ($path_images.$img, 0777);	
		}
		elseif($output == 1) {
			// Speichern
			@imagejpeg($dst_img,$path_images.$img,$quali);
			@chmod ($path_images.$img, 0777);
			
			// Ausgeben
			@ob_start();
			@imagejpeg($dst_img,'',$quali);
			$output = @ob_get_contents();
			@ob_end_clean();
			return $output;
		}
		elseif($output == 2) {
			// Ausgeben
			@ob_start();
			@imagejpeg($dst_img,'',$quali);
			$output = @ob_get_contents();
			@ob_end_clean();
			return $output;
		}
	}
}
?>