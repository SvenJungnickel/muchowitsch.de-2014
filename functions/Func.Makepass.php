<?
/*
 * Funktion zur Erzeugung eines 8-stellingen Passwortes
 *
 * by Sven Jungnickel
 */

function makepass($lenght = 0)
{
    $pass = "";
    $chars = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0",
        "a", "A", "b", "B", "c", "C", "d", "D", "e", "E", "f", "F", "g", "G", "h", "H", "i", "I", "j", "J",
        "k", "K", "l", "L", "m", "M", "n", "N", "o", "O", "p", "P", "q", "Q", "r", "R", "s", "S", "t", "T",
        "u", "U", "v", "V", "w", "W", "x", "X", "y", "Y", "z", "Z");
	
	$ch = ($lenght == 0) ? 8 : $lenght;
    $count = count($chars) - 1;
    srand((double)microtime() * 1000000);
    for($i = 0; $i < $ch; $i++) $pass .= $chars[rand(0, $count)];
	
    return($pass);
}
?>