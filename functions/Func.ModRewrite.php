<?php
/*
 * Funktionen f�r ModRewrite Klasse
 *
 * by Sven Jungnickel
 */

// Funktion zum parsen der URL
function parseURL($string) {
	$string = strtolower($string);
	$string = str_replace(' ', '_', $string);
	$string = preg_replace('/[^_a-üA-Ü0-9]/u','', utf8_encode($string));
	return utf8_decode($string);
}

// Funktion zum Umschreiben der URL der Inhaltsseiten
function getPageTitle($id, $table = '') {
	global $db, $prepage;
	$url = '';
	
	if($table != '') {
		$sql = $db->Query("SELECT title FROM ".PREFIX."_".$table." WHERE id = ".$id);
		$row = $sql->fetchrow();
		
		$url = parseURL($row->title);
	}
	else {	
		include_once(BASEDIR.'/functions/Func.Navigation.php');
				
		$path = array();
		navipath($id, $path);
	
		for($i = count($path); $i >= 0; $i--) {
			if($path[$i]['title'] != '') {
				$title = parseURL($path[$i]['title']);
				
				$url.= $title.'/';
				#if($i > 0) $url.= '/';
			}
		}
	}
	
	return $url;
}