<?
/*
 * Startseite
 *
 * by Sven Jungnickel
 */

if(!defined("BASEDIR")) exit;

// Überprüfen, ob Cache vorhanden
if(!$tmpl->is_cached('start.tpl')) {
	
	// Startseitentext aus Datenbank lesen
	$sql_text = $db->Query("SELECT content FROM ".PREFIX."_navigation WHERE id = 1 AND active = 1");
	$row_text = $sql_text->fetchrow();
	$tmpl->assign('text', $row_text->content);
	
	// Aktuelles ermitteln
	$sql = $db->Query("SELECT * FROM ".PREFIX."_news WHERE active = 1 ORDER BY ctime DESC LIMIT 3");
	$news = array();
	while($row = $sql->fetchrow()) {
		// Autoren ermitteln
		$sql_author = $db->Query("SELECT authortoken FROM ".PREFIX."_admin_user WHERE id = ".$row->author);
		$row_author = $sql_author->fetchrow();
		$row->author = $row_author->authortoken;
			
		array_push($news, $row);
	}
	$tmpl->assign('news', $news);
}

$tmpl->assign('content', $tmpl->fetch('start.tpl'));
?>