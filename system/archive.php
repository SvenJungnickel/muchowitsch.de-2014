<?
/*
 * Archiv - Übersicht aller Neuigkeiten
 *
 * by Sven Jungnickel
 */



if (!defined("BASEDIR")) {
    exit;
}

$title = 'Archiv';
$description = 'Im Archiv finden Sie alle auf muchowitsch.de publizierten Artikel in ihrer chronologischen Erscheinungsweise.';
$keywords = 'Archiv, Aktuelles';

switch ($_REQUEST['action']) {
    case '':
    case 'overview':
    default:
        // Überprüfen, ob Cache vorhanden
        if (!$tmpl->is_cached('archive/overview.tpl')) {

            // Anzahl News für Navigation ermitteln
            $sql_num = $db->Query("SELECT id FROM " . PREFIX . "_news WHERE active = 1");
            $num = $sql_num->numrows();
            $tmpl->assign('num', $num);

            $limit = ($_REQUEST['pp'] == '') ? 10 : escs($_REQUEST['pp']);
            $page = ($_REQUEST['page'] == '') ? 1 : escs($_REQUEST['page']);
            $a = $page * $limit - $limit;

            // Navigation erzeugen
            if ($num > $limit) {
                $link = $domainname . "index.php?p=archive&amp;action=overview&amp;pp=" . $limit . "&amp;page={s}";
                $seiten = ceil($num / $limit);
                $tmpl->assign('navi', pagenav_tpl($seiten, $link, 4, 4, 1));
            }

            // News  ermitteln
            $sql = $db->Query(
                "SELECT * FROM " . PREFIX . "_news WHERE active = 1 ORDER BY ctime DESC LIMIT $a, $limit"
            );
            $news = array();
            while ($row = $sql->fetchrow()) {
                // Autoren ermitteln
                $sql_author = $db->Query(
                    "SELECT authortoken FROM " . PREFIX . "_admin_user WHERE id = " . $row->author
                );
                $row_author = $sql_author->fetchrow();
                $row->author = $row_author->authortoken;

                array_push($news, $row);
            }
            $tmpl->assign('news', $news);
        }
        $tmpl->assign('content', $tmpl->fetch('archive/overview.tpl'));
        break;
    case 'details':
        if (isset($_REQUEST['aid']) && $_REQUEST['aid'] != '' && $_REQUEST['aid'] != 0) {
            // News  ermitteln
            $sql = $db->Query(
                "SELECT * FROM " . PREFIX . "_news WHERE active = 1 AND id = " . escs($_REQUEST['aid'])
            );
            if($sql->numrows() > 0) {
                $row = $sql->fetchrow();

                // Titel setzen
                $title = htmlspecialchars($row->title).' &raquo; Archiv';

                // Autor ermitteln
                $sql_author = $db->Query(
                    "SELECT uname FROM " . PREFIX . "_admin_user WHERE id = " . $row->author
                );
                $row_author = $sql_author->fetchrow();
                $row->author = $row_author->uname;

                // Meta Description setzen, wenn leer
                if($row->metadescription == '' ) {
                    $row->metadescription = htmlspecialchars($row->teaser);
                }

                $tmpl->assign('row', $row);

                $tmpl->assign('content', $tmpl->fetch('archive/details.tpl'));
            }else {
                include_once(BASEDIR . '/system/error.php');
            }
        } else {
            include_once(BASEDIR . '/system/error.php');
        }
        break;
}
