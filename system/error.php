<?
/*
 * Fehlerseite
 *
 * by Sven Jungnickel
 */

if(!defined("BASEDIR")) exit;

$title = 'Fehler - Seite nicht gefunden!';

$tmpl->assign('content', $tmpl->fetch('error.tpl'));

