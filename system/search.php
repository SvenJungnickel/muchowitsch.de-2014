<?
/*
 * Suche
 *
 * by Sven Jungnickel
 */

if(!defined("BASEDIR")) exit;

$title = 'Suche';
$description = 'Sie suchen was spezielles zum Thema Unternehmensberatung oder einem anderem Thema? Dann verwenden Sie die Suche auf www.muchowitsch.de';
$keywords = 'Suche, Unternehmensberatung, Unternehmensberater';

if(isset($_REQUEST['search']) && $_REQUEST['search'] != '' && $_REQUEST['search'] != 'Suchbegriff eingeben') {
		
	if(!isset($_REQUEST['modrewrite'])) header('location: '.$domainname.'suche/'.$_REQUEST['search']);
		
	$searchword = utf8_decode($_REQUEST['search']);
	$title.= ' - '.$searchword;
	$tmpl->assign('searchword', $searchword);
		
//	if(strlen($searchword) <= 3) {
		// Abfrage aller Seiten
		$q1 = "SELECT * FROM ".PREFIX."_navigation WHERE (title LIKE '%".$searchword."%' OR content LIKE '%".$searchword."%') AND active = 1";
		// Abfrage aller News
		$q2 = "SELECT * FROM ".PREFIX."_news WHERE (title LIKE '%".$searchword."%' OR teaser LIKE '%".$searchword."%' OR text LIKE '%".$searchword."%' OR topic LIKE '%".$searchword."%') AND active = 1";
	/*}
	else {
		// Abfrage aller Seiten
		#$q1 = "SELECT *, MATCH (title, content) AGAINST ('".$searchword."') AS score FROM ".PREFIX."_navigation WHERE MATCH (title, content) AGAINST ('".$searchword."') AND active = 1 ORDER BY score DESC";
		$q1 = "SELECT *, MATCH (title) AGAINST ('".$searchword."')*2 + MATCH (content) AGAINST ('".$searchword."') AS score FROM ".PREFIX."_navigation WHERE MATCH (title, content) AGAINST ('".$searchword."') AND active = 1 ORDER BY score DESC";
		// Abfrage aller News
		#$q2 = "SELECT *, MATCH (title, teaser, text, topic) AGAINST ('".$searchword."') AS score FROM ".PREFIX."_news WHERE MATCH (title, teaser, text, topic) AGAINST ('".$searchword."') AND active = 1 ORDER BY score DESC";
		$q2 = "SELECT *, MATCH (title) AGAINST ('".$searchword."')*2 + MATCH (teaser, text) AGAINST ('".$searchword."')*1,5 + MATCH (topic) AGAINST ('".$searchword."') AS score FROM ".PREFIX."_news WHERE MATCH (title, teaser, text, topic) AGAINST ('".$searchword."') AND active = 1 ORDER BY score DESC";
	}*/
	#echo $q1.'<br />'.$q2.'<br />';
	$result = array();
	
	// Alle Inhalte der Seiten durchgehen
	$sql1 = $db->Query($q1);
	while($row1 = $sql1->fetchrow()) {
		// Text mit Suchbegriff ermitteln
		$searchtext = strip_tags($row1->content);
		$pos = strpos(strtolower($searchtext), strtolower($searchword));
		if($pos) {
			$text = substr($searchtext, $pos-50, 450);
			// Suchbegriff markieren
			$text = str_ireplace($searchword, "<span class='highlight' title='Suchbegriff'>".$searchword."</span>", $text);
			
			$row1->content = $text;
		}
		else $row1->content = substr($searchtext,0,500);
		
		// Suchbegriff markieren
		$row1->title = str_ireplace($searchword, "<span style='background-color: #FF9' title='Suchbegriff'>".$searchword."</span>", $row1->title);
		
		array_push($result, $row1);
	}
	
	// Alle News durchgehen
	$sql2 = $db->Query($q2);
	while($row2 = $sql2->fetchrow()) {
		$row2->isnews = 1;
		$row2->content = $row2->teaser.' '.$row2->text;
		
		// Text mit Suchbegriff ermitteln
		$searchtext = strip_tags($row2->content);
		$pos = strpos(strtolower($searchtext), strtolower($searchword));
		if($pos) {
			$text = substr($searchtext, $pos-50, 450);
			// Suchbegriff markieren
			$text = str_ireplace($searchword, "<span class='highlight' title='Suchbegriff'>".$searchword."</span>", $text);
			
			$row2->content = $text;
		}
		else $row2->content = substr($searchtext,0,500);
		
		// Suchbegriff markieren
		$row2->title = str_ireplace($searchword, "<span style='background-color: #FF9' title='Suchbegriff'>".$searchword."</span>", $row2->title);
		$row2->topic = str_ireplace($searchword, "<span class='highlight' title='Suchbegriff'>".$searchword."</span>", $row2->topic);
		
		array_push($result, $row2);
	}
	
	// Sortieren nach Punkten
	$points = array();
	foreach($result as $key => $row) {
		array_push($points,$row->score);
	}
	array_multisort($points, SORT_DESC, $result);
	
	$tmpl->assign('result', $result);
	$tmpl->assign('hits', $sql1->numrows()+$sql2->numrows());
}
else $tmpl->assign('error', "Bitte Suchbegriff eingeben!");

$tmpl->clear_cache('search.tpl');
$tmpl->assign('content', $tmpl->fetch('search.tpl'));	
?>