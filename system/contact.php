<?
/*
 * Kontaktformular
 *
 * by Sven Jungnickel
 */

if(!defined("BASEDIR")) exit;

$title = 'Kontakt';
$description = 'Sie w&uuml;nschen Beratung zum Thema Unternehmensberatung? Oder haben Sie andere Fragen an uns? Melden Sie sich einfach per Kontaktformular bei uns.';
$keywords = 'Kontaktformular, Unternehmensberatung, Unternehmensberater';

// reCAPTCHA
require(BASEDIR.'/recaptcha/src/autoload.php');
$tmpl->assign('captchaKey', $captcha_publickey);

if($_REQUEST['send'] == 1) {
	$error = array();

	if(isset($_POST["g-recaptcha-response"])) {
        $recaptcha = new \ReCaptcha\ReCaptcha($captcha_privatekey);
        $resp = $recaptcha->verify($_POST["g-recaptcha-response"], $_SERVER["REMOTE_ADDR"]);
	    if(!$resp->isSuccess()) $error['secure'] = 1;
    }
	if($_REQUEST['name'] == '') $error['name'] = 1;
	if($_REQUEST['email'] == '') $error['email'] = 1;
	if($_REQUEST['subject'] == '') $error['subject'] = 1;
	if($_REQUEST['text'] == '') $error['text'] = 1;
	
	if(count($error) == 0) {
		
		// Mail versenden
		$subject = $_REQUEST['subject']." - Kontaktformular ".$_SERVER['HTTP_HOST'];
		$message = "Von: ".$_REQUEST['name']."<br />";
		$message.= "E-Mail: ".$_REQUEST['email']."<br />";
		$message.= "Betreff: ".$_REQUEST['subject']."<br /><br />";
		$message.= nl2br(substr($_REQUEST['text'],0,5000))."<br /><br />";
		
		email($mailcontact_address, utf8_decode($mailcontact_name), utf8_decode($subject), utf8_decode($message), utf8_decode($_REQUEST['email']), utf8_decode($_REQUEST['name']));
		
		$tmpl->assign('success', 1);
	}
	else $tmpl->assign('error', $error);
}

$tmpl->clear_cache('contact.tpl');
$tmpl->assign('content', $tmpl->fetch('contact.tpl'));
?>