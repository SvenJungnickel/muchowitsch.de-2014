<?
/*
 * Impressum
 *
 * by Sven Jungnickel
 */

if(!defined("BASEDIR")) exit;

$title = 'Impressum';
$description = 'Hier finden Sie das Impressum und die Anschrift der Andreas Muchowitsch Unternehmensberatung GmbH.';
$keywords = 'Impressum, Anschrift, Postanschrift';

$tmpl->assign('content', $tmpl->fetch('imprint.tpl'));
?>