<?
/*
 * Anzeige der Inhalte der Seiten
 *
 * by Sven Jungnickel
 */

if(!defined("BASEDIR")) exit;

// Überprüfen, ob Cache vorhanden
#if(!$tmpl->is_cached('page.tpl')) { }

// ID Anhand der URL ermitteln
/*if(!isset($_REQUEST['id']) && isset($_REQUEST['url']) && $_REQUEST['url'] != '' && !is_numeric($_REQUEST['url'])) {
	$url = strtolower($_REQUEST['url']);
	$url = str_replace(' ', '_', $url);
	$url = preg_replace("/[^_a-üA-Ü0-9]/","", $url);
	$sql = $db->Query("SELECT * FROM ".PREFIX."_navigation WHERE title = '".$link."' AND active = 1");
	
}
else*/
if(isset($_REQUEST['id']) && $_REQUEST['id'] != '' && $_REQUEST['id'] != 0 && is_numeric($_REQUEST['id'])) {
	$sql = $db->Query("SELECT * FROM ".PREFIX."_navigation WHERE id = ".escs($_REQUEST['id'])." AND active = 1");
	$row = $sql->fetchrow();
	
	// Text nach Glossareinträgen durchsuchen
	$row->content = parseGlossary($row->content);
	
	$tmpl->assign('row', $row);
	
	// Krümelpfad-Navigation erstellen
	$path = array(array('title' => $row->title));
	if($row->parent != 0) {
		navipath($row->parent, $path);
	}
	krsort($path);
	if(count($path) > 1) $tmpl->assign('path', $path);
	
	// Seitentitel erstellen
	$title = '';
	$index = 1;
	$max = count($path);
	foreach($path as $line) {
		$title.= $line['title'];
		if($index < $max) $title.= ' &raquo; ';
		$index++;
	}
	$tmpl->assign('content', $tmpl->fetch('page.tpl'));
}
else include_once(BASEDIR.'/system/error.php');
?>