<?
/*
 * Detailseite von Presse und Medieninhalten
 *
 * by Sven Jungnickel
 */

if(!defined("BASEDIR")) exit;

$title = 'Presse und Medien';
$keywords = 'Presse, Medien';

if(isset($_REQUEST['pid']) && $_REQUEST['pid'] != '' && $_REQUEST['pid'] != 0) {
	$sql = $db->Query("SELECT * FROM ".PREFIX."_printmedia WHERE id = ".escs($_REQUEST['pid']));
	if($sql->numrows() > 0) {
		$row = $sql->fetchrow();
		// Link kürzen
		if($row->link != '') {
			$tmp = explode('/', $row->link);
			$row->shortlink = $tmp[2];
		}		
		$tmpl->assign('row', $row);
		
		// Überprüfen, ob Bilder dazu gespeichert sind
		$sql_pics = $db->Query("SELECT * FROM ".PREFIX."_printmedia_pics WHERE pid = ".$row->id." AND active = 1 ORDER BY id ASC");
		$pics = array();
		$number = 1;
		while($row_pics = $sql_pics->fetchrow()) {
			// Thumb ermitteln
			$row_pics->thumb = dirname($row_pics->path).'/thumb/'.basename($row_pics->path);
			$row_pics->number = $number;					
			
			array_push($pics, $row_pics);
			$number++;
		}
		$tmpl->assign('pics', $pics);

        // Seo Beschreibung setzen
        $row->metadescription = substr(strip_tags($row->descr), 0, 255);
		
		$tmpl->assign('content', $tmpl->fetch('printmedia/details.tpl'));
		
		$title = stripslashes($row->title).' &raquo; '.$title;
	}
	else include_once(BASEDIR.'/system/error.php');
}
else include_once(BASEDIR.'/system/error.php');
?>