<?
// BASEDIR definieren
define("BASEDIR", dirname(__FILE__));

// Config, Klassen und Funtionen einbinden
include_once(BASEDIR.'/config/config.php');
include_once(BASEDIR.'/class/Database.Class.php');
include_once(BASEDIR.'/functions/Func.ModRewrite.php');

// Datenbank-Prefix setzen
define("PREFIX", $db_prefix);

// Datenbank initialisieren
$db = DB::getInstance();
$db->setConnectionData($db_host, $db_user, $db_pass, $db_name);

// XML Struktur
$xml = '<?xml version="1.0" encoding="UTF-8"?>
<urlset
  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
  http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
';

// Impressum und Kontaktformular
$xml.= '<url><loc>'.$domainname.'impressum</loc><changefreq>yearly</changefreq><priority>0.1</priority></url>
';
$xml.= '<url><loc>'.$domainname.'kontakt</loc><changefreq>yearly</changefreq><priority>0.1</priority></url>
';

// Alle Newsbeiträge ermitteln
$sql_archiv = $db->Query("SELECT * FROM ".PREFIX."_news WHERE active = 1 ORDER BY ctime DESC");
$i = 0;
while($row_archiv = $sql_archiv->fetchrow()) {
    // Aktuellsten Eintag auf der Startseite für Archiv ermitteln
    if($i == 0) {
$xml.= '<url><loc>'.$domainname.'archiv</loc><lastmod>'.date(DATE_ATOM, $row_archiv->ctime).'</lastmod><changefreq>weekly</changefreq><priority>1.0</priority></url>
';
    }
$xml.= '<url><loc>'.$domainname.'archiv/'.getPageTitle($row_archiv->id,'news').'-'.$row_archiv->id.'.htm</loc><lastmod>'.date(DATE_ATOM, $row_archiv->ctime).'</lastmod><changefreq>weekly</changefreq><priority>1.0</priority></url>
';
    $i++;
}

$sql_news = $db->Query("SELECT * FROM ".PREFIX."_news WHERE active = 1 ORDER BY ctime DESC");

// Inhalte ermitteln
$sql_navi = $db->Query("SELECT id, lastmod_time FROM ".PREFIX."_navigation WHERE active = 1");
while($row_navi = $sql_navi->fetchrow()) {
	$xml.= '<url><loc>'.$domainname.getPageTitle($row_navi->id).$row_navi->id.'.htm</loc><lastmod>'.date(DATE_ATOM, $row_navi->lastmod_time).'</lastmod><changefreq>weekly</changefreq><priority>1.0</priority></url>
';	
}

// Presse- und Medieneinträge ermitteln
$sql_printmedia = $db->Query("SELECT * FROM ".PREFIX."_printmedia WHERE active = 1");
while($row_printmedia = $sql_printmedia->fetchrow()) {
	$xml.= '<url><loc>'.$domainname.getPageTitle('93').getPageTitle($row_printmedia->id,'printmedia').'/93-'.$row_printmedia->id.'.htm</loc><lastmod>'.date(DATE_ATOM, $row_printmedia->ctime).'</lastmod><changefreq>monthly</changefreq></url>
';	
}

$xml.= '</urlset>';

// Datenbank schließen
$db->close();

// XML-Struktur anzeigen
echo $xml;
?>