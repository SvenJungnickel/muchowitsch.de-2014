<?
/*
 * Presseeinträge verwalten
 *
 * by Sven Jungnickel
 */

if(!defined("BASEDIR")) exit;

if(!permission($_REQUEST['p'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
else {

	switch($_REQUEST['action']) {
	
		//===================================
		// Übersicht Presseeinträge
		//===================================
		default:
		case "":
		case "overview":
			
			if(!permission($_REQUEST['p'],$_REQUEST['action'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				
				// Anzahl Presseeinräge für Navigation ermitteln
				$sql_num = $db->Query("SELECT id FROM ".PREFIX."_printmedia $andsort");
				$num = $sql_num->numrows();
				$tmpl->assign('num', $num);
				
				$limit = ($_REQUEST['pp'] == '') ? 20 : escs($_REQUEST['pp']);
				$page = ($_REQUEST['page'] == '') ? 1 : escs($_REQUEST['page']);
				$a = $page * $limit - $limit;
				
				// Navigation erzeugen
				if($num > $limit) {
					$link = "index.php?p=printmedia&amp;action=overview&amp;pp=".$limit."&amp;page={s}&amp;sort=".escs($_REQUEST['sort']);
					$seiten = ceil($num / $limit);
					$tmpl->assign('navi', pagenav_tpl($seiten, $link, 4, 4, 1));
				}
				
				// Glossareinträge ermitteln
				$sql = $db->Query("SELECT * FROM ".PREFIX."_printmedia ORDER BY ctime DESC LIMIT $a, $limit");
				$printmedia = array();
				while($row = $sql->fetchrow()) {
					// Anzahl Bilder zum Presseeintrag zählen
					$sql_count_pics = $db->Query("SELECT * FROM ".PREFIX."_printmedia_pics WHERE pid = ".$row->id);
					$row->count_pics = $sql_count_pics->numrows();
					
					array_push($printmedia, $row);
				}
				$tmpl->assign('printmedia', $printmedia);
		
				$tmpl->assign('content', $tmpl->fetch('printmedia/overview.tpl'));	
			}
		break;
		
		//===================================
		// neuer Presseeintrag / bearbeiten
		//===================================
		case "new":
		case "edit":
		
			if(!permission($_REQUEST['p'],$_REQUEST['action'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				// Daten ermitteln
				if($_REQUEST['action'] != 'new' && $_REQUEST['id'] != '') {
					$sql = $db->Query("SELECT * FROM ".PREFIX."_printmedia WHERE id = ".escs($_REQUEST['id']));
					$row = $sql->fetchrow();
					$tmpl->assign('row', $row);
					
					if(file_exists(BASEDIR.$printmediaUploadPath.'thumb/'.$row->image)) $tmpl->assign('image', $row->image);
				}
				
				if($_REQUEST['save'] == 1) {
					$error = '';
					
					if($_REQUEST['title'] == '') $error.= 'Es muss der Titel angegeben werden.<br>';
					if($_REQUEST['date'] == '') $error.= 'Es muss ein Datum angegeben werden.<br>';
					if($_REQUEST['text'] == '') $error.= 'Es muss die Beschreibung eingegeben werden.<br>';
										
					// Image kopieren
					if($_REQUEST['image'] != '') {
						
						$path_big = BASEDIR.$printmediaUploadPath.'big/';
						$path_thumb = BASEDIR.$printmediaUploadPath.'thumb/';
						
						$image = $_REQUEST['image'];
						@copy(BASEDIR.'cache/'.$image,$path_big.$image);
						@chmod($path_big.$image,0777);
						
						$img = @getimagesize($path_big.$image);
						if($img) {
							#$width = 220;
							#$height = $img[1] / $img[0] * $width;
							
							// verkleinerte Version erzeugen ($maxThumbWidth, $maxThumbHeight und $thumbQuali werden in der config gesetzt)
							resize_img($image, $path_big, $path_thumb, $maxThumbWidth/*$width*/, $maxThumbHeight/*$height*/, $thumbQuali , $output = 0);
						}
						else $image = '';
						
						// hochgeladenes Bild aus Zwischenablage entfernen
						@unlink(BASEDIR.'/cache/'.$image);
						// falls vorhanden, bereits bestehendes Bild löschen
						if($_REQUEST['old_image'] != '') {
							@unlink($path_big.$_REQUEST['old_image']);
							@unlink($path_thumb.$_REQUEST['old_image']);
						}
					}				
					elseif($_REQUEST['del_image'] == 1) {
						// Bild löschen
						@unlink($path_big.$_REQUEST['old_image']);
						@unlink($path_thumb.$_REQUEST['old_image']);
						$image = '';
					}
					elseif($_REQUEST['old_image'] != '') $image = $_REQUEST['old_image'];
					$tmpl->assign('image', $image);
					
					if($error == '') {
						$tmp_date = explode('.',$_REQUEST['date']);
						$date = mktime(0,0,0,$tmp_date[1],$tmp_date[0],$tmp_date[2]);
						$descr = stripslashes($_REQUEST['text']);
						$active = ($_REQUEST['active'] == 1) ? 1 : 0;
						
						if($_REQUEST['action'] == 'new') {
							$sql = $db->Query("INSERT INTO ".PREFIX."_printmedia (ctime,title,date,link,newspaper,newspage,image,descr,active)
												 VALUES(".time().",'".escs($_REQUEST['title'])."',".$date.",'".escs($_REQUEST['link'])."','".escs($_REQUEST['newspaper'])."','".escs($_REQUEST['newspage'])."','".$image."','".$descr."',".$active.")");
						}
						else {
							$sql = $db->Query("UPDATE ".PREFIX."_printmedia SET title = '".escs($_REQUEST['title'])."', date = ".$date.", link = '".escs($_REQUEST['link'])."', newspaper = '".escs($_REQUEST['newspaper'])."', newspage = '".escs($_REQUEST['newspage'])."', image = '".$image."', descr = '".$descr."', active = ".$active." WHERE id = ".$row->id);
						}
						// Cache leeren
						clearcache(BASEDIR.'temp/');
						
						if($sql) header('location: '.$admindomain.'index.php?p=printmedia&action=overview&pp='.$_REQUEST['pp'].'&page='.$_REQUEST['page'].'&sort='.escs($_REQUEST['sort']));
						else $tmpl->assign('error', 'Es trat ein Fehler beim Speichern der Daten auf. Bitte versuchen Sie es erneut.');		
					}
					else $tmpl->assign('error', $error);
				}
				
				// CKEditor einbinden
				/*include_once(ADMINDIR.'/ckeditor/ckeditor.php');
				$CKEditor = new CKEditor();
				$CKEditor->basePath = '/admin/ckeditor/';
				$CKEditor->returnOutput = true;		
				$CKEditor->config['customConfig'] = 'admin_config.js';*/
				
				$text = ($error != '') ? stripslashes($_REQUEST['text']) : $row->descr;
				/*$tmpl->assign("text", $CKEditor->editor("text", $text));*/
				$tmpl->assign("text", $text);
				
				$tmpl->assign('content', $tmpl->fetch('printmedia/form.tpl'));	
			}
		break;
		
		//===================================
		// Presseeintrag aktivieren / deaktivieren
		//===================================
		case "active":
			
			if(!permission($_REQUEST['p'],$_REQUEST['action'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				if($_REQUEST['id'] != '' && $_REQUEST['option'] != '') {
						
					$db->Query("UPDATE ".PREFIX."_printmedia SET active = ".escs($_REQUEST['option'])." WHERE id = ".escs($_REQUEST['id']));

					// Cache leeren
					clearcache(BASEDIR.'temp/');
				}
				
				header('location: index.php?p=printmedia&action=overview&pp='.$_REQUEST['pp'].'&page='.$_REQUEST['page'].'&sort='.escs($_REQUEST['sort']));
			}
		break;
		
		//===================================
		// Presseeintrag löschen
		//===================================
		case "delete":
		
			if(!permission($_REQUEST['p'],$_REQUEST['action'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				if($_REQUEST['id'] != '') {
					// Image-Datei ermitteln
					$sql = $db->Query("SELECT id, image FROM ".PREFIX."_printmedia WHERE id = ".escs($_REQUEST['id']));
					$row = $sql->fetchrow();
					
					// gespeicherte Bilder löschen
					@unlink(BASEDIR.$printmediaUploadPath.'big/'.$row->image);
					@unlink(BASEDIR.$printmediaUploadPath.'thumb/'.$row->image);
					
					// Überprüfen, ob weitere Bilder hochgeladen wurden
					$sql_pics = $db->Query("SELECT path FROM ".PREFIX."_printmedia_pics WHERE pid = ".$row->id." LIMIT 1");
					$row_pics = $sql_pics->fetchrow();
					$path = BASEDIR.$printmediaUploadPath.$row->id.'/';					
					
					// Ordner rekursiv entfernen
					$dir = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::CHILD_FIRST);
					for($dir->rewind(); $dir->valid(); $dir->next()) {
						if($dir->isDir()) @rmdir($dir->getPathname());
						else @unlink($dir->getPathname());
					}
					@rmdir($path);
					
					// Datenbankeintrag entfernen
					$db->Query("DELETE FROM ".PREFIX."_printmedia_pics WHERE pid = ".$row->id);
					$db->Query("DELETE FROM ".PREFIX."_printmedia WHERE id = ".$row->id);
					
					// Cache leeren
					clearcache(BASEDIR.'temp/');
				}
				
				header('location: index.php?p=printmedia&action=overview&pp='.$_REQUEST['pp'].'&page='.$_REQUEST['page'].'&sort='.escs($_REQUEST['sort']));
			}
		break;
		
		//===================================
		// Bilder zu Presseeinträgen anzeigen / bearbeiten
		//===================================
		case "showpics":
		
			if(!permission($_REQUEST['p'],$_REQUEST['action'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				if($_REQUEST['id'] != '') {
					
					if($_REQUEST['save'] == 1) {
						$db->Query("UPDATE ".PREFIX."_printmedia_pics SET active = 0 WHERE pid = ".escs($_REQUEST['id']));
						if(count($_REQUEST['active']) > 0) {
							foreach($_REQUEST['active'] as $line) {
								$db->Query("UPDATE ".PREFIX."_printmedia_pics SET active = 1 WHERE id = ".$line);
								
								// Cache leeren
								clearcache(BASEDIR.'temp/');
							}
						}
						if(count($_REQUEST['delete']) > 0) {
							foreach($_REQUEST['delete'] as $line) {
								$sql = $db->Query("SELECT path FROM ".PREFIX."_printmedia_pics WHERE id = ".$line);
								$row = $sql->fetchrow();
								$image = BASEDIR.$printmediaUploadPath.$row->path;
								$thumb = BASEDIR.$printmediaUploadPath.dirname($row->path).'/thumb/'.basename($row->path);
								
								@unlink($image);
								@unlink($thumb);							
								$db->Query("DELETE FROM ".PREFIX."_printmedia_pics WHERE id = ".$line);
								
								// Cache leeren
								clearcache(BASEDIR.'temp/');
							}
						}
					}
					
					// Presseeintrags-Daten ermitteln					
					$sql = $db->Query("SELECT * FROM ".PREFIX."_printmedia WHERE id = ".escs($_REQUEST['id']));
					$row = $sql->fetchrow();
					$tmpl->assign('row', $row);
					
					// Bilder-Daten ermitteln
					$sql_pics = $db->Query("SELECT * FROM ".PREFIX."_printmedia_pics WHERE pid = ".escs($_REQUEST['id'])." ORDER BY id ASC");
					$pics = array();
					$number = 1;
					while($row_pics = $sql_pics->fetchrow()) {						
						// Thumb ermitteln
						$row_pics->thumb = dirname($row_pics->path).'/thumb/'.basename($row_pics->path);
						$row_pics->number = $number;					
						
						array_push($pics, $row_pics);
						$number++;
					}
					$tmpl->assign('pics', $pics);
					$tmpl->assign('count_pics', $sql_pics->numrows());
					
					$tmpl->assign('content', $tmpl->fetch('printmedia/showpics.tpl'));	
				}
				else header('location: index.php?p=printmedia&action=overview&pp='.$_REQUEST['pp'].'&page='.$_REQUEST['page'].'&sort='.escs($_REQUEST['sort']));
			}
		break;
		
		//===================================
		// Bilder zu Presseeinträgen hochladen
		//===================================
		case "upload":
		
			if(!permission($_REQUEST['p'],$_REQUEST['action'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				if($_REQUEST['id'] != '') {
					
					// Galerie aktivieren
					if($_REQUEST['send'] == 1) {
						// Cache leeren
						clearcache(BASEDIR.'temp/');

						header('location: '.$admindomain.'index.php?p=printmedia&action=overview&pp='.$_REQUEST['pp'].'&page='.$_REQUEST['page'].'&sort='.$_REQUEST['sort']);
					}
					
					session_start();
					$_SESSION["file_info"] = array();			
					$tmpl->assign('session_id', session_id());
					$tmpl->assign('uid', UID);
					
					// Galeriedaten ermitteln
					$sql = $db->Query("SELECT * FROM ".PREFIX."_printmedia WHERE id = ".escs($_REQUEST['id']));
					$row = $sql->fetchrow();
					$tmpl->assign('row', $row);
					
					// Speicherpfad festlegen
					$uploaddir = $row->id.'/';
					$tmpl->assign('uploaddir', $uploaddir);
					
					// Speicherpfad erstellen, wenn er noch nicht existiert			
					if(!file_exists(BASEDIR.$printmediaUploadPath.$uploaddir)) mkdir_recursive(BASEDIR.$printmediaUploadPath.$uploaddir,0777);
					$uploaddir_thumb = $uploaddir.'thumb/';
					if(!file_exists(BASEDIR.$printmediaUploadPath.$uploaddir_thumb)) mkdir_recursive(BASEDIR.$printmediaUploadPath.$uploaddir_thumb,0777);
										
					$tmpl->assign('content', $tmpl->fetch('printmedia/upload.tpl'));

				}
				else header('location: index.php?p=printmedia&action=overview&pp='.$_REQUEST['pp'].'&page='.$_REQUEST['page'].'&sort='.escs($_REQUEST['sort']));
			}
		break;
	}
}
?>