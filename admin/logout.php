<?
/*
 * Logout
 *
 * by Sven Jungnickel
 */

unset($_SESSION['uid']);
unset($_SESSION['uname']);
unset($_SESSION['ugroup']);
if(isset($_COOKIE['uid'])) setcookie('uid',0,time()-3600,'/');
if(isset($_COOKIE['pass'])) setcookie('pass','',time()-3600,'/');

header('location: '.$admindomain.'index.php');
?>