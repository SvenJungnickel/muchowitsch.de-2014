<?
/*
 * Flyer Upload f�r Zwischenansicht
 *
 * by Sven Jungnickel
*/
	// BASEDIR definieren
	define("ADMINDIR", substr(dirname(__FILE__),0),true);
	define("BASEDIR", substr(dirname(__FILE__),0,-strlen(basename(ADMINDIR))),true);
	
	// Scripte einbinden
	include_once(BASEDIR.'/config/config.php');
		
	$imgToEdit_exist = false;
	if(isset($_REQUEST['imgToEdit']) && $_REQUEST['imgToEdit'] == "1")
	{
		// Image hochladen
		$targetTEMP = BASEDIR."cache/";
		$list = array(' ' => '_', '�' => 'ue', '�' => 'oe', '�' => 'ae', '�' => 'Ue', '�' => 'Oe', '�' => 'Ae', '�' => 'ss', '�' => 'a', '�' => 'a', '�' => 'e', '�' => 'e', '�' => 'i', '�' => 'i', '�' => 'o', '�' => 'o', '�' => 'u', '�' => 'u');
		$filename = trim(strtr($_FILES['file']['name'],$list),1);	
		$filename = time().'_'.$filename;
		move_uploaded_file($_FILES['file']['tmp_name'],$targetTEMP.$filename);
		
		// Gr��e ermitteln und Daten festlegen
		$img = @getimagesize($targetTEMP.$filename);
		if($img) {
			$width = 220;
			$height = round($img[1] / $img[0] * $width);
			
			$imgToEdit_width = $width;
			$imgToEdit_height = $height;
			$imgToEdit_name = $filename;
			$imgToEdit_exist = true;
		}		
	}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>image_upload</title>
	</head>
	<link href="<?=$admindomain?>templates/admin.css" rel="stylesheet" type="text/css">
	<style>
		<!--
			body
			{
				margin:0px;
				padding:0px;
				background-color:#F5F5F5;
			}
		-->
	</style>
    <script type="text/javascript">
	function uplode_img_to_cache()
	{
		document.getElementById('imgToEdit').value = '1';
		document.getElementById('loade_div').style.display = 'block';
		parent.document.getElementById('iframe_image_upload').style.height = '40px';
		document.getElementById('kform').submit();
	}
	function onEndUpload(flyer,height)
	{
		parent.document.getElementById('iframe_image_upload').style.height = height + 24 + 'px';
		parent.document.getElementById('image').value = flyer;
	}
	</script>
	<body>    	
		<form action="" method="post" enctype="multipart/form-data" id="kform">
			<input name="file" type="file" size="40" onchange="uplode_img_to_cache()">
            <input name="imgToEdit" id="imgToEdit" type="hidden" value="0" />
			<div align="center" style="display:none; width:340px; height:20px;" id="loade_div">
				<img src="<?=$domainname.$img_path?>ajax_loader.gif" style="border:0px; margin:2px; height:16px; width:16px;" />
			</div>
<?
	if($imgToEdit_exist) {
?>
			<div id="cropper_div" style="margin-top:2px;">
			
                <img src="<?=$domainname?>cache/<?= urlencode($imgToEdit_name) ?>" alt="" id="cropperImage" width="<?= $imgToEdit_width ?>" height="<?= $imgToEdit_height ?>" />

			</div>
            <script language="javascript" type="text/javascript">onEndUpload('<?=$imgToEdit_name?>',<?=$imgToEdit_height?>);</script>
<?
	}
?>
		</form>
	</body>
</html>