<?
/*
 * Seiteninhalte verwalten
 *
 * by Sven Jungnickel
 */

if(!defined("BASEDIR")) exit;

if(!permission($_REQUEST['p'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
else {

	switch($_REQUEST['action']) {
	
		//===================================
		// Übersicht Seiteninhalte
		//===================================
		default:
		case "":
		case "overview":			
			
			if(!permission($_REQUEST['p'],$_REQUEST['action'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {

				include_once(BASEDIR.'/functions/Func.Navigation.php');
				
				// Inhalte ermitteln
				$sql = $db->Query("SELECT * FROM ".PREFIX."_navigation WHERE parent = 0 ORDER BY pos ASC");
				$pages = array();
				while($row = $sql->fetchrow()) {
					// Unternavigation ermitteln
					$row->subnavi = subnavi($row->id);
					
					$row->content = substr(htmlentities($row->content),0,150);
					
					array_push($pages, $row);
				}
				$tmpl->assign('pages', $pages);
		
				$tmpl->assign('content', $tmpl->fetch('pages/overview.tpl'));
			}
		break;
		
		//===================================
		// neuen Inhalt anlegen / bearbeiten
		//===================================		
		case "new":
		case "edit":
		
			if(!permission($_REQUEST['p'],$_REQUEST['action'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {				
				
				// Seiteninhalte ermitteln
				$content = '';
				if($_REQUEST['action'] == 'edit' && $_REQUEST['id'] != '') {
					$sql = $db->Query("SELECT * FROM ".PREFIX."_navigation WHERE id = ".escs($_REQUEST['id']));
					$row = $sql->fetchrow();
					$content = $row->content;
					$tmpl->assign('row', $row);
				}
				
				if($_REQUEST['save'] == 1) {
					$error = '';
					
					if($_REQUEST['title'] == '') $error.= 'Es muss ein Titel angegeben werden.<br>';
					if($_REQUEST['menu'] == '') $error.= 'Es muss ein Men&uuml;punkt ausgewählt werden.<br>';
					
					if($error == '') {
						
						$parent = ($_REQUEST['menu'] != '') ? $_REQUEST['menu'] : 0;
						// Position des neuen Menüpunktes neu ermitteln
						if($row->parent != $parent) {
							$sql_pos = $db->Query("SELECT pos FROM ".PREFIX."_navigation WHERE parent = ".$parent." ORDER BY pos DESC LIMIT 1");
							if($sql_pos->numrows() == 0) $pos = 0;
							else {
								$row_pos = $sql_pos->fetchrow();
								$pos = $row_pos->pos+1;
							}
						}
						else $pos = $row->pos;
						$active = ($_REQUEST['active'] == 1) ? 1 : 0;
						$display = ($_REQUEST['display'] == 1) ? 1 : 0;
						$text = stripslashes($_REQUEST['text']);
						
						if($_REQUEST['action'] == 'new') {
							$sql = $db->Query("INSERT INTO ".PREFIX."_navigation (parent,title,link,pos,active,display,content,metadescription,keywords,lastmod_time) VALUES(".$parent.",'".escs($_REQUEST['title'])."','".escs($_REQUEST['link'])."',".$pos.",".$active.",".$display.",'".$text."','".escs($_REQUEST['metadescription'])."','".escs($_REQUEST['keywords'])."',".time().")");
						}
						else {
							$sql = $db->Query("UPDATE ".PREFIX."_navigation SET parent = ".$parent.", title = '".escs($_REQUEST['title'])."', pos = ".$pos.", active = ".$active.", display = ".$display.", content = '".$text."', metadescription = '".escs($_REQUEST['metadescription'])."', keywords = '".escs($_REQUEST['keywords'])."', lastmod_time = ".time()." WHERE id = ".$row->id);
						}
						// Cache leeren
						clearcache(BASEDIR.'temp/');
						
						if($sql) header('location: '.$admindomain.'index.php?p=pages&action=overview');
						else $tmpl->assign('error', 'Es trat ein Fehler beim Speichern der Daten auf. Bitte versuchen Sie es erneut.');		
					}
					else $tmpl->assign('error', $error);
				}
				
				// Menüpunkte ermitteln
				$sql = $db->Query("SELECT id, title FROM ".PREFIX."_navigation WHERE parent = 0 ORDER BY pos ASC");
				$menue = array();
				while($row = $sql->fetchrow()) {
					// Unternavigation ermitteln
					$row->subnavi = subnavi($row->id);
										
					array_push($menue, $row);
				}
				$tmpl->assign('menue', $menue);
				
				// CKEditor einbinden /*veraltet*/
				/*include_once(ADMINDIR.'/ckeditor/ckeditor.php');
				$CKEditor = new CKEditor();
				$CKEditor->basePath = '/admin/ckeditor/';
				$CKEditor->returnOutput = true;		
				$CKEditor->config['customConfig'] = 'admin_config.js';*/
				
				$text = ($error != '') ? stripslashes($_REQUEST['text']) : $content;
				/*$tmpl->assign("text", $CKEditor->editor("text", $text));*/
				$tmpl->assign("text", $text);
				
				$tmpl->assign('content', $tmpl->fetch('pages/form.tpl'));	
			}
		break;
		
		//===================================
		// Inhalt aktivieren / deaktivieren
		//===================================
		case "active":
			
			if(!permission($_REQUEST['p'],$_REQUEST['action'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				if($_REQUEST['id'] != '' && $_REQUEST['option'] != '') {
					$db->Query("UPDATE ".PREFIX."_navigation SET active = ".escs($_REQUEST['option']).", display = ".escs($_REQUEST['option'])." WHERE id = ".escs($_REQUEST['id']));

					// Cache leeren
					clearcache(BASEDIR.'temp/');
				}
				
				header('location: index.php?p=pages&action=overview');		
			}
		break;
		
		//===================================
		// Inhaltsmenü ein- / ausblenden
		//===================================
		case "display":
			
			if(!permission($_REQUEST['p'],'active')) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				if($_REQUEST['id'] != '' && $_REQUEST['option'] != '') {
					$db->Query("UPDATE ".PREFIX."_navigation SET display = ".escs($_REQUEST['option'])." WHERE id = ".escs($_REQUEST['id']));

					// Cache leeren
					clearcache(BASEDIR.'temp/');
				}
				
				header('location: index.php?p=pages&action=overview');		
			}
		break;
				
		//===================================
		// Inhalt löschen
		//===================================
		case "delete":
		
			if(!permission($_REQUEST['p'],$_REQUEST['action'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				if($_REQUEST['id'] != '') {
					// Datenbankeintrag entfernen
					$db->Query("DELETE FROM ".PREFIX."_navigation WHERE id = ".escs($_REQUEST['id']));
					
					// Cache leeren
					clearcache(BASEDIR.'temp/');
				}
				
				header('location: index.php?p=pages&action=overview');
			}
		break;
		
		//===================================
		// Reihenfolge des Inhaltes ändern
		//===================================
		case "order":		
			
			if(!permission($_REQUEST['p'],'edit')) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				
				// alle Inhalte der gleichen Ebene ermitteln
				$sql = $db->Query("SELECT parent FROM ".PREFIX."_navigation WHERE id = ".escs($_REQUEST['id']));
				$row = $sql->fetchrow();
				
				$sql_pages = $db->Query("SELECT * FROM ".PREFIX."_navigation WHERE parent = ".$row->parent." ORDER BY pos ASC");
				$pages = array();
				while($row_pages = $sql_pages->fetchrow()) {										
					array_push($pages, $row_pages);
				}
				$tmpl->assign('pages', $pages);
		
				$tmpl->assign('content', $tmpl->fetch('pages/order.tpl'));
			}
		break;
		
		//===================================
		// Ajax: veränderte Reihenfolge speichern
		//===================================
		case "updateOrder":		
			
			if($_REQUEST['order'] == '' || count($_REQUEST['order']) == 0) {
				echo 'Falsche Parameter';
			}
			else {
				
				// Reihenfolge speichern
				$index = 0;
				foreach($_REQUEST['order'] as $line) {
					$db->Query("UPDATE ".PREFIX."_navigation SET pos = ".$index." WHERE id = ".$line);
					$index++;
				}
				
				echo 1;
			}
			
			exit;
		break;
	}
}
?>