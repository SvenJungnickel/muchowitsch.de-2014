<?
/*
 * Loginüberprüfung
 *
 * by Sven Jungnickel
 */

if(!defined("BASEDIR")) exit;

if(isset($_SESSION['uid']) && $_SESSION['uid'] != '' && $_SESSION['uid'] != 0) header('location: index.php');

switch($_REQUEST['action']) {

	//===================================
	// Loginseite
	//===================================
	default:
	case "":

		if($_REQUEST['send'] == 1) {
			$error = '';
			if($_REQUEST['uname'] == '') $error.= 'Bitte den Benutzernamen eingeben.<br />';
			if($_REQUEST['password'] == '') $error.= 'Bitte das Passwort eingeben.<br />';
			
			if($error == '') {
				$password = md5(escs($_REQUEST['password']));
				$sql = $db->Query("SELECT id, uname, ugroup, password, active FROM ".PREFIX."_admin_user WHERE uname = '".escs($_REQUEST['uname'])."' AND password = '".$password."'");
				
				if($sql->numrows() == 0) $tmpl->assign('error', 'Der Benutzername und das Passwort stimmen nicht &uuml;berein.');
				else {
					$row = $sql->fetchrow();
					
					if($row->active == 0) $tmpl->assign('error', 'Der Account wurde deaktiviert. Bitte wenden Sie sich an den Administrator.');
					else {
						$_SESSION['uid'] = $row->id;
						$_SESSION['uname'] = $row->uname;
						$_SESSION['ugroup'] = $row->ugroup;
											
						// Cookie setzen für Dauerlogin
						if($_REQUEST['setcookie'] == 1) {
							setcookie('uid',$row->id,time()+60*60*24*365,'/');
							setcookie('pass',$row->password,time()+60*60*24*365,'/');
						}
						
						header('location: '.$admindomain.'index.php');
					}
				}
			}
			else $tmpl->assign('error', $error);
		}
		
		$template = 'login/login.tpl';
	break;
	
	case "pw_recover":
		
		if($_REQUEST['send'] == 1) {
			$error = '';
			if($_REQUEST['uname'] == '') $error.= 'Bitte den Benutzernamen eingeben.<br />';
			if($_REQUEST['email'] == '') $error.= 'Bitte die Email eingeben.<br />';
			
			if($_REQUEST['uname'] != '' && $_REQUEST['email'] != '') {
				$sql_user = $db->Query("SELECT id, uname, email FROM ".PREFIX."_admin_user WHERE uname = '".escs($_REQUEST['uname'])."' AND email = '".escs($_REQUEST['email'])."'");
				if($sql_user->numrows() == 0) $error.= 'Der Benutzername und die Email Adresse stimmen nicht &uuml;berein.';
			}
			
			if($error == '') {
				$row = $sql_user->fetchrow();
				
				// Neues Passwort genierieren
				$pass = makepass();
				$pass_md5 = md5($pass);
			
				$db->Query("UPDATE ".PREFIX."_admin_user SET password_tmp = '".$pass_md5."' WHERE id = ".$row->id);
				
				// Email versenden
				$subject = "Neues Passwort";
				$message = "Hallo ".$row->uname.",<br /><br />";
				$message.= "du hast ein neues Passwort angefordert.<br />";
				$message.= "Dein neues Passwort lautet: <b>".$pass."</b><br /><br />";
				$message.= "Um dein neues Passwort zu best&auml;tigen, klicke auf folgenden Link:<br />";
				$message.= '<a href="'.$admindomain.'index.php?p=login&action=confirm&amp;pw='.$pass.'&amp;uid='.$row->id.'">'.$admindomain.'index.php?p=login&action=confirm&amp;pw='.$pass.'&amp;uid='.$row->id.'</a>';
				
				email($row->email,$row->uname,$subject,$message);
				
				$tmpl->assign('thankyou', 1);		
			}
			else $tmpl->assign('error', $error);
		}
		
		$template = 'login/pw_recover.tpl';		
	break;
	
	case "confirm":
	
		$error = '';
		
		if($_REQUEST['pw'] == '' || $_REQUEST['uid'] == '') $error.= 'Es ist ein Fehler aufgetreten. M&ouml;glicherweise ist der Link fehlerhaft.';
		else {
			$pass = md5($_REQUEST['pw']);
			$sql = $db->Query("SELECT id, uname, ugroup FROM ".PREFIX."_admin_user WHERE id = ".escs($_REQUEST['uid'])." AND password_tmp = '".$pass."'");
			if($sql->numrows() == 0) $error.= 'Es ist ein Fehler aufgetreten. M&ouml;glicherweise ist der Link fehlerhaft.';
		}
		
		if($error == '') {
			$row = $sql->fetchrow();
			
			$db->Query("UPDATE ".PREFIX."_admin_user SET password = '".$pass."', password_tmp = '' WHERE id = ".$row->id);
			
			$_SESSION['uid'] = $row->id;
			$_SESSION['uname'] = $row->uname;
			$_SESSION['ugroup'] = $row->ugroup;			
		}
		else $tmpl->assign('error', $error);
		
		$template = 'login/confirm.tpl';
	break;
}
?>