<?
/*
 * Administrationsbereich Hauptdatei
 *
 * by Sven Jungnickel
 */

session_start();

// BASEDIR definieren
define("ADMINDIR", substr(dirname(__FILE__),0),true);
define("BASEDIR",  substr(dirname(__FILE__),0,-strlen(basename(ADMINDIR))),true);

// Scripte einbinden
include_once(BASEDIR.'/config/config.php');
include_once(BASEDIR.'/class/Smarty.class.php');
include_once(BASEDIR.'/class/Database.Class.php');
include_once(BASEDIR.'/functions/Func.Init.php');

// Datenbank-Prefix setzen
define("PREFIX", $db_prefix);

// Datenbank initialisieren
$db = DB::getInstance();
$db->setConnectionData($db_host, $db_user, $db_pass, $db_name);

// Template-Engine initialisieren
$tmpl = new Smarty;
$tmpl->compile_dir = ADMINDIR.'/temp';

// Caching Einstellungen
$tmpl->compile_check = true;
$tmpl->force_compile = true;
$tmpl->caching = 0;
$tmpl->cache_dir = ADMINDIR.'/temp';

// Funktionen f�r Smarty registrieren
$tmpl->register_function("in_array", "in_array");
$tmpl->register_function("permission", "permission");
$tmpl->register_modifier("sslash", "stripslashes");
$tmpl->register_function("subpages", "subnavi_show");

// globale Variablen initialisieren
$tmpl->assign('img_path', $img_path);
$tmpl->assign('js_path', $js_path);
$tmpl->assign('css_path', $css_path);
$tmpl->assign('upload_path', $upload_path);
$tmpl->assign('gallery_path', $galleryUploadPath);

// Domain-Adressen definieren
$tmpl->assign('domainname', $domainname);
$tmpl->assign('admindomain', $admindomain);

// Header-Titel initialisieren
$tmpl->assign('title', 'Administrationsbereich muchowitsch.de');

// Logindaten �berpr�fen
if($_REQUEST['p'] != 'login') {
	if(isset($_COOKIE['uid']) && $_COOKIE['uid'] != '' && $_COOKIE['uid'] != 0 && isset($_COOKIE['pass']) && $_COOKIE['pass'] != '' && $_COOKIE['pass'] != 0) {
		// �berpr�fen der Benutzerdaten
		$sql = $db->Query("SELECT id, uname, ugroup FROM ".PREFIX."_admin_user WHERE id = ".escs($_COOKIE['uid'])." AND password = '".escs($_COOKIE['pass'])."' AND active = 1");
		
		if($sql->numrows() == 0) {
			setcookie('uid', 0, time()-3600,'/');
			setcookie('pass', '', time()-3600,'/');
			header('location: index.php?p=login');
		}
		else {
			$row = $sql->fetchrow();
			define("UID", $row->id);
			define("UNAME", $row->uname);
			define("UGROUP", $row->ugroup);
		}
	}
	elseif(isset($_SESSION['uid']) && $_SESSION['uid'] != '' && $_SESSION['uid'] != 0) {
		// �berpr�fen der Benutzerdaten
		$sql = $db->Query("SELECT id, uname, ugroup FROM ".PREFIX."_admin_user WHERE id = ".escs($_SESSION['uid'])." AND active = 1");
		
		if($sql->numrows() == 0) {
			$_SESSION['uid'] = 0;
			$_SESSION['uname'] = '';
			$_SESSION['ugroup'] = 0;
			header('location: index.php?p=login');
		}
		else {
			$row = $sql->fetchrow();
			define("UID", $row->id);
			define("UNAME", $row->uname);
			define("UGROUP", $row->ugroup);
		}
		
		define("UID", $_SESSION['uid']);
		define("UNAME", $_SESSION['uname']);
		define("UGROUP", $_SESSION['ugroup']);
	}
	else header('location: index.php?p=login');
	
	$tmpl->assign('uid', UID);
	$tmpl->assign('uname', UNAME);
	$tmpl->assign('ugroup', UGROUP);
	
	$template = 'admin.tpl';

	if($_REQUEST['p'] != '') {
		if(file_exists(ADMINDIR.'/'.$_REQUEST['p'].'.php')) include_once(ADMINDIR.'/'.$_REQUEST['p'].'.php');
		else include_once(ADMINDIR.'/error.php');
	}
	else include_once(ADMINDIR.'/start.php');
}
else include_once(ADMINDIR.'/login.php');

$tmpl->display($template);

// Datenbank schlie�en
$db->close();
?>