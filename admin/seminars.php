<?
/*
 * Seminare verwalten
 *
 * by Sven Jungnickel
 */

if(!defined("BASEDIR")) exit;

if(!permission($_REQUEST['p'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
else {

	switch($_REQUEST['action']) {
	
		//===================================
		// Übersicht Seminare
		//===================================
		default:
		case "":
		case "overview":
			
			if(!permission($_REQUEST['p'],$_REQUEST['action'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				// Anzahl Seminare für Navigation ermitteln
				$sql_num = $db->Query("SELECT id FROM ".PREFIX."_seminars");
				$num = $sql_num->numrows();
				$tmpl->assign('num', $num);
				
				$limit = ($_REQUEST['pp'] == '') ? 20 : escs($_REQUEST['pp']);
				$page = ($_REQUEST['page'] == '') ? 1 : escs($_REQUEST['page']);
				$a = $page * $limit - $limit;
				
				// Navigation erzeugen
				if($num > $limit) {
					$link = "index.php?p=".$_REQUEST['p']."&amp;action=".$_REQUEST['action']."&amp;pp=".$limit."&amp;page={s}";
					$seiten = ceil($num / $limit);
					$tmpl->assign('navi', pagenav_tpl($seiten, $link, 4, 4, 1));
				}
				
				// Seminare  ermitteln
				$sql = $db->Query("SELECT * FROM ".PREFIX."_seminars ORDER BY time_start DESC LIMIT $a, $limit");
				$seminars = array();
				while($row = $sql->fetchrow()) {
					// Stadt und Bundeslandermitteln
					$sql_city = $db->Query("SELECT c.title AS city, s.title AS state FROM ".PREFIX."_seminars_city AS c JOIN ".PREFIX."_seminars_state AS s ON (s.id = c.sid) WHERE c.id = ".$row->cid);
					$row_city = $sql_city->fetchrow();
					$row->city = $row_city->city;
					$row->state = $row_city->state;
					
					// Seminartyp ermitteln
					$sql_type = $db->Query("SELECT title, type FROM ".PREFIX."_seminars_type WHERE id = ".$row->tid);
					$row_type = $sql_type->fetchrow();
					if($row_type->type != '') $row->type = $row_type->type.' - ';
					$row->type.= $row_type->title;
					
					array_push($seminars, $row);
				}
				$tmpl->assign('seminars', $seminars);
		
				$tmpl->assign('content', $tmpl->fetch('seminars/overview.tpl'));	
			}
		break;
		
		//===================================
		// neues Seminar anlegen / bearbeiten
		//===================================
		case "new":
		case "edit":
			
			if(!permission($_REQUEST['p'],$_REQUEST['action'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				if(isset($_REQUEST['id']) && $_REQUEST['id'] != '' && $_REQUEST['id'] != 0) {
					$sql = $db->Query("SELECT * FROM ".PREFIX."_seminars WHERE id = ".escs($_REQUEST['id']));
					$row = $sql->fetchrow();
					
					// Stadt und Bundeslandermitteln
					$sql_city = $db->Query("SELECT c.title AS city, s.title AS state FROM ".PREFIX."_seminars_city AS c JOIN ".PREFIX."_seminars_state AS s ON (s.id = c.sid) WHERE c.id = ".$row->cid);
					$row_city = $sql_city->fetchrow();
					$row->city = $row_city->city.' ('.$row_city->state.')';
					
					$tmpl->assign('row', $row);
				}
				else $tmpl->assign('timeend', mktime(0,0,0,strftime('%m'),strftime('%d')+2,strftime('%Y')));
				
				if($_REQUEST['save'] == 1) {
					$error = '';
					
					if($_REQUEST['type'] == '') $error.= 'Es muss ein Typ ausgew&auml;hlt werden.<br>';
					if($_REQUEST['city'] == '') $error.= 'Es muss eine Stadt ausgew&auml;hlt werden.<br>';
					if($_REQUEST['time_start'] == '') $error.= 'Es muss ein Starttermin angegeben werden.<br>';
					if($_REQUEST['time_end'] == '') $error.= 'Es muss ein Endtermin angegeben werden.<br>';
					if($_REQUEST['time'] == '') $error.= 'Es m&uuml;ssen Seminarzeiten angegeben werden.<br>';
					
					if($error == '') {
						$tmp_s = explode('.',$_REQUEST['time_start']);
						$time_start = mktime(0,0,0,$tmp_s[1],$tmp_s[0],$tmp_s[2]);
						
						$tmp_e = explode('.',$_REQUEST['time_end']);
						$time_end = mktime(0,0,0,$tmp_e[1],$tmp_e[0],$tmp_e[2]);
											
						$active = ($_REQUEST['active'] == 1) ? 1 : 0;
												
						if($_REQUEST['action'] == 'new') {
							$res = $db->Query("INSERT INTO ".PREFIX."_seminars(uid,ctime,cid,tid,time_start,time_end,time,active)
												VALUES (".UID.",".time().",".escs($_REQUEST['city']).",".escs($_REQUEST['type']).",".$time_start.",".$time_end.",'".escs($_REQUEST['time'])."',".$active.")");
						}
						else {
							$res = $db->Query("UPDATE ".PREFIX."_seminars SET cid = ".escs($_REQUEST['city']).", tid = ".escs($_REQUEST['type']).", time_start = ".$time_start.", time_end = ".$time_end.", time = '".escs($_REQUEST['time'])."', active = ".$active." WHERE id = ".$row->id);
						}
						
						// Cache leeren
						clearcache(BASEDIR.'temp/');
						
						if($res) header('location: '.$admindomain.'index.php?p=seminars&action=overview');
						else $tmpl->assign('error', 'Es trat ein Fehler beim Speichern der Daten auf. Bitte versuchen Sie es erneut.');		
					}
					else $tmpl->assign('error', $error);
				}

				// Städte und dazugehöriges Bundesland ermitteln
				$sql_cities = $db->Query("SELECT c.id, c.title AS city, s.title AS state FROM ".PREFIX."_seminars_city AS c JOIN ".PREFIX."_seminars_state AS s ON (s.id = c.sid) ORDER BY c.title ASC");
				$cities = array();
				while($row_cities = $sql_cities->fetchrow()) {
					array_push($cities, $row_cities);
				}
				$tmpl->assign('cities', $cities);
				
				// Seminartypen ermitteln
				$sql_types = $db->Query("SELECT * FROM ".PREFIX."_seminars_type ORDER BY title ASC");
				$types = array();
				while($row_types = $sql_types->fetchrow()) {
					array_push($types, $row_types);
				}
				$tmpl->assign('types', $types);
					
				$tmpl->assign('content', $tmpl->fetch('seminars/form.tpl'));
			}
		break;
				
		//===================================
		// Seminar aktivieren / deaktivieren
		//===================================
		case "active":
			
			if(!permission($_REQUEST['p'],$_REQUEST['action'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				if($_REQUEST['id'] != '' && $_REQUEST['option'] != '') {
						
					$db->Query("UPDATE ".PREFIX."_seminars SET active = ".escs($_REQUEST['option'])." WHERE id = ".escs($_REQUEST['id']));

					// Cache leeren
					clearcache(BASEDIR.'temp/');
				}
				
				header('location: index.php?p=seminars&action=overview&pp='.$_REQUEST['pp'].'&page='.$_REQUEST['page']);
			}
		break;
		
		//===================================
		// Seminar löschen
		//===================================
		case "delete":
		
			if(!permission($_REQUEST['p'],$_REQUEST['action'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				if($_REQUEST['id'] != '') {
						
					// Datenbankeintrag entfernen
					$db->Query("DELETE FROM ".PREFIX."_seminars WHERE id = ".escs($_REQUEST['id']));
					
					// Cache leeren
					clearcache(BASEDIR.'temp/');
				}
				
				header('location: index.php?p=seminars&action=overview&pp='.$_REQUEST['pp'].'&page='.$_REQUEST['page']);
			}
		break;
		
		//===================================
		// Übersicht Bundesländer
		//===================================
		case "state":
			
			if(!permission($_REQUEST['p'],$_REQUEST['action'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				// Anzahl Bundesländer für Navigation ermitteln
				$sql_num = $db->Query("SELECT id FROM ".PREFIX."_seminars_state");
				$num = $sql_num->numrows();
				$tmpl->assign('num', $num);
				
				$limit = ($_REQUEST['pp'] == '') ? 20 : escs($_REQUEST['pp']);
				$page = ($_REQUEST['page'] == '') ? 1 : escs($_REQUEST['page']);
				$a = $page * $limit - $limit;
				
				// Navigation erzeugen
				if($num > $limit) {
					$link = "index.php?p=".$_REQUEST['p']."&amp;action=".$_REQUEST['action']."&amp;pp=".$limit."&amp;page={s}";
					$seiten = ceil($num / $limit);
					$tmpl->assign('navi', pagenav_tpl($seiten, $link, 4, 4, 1));
				}
				
				// Bundesländer  ermitteln
				$sql = $db->Query("SELECT * FROM ".PREFIX."_seminars_state ORDER BY title ASC LIMIT $a, $limit");
				$states = array();
				while($row = $sql->fetchrow()) {
					// Städte ermitteln
					$sql_cities = $db->Query("SELECT title FROM ".PREFIX."_seminars_city WHERE sid = ".$row->id);
					$cities = array();
					while($row_cities = $sql_cities->fetchrow()) {
						array_push($cities, $row_cities);					
					}
					$row->cities = $cities;
					
					array_push($states, $row);
				}
				$tmpl->assign('states', $states);
				
				$tmpl->assign('content', $tmpl->fetch('seminars/state_overview.tpl'));	
			}
		break;
		
		//===================================
		// Bundesland anlegen / bearbeiten
		//===================================
		case "state_new":
		case "state_edit":
			
			if(!permission($_REQUEST['p'],'state')) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				if(isset($_REQUEST['id']) && $_REQUEST['id'] != '' && $_REQUEST['id'] != 0) {
					$sql = $db->Query("SELECT * FROM ".PREFIX."_seminars_state WHERE id = ".escs($_REQUEST['id']));
					$row = $sql->fetchrow();
					$tmpl->assign('row', $row);
				}
				
				if($_REQUEST['save'] == 1) {
					$error = '';
					
					if($_REQUEST['title'] == '') $error.= 'Es muss ein Titel angegeben werden.<br>';
					
					if($error == '') {
						
						if($_REQUEST['action'] == 'state_new') {
							$res = $db->Query("INSERT INTO ".PREFIX."_seminars_state(title) VALUES ('".escs($_REQUEST['title'])."')");
						}
						else {
							$res = $db->Query("UPDATE ".PREFIX."_seminars_state SET title = '".escs($_REQUEST['title'])."' WHERE id = ".$row->id);
						}			
						
						// Cache leeren
						clearcache(BASEDIR.'temp/');
						
						if($res) header('location: '.$admindomain.'index.php?p=seminars&action=state');
						else $tmpl->assign('error', 'Es trat ein Fehler beim Speichern der Daten auf. Bitte versuchen Sie es erneut.');	
					}
					else $tmpl->assign('error', $error);
				}					
				
				$tmpl->assign('content', $tmpl->fetch('seminars/state_form.tpl'));	
			}
		break;
		
		//===================================
		// Bundesland löschen
		//===================================
		case "state_delete":
		
			if(!permission($_REQUEST['p'],'state')) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				if($_REQUEST['id'] != '') {
						
					// Datenbankeintrag entfernen
					$db->Query("DELETE FROM ".PREFIX."_seminars_state WHERE id = ".escs($_REQUEST['id']));
					
					// Cache leeren
					clearcache(BASEDIR.'temp/');
				}
				
				header('location: index.php?p=seminars&action=state&pp='.$_REQUEST['pp'].'&page='.$_REQUEST['page']);
			}
		break;
		
		//===================================
		// Übersicht Städte
		//===================================
		case "city":
			
			if(!permission($_REQUEST['p'],$_REQUEST['action'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				// Anzahl Städte für Navigation ermitteln
				$sql_num = $db->Query("SELECT id FROM ".PREFIX."_seminars_city");
				$num = $sql_num->numrows();
				$tmpl->assign('num', $num);
				
				$limit = ($_REQUEST['pp'] == '') ? 20 : escs($_REQUEST['pp']);
				$page = ($_REQUEST['page'] == '') ? 1 : escs($_REQUEST['page']);
				$a = $page * $limit - $limit;
				
				// Navigation erzeugen
				if($num > $limit) {
					$link = "index.php?p=".$_REQUEST['p']."&amp;action=".$_REQUEST['action']."&amp;pp=".$limit."&amp;page={s}";
					$seiten = ceil($num / $limit);
					$tmpl->assign('navi', pagenav_tpl($seiten, $link, 4, 4, 1));
				}
				
				// Städte  ermitteln
				$sql = $db->Query("SELECT * FROM ".PREFIX."_seminars_city ORDER BY title ASC LIMIT $a, $limit");
				$cities = array();
				while($row = $sql->fetchrow()) {
					// Bundesland ermitteln
					$sql_state = $db->Query("SELECT title FROM ".PREFIX."_seminars_state WHERE id = ".$row->sid);
					$row_state= $sql_state->fetchrow();
					$row->state = $row_state->title;
					
					array_push($cities, $row);
				}
				$tmpl->assign('cities', $cities);
		
				$tmpl->assign('content', $tmpl->fetch('seminars/city_overview.tpl'));	
			}
		break;
		
		//===================================
		// Stadt anlegen / bearbeiten
		//===================================
		case "city_new":
		case "city_edit":
			
			if(!permission($_REQUEST['p'],'city')) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				if(isset($_REQUEST['id']) && $_REQUEST['id'] != '' && $_REQUEST['id'] != 0) {
					$sql = $db->Query("SELECT * FROM ".PREFIX."_seminars_city WHERE id = ".escs($_REQUEST['id']));
					$row = $sql->fetchrow();
					$tmpl->assign('row', $row);
				}
				
				if($_REQUEST['save'] == 1) {
					$error = '';
					
					if($_REQUEST['title'] == '') $error.= 'Es muss ein Titel angegeben werden.<br>';
					if($_REQUEST['state'] == '') $error.= 'Es muss ein Bundesland ausgew&auml;hlt werden.<br>';
										
					if($error == '') {
						
						if($_REQUEST['action'] == 'city_new') {
							$res = $db->Query("INSERT INTO ".PREFIX."_seminars_city(sid,title) VALUES (".escs($_REQUEST['state']).",'".escs($_REQUEST['title'])."')");
						}
						else {
							$res = $db->Query("UPDATE ".PREFIX."_seminars_city SET sid = ".escs($_REQUEST['state']).", title = '".escs($_REQUEST['title'])."' WHERE id = ".$row->id);
						}
						
						// Cache leeren
						clearcache(BASEDIR.'temp/');
						
						if($res) header('location: '.$admindomain.'index.php?p=seminars&action=city');
						else $tmpl->assign('error', 'Es trat ein Fehler beim Speichern der Daten auf. Bitte versuchen Sie es erneut.');	
					}
					else $tmpl->assign('error', $error);
				}
				
				// Bundesländer ermitteln
				$sql_states = $db->Query("SELECT * FROM ".PREFIX."_seminars_state ORDER BY title ASC");
				$states = array();
				while($row_states = $sql_states->fetchrow()) {
					array_push($states, $row_states);
				}
				$tmpl->assign('states', $states);
				
				$tmpl->assign('content', $tmpl->fetch('seminars/city_form.tpl'));	
			}
		break;
		
		//===================================
		// Stadt löschen
		//===================================
		case "city_delete":
		
			if(!permission($_REQUEST['p'],'city')) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				if($_REQUEST['id'] != '') {
						
					// Datenbankeintrag entfernen
					$db->Query("DELETE FROM ".PREFIX."_seminars_city WHERE id = ".escs($_REQUEST['id']));
					
					// Cache leeren
					clearcache(BASEDIR.'temp/');
				}
				
				header('location: index.php?p=seminars&action=city&pp='.$_REQUEST['pp'].'&page='.$_REQUEST['page']);
			}
		break;
		
		//===================================
		// Übersicht Seminartypen
		//===================================
		case "type":
			
			if(!permission($_REQUEST['p'],$_REQUEST['action'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				// Anzahl Seminartypen für Navigation ermitteln
				$sql_num = $db->Query("SELECT id FROM ".PREFIX."_seminars_type");
				$num = $sql_num->numrows();
				$tmpl->assign('num', $num);
				
				$limit = ($_REQUEST['pp'] == '') ? 20 : escs($_REQUEST['pp']);
				$page = ($_REQUEST['page'] == '') ? 1 : escs($_REQUEST['page']);
				$a = $page * $limit - $limit;
				
				// Navigation erzeugen
				if($num > $limit) {
					$link = "index.php?p=".$_REQUEST['p']."&amp;action=".$_REQUEST['action']."&amp;pp=".$limit."&amp;page={s}";
					$seiten = ceil($num / $limit);
					$tmpl->assign('navi', pagenav_tpl($seiten, $link, 4, 4, 1));
				}
				
				// News  ermitteln
				$sql = $db->Query("SELECT * FROM ".PREFIX."_seminars_type ORDER BY title ASC LIMIT $a, $limit");
				$types = array();
				while($row = $sql->fetchrow()) {					
					array_push($types, $row);
				}
				$tmpl->assign('types', $types);
		
				$tmpl->assign('content', $tmpl->fetch('seminars/type_overview.tpl'));	
			}
		break;
		
		//===================================
		// Seminartyp anlegen / bearbeiten
		//===================================
		case "type_new":
		case "type_edit":
			
			if(!permission($_REQUEST['p'],'type')) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				if(isset($_REQUEST['id']) && $_REQUEST['id'] != '' && $_REQUEST['id'] != 0) {
					$sql = $db->Query("SELECT * FROM ".PREFIX."_seminars_type WHERE id = ".escs($_REQUEST['id']));
					$row = $sql->fetchrow();
					$tmpl->assign('row', $row);
				}
				
				if($_REQUEST['save'] == 1) {
					$error = '';
					
					if($_REQUEST['title'] == '') $error.= 'Es muss ein Titel angegeben werden.<br>';
					#if($_REQUEST['type'] == '') $error.= 'Es muss ein Typ angegeben werden.<br>';
										
					if($error == '') {
						
						if($_REQUEST['action'] == 'type_new') {
							$res = $db->Query("INSERT INTO ".PREFIX."_seminars_type(title,shorttitle,type) VALUES ('".escs($_REQUEST['title'])."','".escs($_REQUEST['shorttitle'])."','".escs($_REQUEST['type'])."')");
						}
						else {
							$res = $db->Query("UPDATE ".PREFIX."_seminars_type SET title = '".escs($_REQUEST['title'])."', shorttitle = '".escs($_REQUEST['shorttitle'])."', type = '".escs($_REQUEST['type'])."' WHERE id = ".$row->id);
						}
						
						// Cache leeren
						clearcache(BASEDIR.'temp/');
						
						if($res) header('location: '.$admindomain.'index.php?p=seminars&action=type');
						else $tmpl->assign('error', 'Es trat ein Fehler beim Speichern der Daten auf. Bitte versuchen Sie es erneut.');	
					}
					else $tmpl->assign('error', $error);
				}
				
				$tmpl->assign('content', $tmpl->fetch('seminars/type_form.tpl'));	
			}
		break;
		
		//===================================
		// Seminartyp löschen
		//===================================
		case "type_delete":
		
			if(!permission($_REQUEST['p'],'type')) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				if($_REQUEST['id'] != '') {
						
					// Datenbankeintrag entfernen
					$db->Query("DELETE FROM ".PREFIX."_seminars_type WHERE id = ".escs($_REQUEST['id']));
					
					// Cache leeren
					clearcache(BASEDIR.'temp/');
				}
				
				header('location: index.php?p=seminars&action=type&pp='.$_REQUEST['pp'].'&page='.$_REQUEST['page']);
			}
		break;
	}
}
?>