<?
/*
 * Benutzer & Gruppen verwalten
 *
 * by Sven Jungnickel
 */

if(!defined("BASEDIR")) exit;

if(!permission($_REQUEST['p'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
else {	
	
	switch($_REQUEST['action']) {
	
		//===================================
		// Übersicht Benutzer
		//===================================
		default:
		case "":
		case "overview":
			
			// Anzahl Benutzer für Navigation ermitteln
			$sql_num = mysql_query("SELECT id FROM ".PREFIX."_admin_user ORDER BY id DESC");
			$num = mysql_num_rows($sql_num);
			$tmpl->assign('num', $num);
		
			$limit = ($_REQUEST['pp'] == '') ? 20 : escs($_REQUEST['pp']);
			$page = ($_REQUEST['page'] == '') ? 1 : escs($_REQUEST['page']);
			$a = $page * $limit - $limit;
			
			// Navigation erzeugen
			if($num > $limit) {
				$link = "index.php?p=user&amp;action=overview&&amp;pp=".$limit."&amp;page={s}";
				$seiten = ceil($num / $limit);
				$tmpl->assign('navi', pagenav_tpl($seiten, $link, 4, 4, 1));
			}
			
			$sql = mysql_query("SELECT * FROM ".PREFIX."_admin_user ORDER BY id DESC LIMIT $a, $limit");
			$user = array();
			while($row = mysql_fetch_assoc($sql)) {			
				// Gruppe ermitteln
				$sql_group = mysql_query("SELECT name FROM ".PREFIX."_admin_groups WHERE id = ".$row['ugroup']);
				$row_group = mysql_fetch_assoc($sql_group);
				$row['group'] = $row_group['name'];
							
				array_push($user, $row);
			}
			$tmpl->assign('user', $user);
			
			$tmpl->assign('content', $tmpl->fetch('user/overview.tpl'));	
		break;
		
		//===================================
		// neuen Benutzer anlegen
		//===================================
		case "new_user":
			
			if($_REQUEST['save'] == 1) {
				$error = '';
				
				if($_REQUEST['uname'] == '') $error.= 'Es muss ein Benutzername angegeben werden.<br>';
				if($_REQUEST['email'] == '') $error.= 'Es muss eine Email Adresse angegeben werden.<br>';
				if($_REQUEST['authortoken'] == '') $error.= 'Es muss ein Autorenk&uuml;rzel angegeben werden.<br>';
				if($_REQUEST['ugroup'] == '') $error.= 'Es muss eine Gruppe ausgew&auml;hlt werden.<br>';
				if($_REQUEST['password'] == '') $error.= 'Es muss ein Passwort angegeben werden.<br>';
				if($_REQUEST['password_retry'] == '') $error.= 'Das Passwort muss wiederholt werden.<br>';
				if($_REQUEST['password'] != $_REQUEST['password_retry']) $error.= 'Die Passw&ouml;rter stimmen nicht &uuml;berein.<br>';
				
				if($error == '') {
					$password = md5($_REQUEST['password']);
					
					$insert = mysql_query("INSERT INTO ".PREFIX."_admin_user (uname,ugroup,password,active,email,authortoken)
										 VALUES('".escs($_REQUEST['uname'])."',".escs($_REQUEST['ugroup']).",'".$password."',1,'".escs($_REQUEST['email'])."','".escs($_REQUEST['authortoken'])."')");
					
					if($insert) header('location: index.php?p=user&action=overview');
					else $tmpl->assign('error', 'Es trat ein Fehler beim Speichern der Daten auf. Bitte versuchen Sie es erneut.');		
				}
				else $tmpl->assign('error', $error);
			}
			
			// Alle Gruppen ermitteln
			$sql_group = mysql_query("SELECT id, name FROM ".PREFIX."_admin_groups");
			$groups = array();
			while($row_group = mysql_fetch_assoc($sql_group)) {
				array_push($groups, $row_group);
			}
			$tmpl->assign('groups', $groups);
		
			$tmpl->assign('content', $tmpl->fetch('user/form_user.tpl'));	
		break;
		
		//===================================
		// Benutzer bearbeiten
		//===================================
		case "edit_user":
			
			// Alle Benutzerdatem ermitteln
			$sql = mysql_query("SELECT * FROM ".PREFIX."_admin_user WHERE id = ".escs($_REQUEST['id']));
			$row = mysql_fetch_assoc($sql);
			$tmpl->assign('row', $row);
			
			if($_REQUEST['save'] == 1) {
				$error = '';
				
				if($_REQUEST['uname'] == '') $error.= 'Es muss ein Benutzername angegeben werden.<br>';
				if($_REQUEST['email'] == '') $error.= 'Es muss eine Email Adresse angegeben werden.<br>';
				if($_REQUEST['authortoken'] == '') $error.= 'Es muss ein Autorenk&uuml;rzel angegeben werden.<br>';
				if($_REQUEST['ugroup'] == '') $error.= 'Es muss eine Gruppe ausgew&auml;hlt werden.<br>';
				if($_REQUEST['password_new'] != '' && $_REQUEST['password_retry'] == '') $error.= 'Das Passwort muss wiederholt werden.<br>';
				if($_REQUEST['password_new'] != '' && $_REQUEST['password_retry'] != '' && $_REQUEST['password_new'] != $_REQUEST['password_retry']) $error.= 'Die Passw&ouml;rter stimmen nicht &uuml;berein.<br>';
				
				if($error == '') {
					if($_REQUEST['password_new'] != '') $password = md5($_REQUEST['password_new']);
					else $password = $row['password'];
					$active = ($_REQUEST['active'] == 1) ? 1 : 0;
					if($row['id'] == UID) $active = 1;
					
					$update = mysql_query("UPDATE ".PREFIX."_admin_user SET uname = '".escs($_REQUEST['uname'])."', ugroup = ".escs($_REQUEST['ugroup']).",
											 password = '".$password."', active = ".$active.", email = '".escs($_REQUEST['email'])."', authortoken = '".escs($_REQUEST['authortoken'])."' WHERE id = ".$row['id']);

					if($update) header('location: index.php?p=user&action=overview&pp='.$_REQUEST['pp'].'&page='.$_REQUEST['page']);
					else $tmpl->assign('error', 'Es trat ein Fehler beim Speichern der Daten auf. Bitte versuchen Sie es erneut.');		
				}
				else $tmpl->assign('error', $error);
			}
			
			// Alle Gruppen ermitteln
			$sql_group = mysql_query("SELECT id, name FROM ".PREFIX."_admin_groups");
			$groups = array();
			while($row_group = mysql_fetch_assoc($sql_group)) {
				array_push($groups, $row_group);
			}
			$tmpl->assign('groups', $groups);
		
			$tmpl->assign('content', $tmpl->fetch('user/form_user.tpl'));	
		break;
		
		//===================================
		// Benutzer aktivieren / deaktivieren
		//===================================
		case "active_user":
		
			if($_REQUEST['id'] != '' && $_REQUEST['id'] != UID && $_REQUEST['option'] != '') {
				mysql_query("UPDATE ".PREFIX."_admin_user SET active = ".escs($_REQUEST['option'])." WHERE id = ".escs($_REQUEST['id']));		
			}
			
			header('location: index.php?p=user&action=overview&pp='.$_REQUEST['pp'].'&page='.$_REQUEST['page']);		
		break;
		
		//===================================
		// Benutzer löschen
		//===================================
		case "del_user":
		
			if($_REQUEST['id'] != '' && $_REQUEST['id'] != UID) {
				mysql_query("DELETE FROM ".PREFIX."_admin_user WHERE id = ".escs($_REQUEST['id']));
			}
			
			header('location: index.php?p=user&action=overview&pp='.$_REQUEST['pp'].'&page='.$_REQUEST['page']);				
		break;
		
		//===================================
		// Gruppen Übersicht
		//===================================
		case "groups":
		
			// Alle Gruppen ermitteln
			$sql = mysql_query("SELECT * FROM ".PREFIX."_admin_groups ORDER BY id ASC");
			$groups = array();
			while($row = mysql_fetch_assoc($sql)) {
				// Anzahl Benutzer ermitteln
				$sql_user = mysql_query("SELECT id FROM ".PREFIX."_admin_user WHERE ugroup = ".$row['id']);
				$row['user'] = mysql_num_rows($sql_user);
				
				array_push($groups, $row);
			}
			$tmpl->assign('groups', $groups);
		
			$tmpl->assign('content', $tmpl->fetch('user/groups.tpl'));	
		break;
		
		//===================================
		// neue Gruppe anlegen
		//===================================
		case "new_group":
			
			if($_REQUEST['save'] == 1) {
				$error = '';
				
				if($_REQUEST['name'] == '') $error.= 'Es muss ein Gruppenname angegeben werden.<br>';
				if(count($_REQUEST['modul']) == 0) $error.= 'Es m&uuml;ssen Berechtigungen gesetzt werden.<br>';
				
				if($error == '') {				
					$insert = mysql_query("INSERT INTO ".PREFIX."_admin_groups (name) VALUES('".escs($_REQUEST['name'])."')");
					$insert_id = mysql_insert_id();
					
					// Berechtigungen speichern
					foreach($_REQUEST['modul'] as $modul) {
						mysql_query("INSERT INTO ".PREFIX."_admin_permissions(group_id,modul_id) VALUES(".$insert_id.",".$modul.")");
					}
					
					if($insert) header('location: index.php?p=user&action=groups');
					else $tmpl->assign('error', 'Es trat ein Fehler beim Speichern der Daten auf. Bitte versuchen Sie es erneut.');		
				}
				else $tmpl->assign('error', $error);
			}
			
			// Alle Module ermitteln
			$sql_moduls = mysql_query("SELECT * FROM ".PREFIX."_admin_modul WHERE pid = 0 ORDER BY ID ASC");
			$moduls = array();
			while($row_moduls = mysql_fetch_assoc($sql_moduls)) {
				// Modulfunktionen ermitten
				$sql_mod_actions = mysql_query("SELECT * FROM ".PREFIX."_admin_modul WHERE pid = ".$row_moduls['id']." ORDER BY title ASC");
				$actions = array();
				while($row_mod_actions = mysql_fetch_assoc($sql_mod_actions)) {
					array_push($actions, $row_mod_actions);
				}
				$row_moduls['actions'] = $actions;
				
				array_push($moduls, $row_moduls);
			}
			$tmpl->assign('moduls', $moduls);
					
			$tmpl->assign('content', $tmpl->fetch('user/form_group.tpl'));	
		break;
		
		//===================================
		// Gruppe bearbeiten
		//===================================
		case "edit_group":
			
			// Gruppendaten ermitteln
			$sql = mysql_query("SELECT * FROM ".PREFIX."_admin_groups WHERE id = ".escs($_REQUEST['id']));
			$row = mysql_fetch_assoc($sql);
			$tmpl->assign('group', $row);
			
			if($_REQUEST['save'] == 1) {
				$error = '';
				
				if($_REQUEST['name'] == '') $error.= 'Es muss ein Gruppenname angegeben werden.<br>';
				if(count($_REQUEST['modul']) == 0 && $row['id'] > 1) $error.= 'Es muss mindestens ein Modul ausgew&auml;hlt werden.<br>';
				
				if($error == '') {				
					$update = mysql_query("UPDATE ".PREFIX."_admin_groups SET name = '".escs($_REQUEST['name'])."' WHERE id = ".$row['id']);
					
					// Berechtigungen speichern
					if($row['id'] > 1) {
						mysql_query("DELETE FROM ".PREFIX."_admin_permissions WHERE group_id = ".$row['id']);
						foreach($_REQUEST['modul'] as $modul) {
							mysql_query("INSERT INTO ".PREFIX."_admin_permissions(group_id,modul_id) VALUES(".$row['id'].",".$modul.")");
						}
					}
					
					if($update) header('location: index.php?p=user&action=groups');
					else $tmpl->assign('error', 'Es trat ein Fehler beim Speichern der Daten auf. Bitte versuchen Sie es erneut.');		
				}
				else $tmpl->assign('error', $error);
			}
			
			// Alle Module ermitteln
			$sql_moduls = mysql_query("SELECT * FROM ".PREFIX."_admin_modul WHERE pid = 0 ORDER BY ID ASC");
			$moduls = array();
			while($row_moduls = mysql_fetch_assoc($sql_moduls)) {
				// Modulfunktionen ermitten
				$sql_mod_actions = mysql_query("SELECT * FROM ".PREFIX."_admin_modul WHERE pid = ".$row_moduls['id']." ORDER BY title ASC");
				$actions = array();
				while($row_mod_actions = mysql_fetch_assoc($sql_mod_actions)) {
					array_push($actions, $row_mod_actions);
				}
				$row_moduls['actions'] = $actions;
				
				array_push($moduls, $row_moduls);
			}
			$tmpl->assign('moduls', $moduls);
						
			// Berechtigunen der Gruppe ermitteln
			$sql_permissions = mysql_query("SELECT * FROM ".PREFIX."_admin_permissions WHERE group_id = ".escs($_REQUEST['id']));
			$permissions = array();
			while($row_permissions = mysql_fetch_assoc($sql_permissions)) {
				array_push($permissions, $row_permissions['modul_id']);
			}
			$tmpl->assign('permissions', $permissions);
			
			$tmpl->assign('content', $tmpl->fetch('user/form_group.tpl'));	
		break;
		
		//===================================
		// Gruppe löschen
		//===================================
		case "del_group":
		
			if($_REQUEST['id'] != '' && $_REQUEST['id'] > 1) {
				mysql_query("DELETE FROM ".PREFIX."_admin_groups WHERE id = ".escs($_REQUEST['id']));
				
				header('location: index.php?p=user&action=groups');	
			}
			else header('location: index.php?p=user&action=groups&nodel=1');				
		break;
	}
}
?>