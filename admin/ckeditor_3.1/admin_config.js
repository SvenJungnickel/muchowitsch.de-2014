/*
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	config.language = 'de';
	config.height = '300';
	config.enterMode = CKEDITOR.ENTER_BR;
	config.shiftEnterMode = CKEDITOR.ENTER_P;
	
	config.toolbar_Basic =
	[
		[ 'Source', '-', 'Preview', '-', 'Bold', 'Italic', 'Underline', 'Subscript','Superscript', '-' ],
		['NumberedList', 'BulletedList', '-', 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
		['Link','Unlink','-', 'Table', 'HorizontalRule', 'SpecialChar'],
		'/',
		['Styles','Format','Font','FontSize', 'TextColor','BGColor', '-', 'Maximize']
	];
	config.toolbar = 'Basic';	
};
