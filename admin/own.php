<?
/*
 * Eigene Benutzerdaten ändern
 *
 * by Sven Jungnickel
 */

if(!defined("BASEDIR")) exit;

switch($_REQUEST['action']) {

	//===================================
	// eigene Benutzerdaten bearbeiten
	//===================================
	default:
	case "":
	case "edit_user":
						
		// Alle Benutzerdatem ermitteln
		$sql = mysql_query("SELECT * FROM ".PREFIX."_admin_user WHERE id = ".UID);
		$row = mysql_fetch_assoc($sql);
		$tmpl->assign('row', $row);
		
		if($_REQUEST['save'] == 1) {
			$error = '';
			
			if($_REQUEST['uname'] == '') $error.= 'Es muss ein Benutzername angegeben werden.<br>';
			if($_REQUEST['email'] == '') $error.= 'Es muss eine Email Adresse angegeben werden.<br>';
			if($_REQUEST['authortoken'] == '') $error.= 'Es muss ein Autorenk&uuml;rzel angegeben werden.<br>';
			if($_REQUEST['password_new'] != '' && $_REQUEST['password_retry'] == '') $error.= 'Das Passwort muss wiederholt werden.<br>';
			if($_REQUEST['password_new'] != '' && $_REQUEST['password_retry'] != '' && $_REQUEST['password_new'] != $_REQUEST['password_retry']) $error.= 'Die Passw&ouml;rter stimmen nicht &uuml;berein.<br>';
			if($_REQUEST['password_old'] == '') $error.= 'Aus Sicherheitsgr&uuml;nden muss das alte Passwort eingegeben werden.<br>';
			
			if($error == '') {
				$sql_passcheck = mysql_query("SELECT * FROM ".PREFIX."_admin_user WHERE password = '".md5($_REQUEST['password_old'])."' AND id = ".UID);
				if(mysql_num_rows($sql_passcheck) == 0) $tmpl->assign('error', 'Das alte Passwort stimmt nicht &uuml;berein.');				
				else { 
					if($_REQUEST['password_new'] != '') $password = md5($_REQUEST['password_new']);
					else $password = $row['password'];
					$active = ($_REQUEST['active'] == 1) ? 1 : 0;
					
					$update = mysql_query("UPDATE ".PREFIX."_admin_user SET uname = '".escs($_REQUEST['uname'])."', password = '".$password."', email = '".escs($_REQUEST['email'])."', authortoken = '".escs($_REQUEST['authortoken'])."' WHERE id = ".$row['id']);
					
					if($update) header('location: index.php?p=own&edit=1&pp='.$_REQUEST['pp'].'&page='.$_REQUEST['page']);
					else $tmpl->assign('error', 'Es trat ein Fehler beim Speichern der Daten auf. Bitte versuchen Sie es erneut.');		
				}
			}
			else $tmpl->assign('error', $error);
		}
	
		$tmpl->assign('content', $tmpl->fetch('user/form_user.tpl'));	
	break;		
}
?>