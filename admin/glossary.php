<?
/*
 * Glossar verwalten
 *
 * by Sven Jungnickel
 */

if(!defined("BASEDIR")) exit;

if(!permission($_REQUEST['p'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
else {

	switch($_REQUEST['action']) {
	
		//===================================
		// Übersicht Glossar
		//===================================
		default:
		case "":
		case "overview":
			
			if(!permission($_REQUEST['p'],$_REQUEST['action'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				// Array aller Buchstaben erstellen
				$alphabet = array('Alle');
				for($i = 'A'; $i < 'Z'; $i++) {
					array_push($alphabet, $i);
				}
				array_push($alphabet, 'Z');
				$tmpl->assign('alphabet', $alphabet);
				
				// Sortieren
				if($_REQUEST['sort'] != '' && $_REQUEST['sort'] != 'Alle' && $_REQUEST['sort'] != 'ALLE') {
					$andsort = " WHERE UPPER(title) LIKE '".strtoupper(escs($_REQUEST['sort']))."%'";
				}
				
				// Anzahl Glossareinträge für Navigation ermitteln
				$sql_num = $db->Query("SELECT id FROM ".PREFIX."_glossary $andsort");
				$num = $sql_num->numrows();
				$tmpl->assign('num', $num);
				
				$limit = ($_REQUEST['pp'] == '') ? 20 : escs($_REQUEST['pp']);
				$page = ($_REQUEST['page'] == '') ? 1 : escs($_REQUEST['page']);
				$a = $page * $limit - $limit;
				
				// Navigation erzeugen
				if($num > $limit) {
					$link = "index.php?p=glossary&amp;action=overview&amp;pp=".$limit."&amp;page={s}&amp;sort=".escs($_REQUEST['sort']);
					$seiten = ceil($num / $limit);
					$tmpl->assign('navi', pagenav_tpl($seiten, $link, 4, 4, 1));
				}
				
				// Glossareinträge ermitteln
				$sql = $db->Query("SELECT * FROM ".PREFIX."_glossary $andsort ORDER BY title ASC LIMIT $a, $limit");
				$glossary = array();
				while($row = $sql->fetchrow()) {
					// Nach Anfangsbuchstaben gruppieren
					$firstletter = substr(strtoupper($row->title),0,1);
					if($firstletter != $fist_old) $row->firstletter = 1;
					$fist_old = $firstletter;
					
					array_push($glossary, $row);
				}
				$tmpl->assign('glossary', $glossary);
		
				$tmpl->assign('content', $tmpl->fetch('glossary/overview.tpl'));	
			}
		break;
		
		//===================================
		// neuer Glossareintrag / bearbeiten
		//===================================
		case "new":
		case "edit":
		
			if(!permission($_REQUEST['p'],$_REQUEST['action'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				// Daten ermitteln
				if($_REQUEST['action'] != 'new' && $_REQUEST['id'] != '') {
					$sql = $db->Query("SELECT * FROM ".PREFIX."_glossary WHERE id = ".escs($_REQUEST['id']));
					$row = $sql->fetchrow();
					$tmpl->assign('row', $row);
				}
				
				if($_REQUEST['save'] == 1) {
					$error = '';
					
					if($_REQUEST['title'] == '') $error.= 'Es muss der Titel angegeben werden.<br>';
					if($_REQUEST['descr'] == '') $error.= 'Es muss die Beschreibung eingegeben werden.<br>';
					
					if($error == '') {
						$descr = stripslashes($_REQUEST['descr']);
						$active = ($_REQUEST['active'] == 1) ? 1 : 0;
						
						if($_REQUEST['action'] == 'new') {
							$sql = $db->Query("INSERT INTO ".PREFIX."_glossary (title,descr,active)
												 VALUES('".escs($_REQUEST['title'])."','".$descr."',".$active.")");
						}
						else {
							$sql = $db->Query("UPDATE ".PREFIX."_glossary SET title = '".escs($_REQUEST['title'])."', descr = '".$descr."', active = ".$active." WHERE id = ".$row->id);
						}
						// Cache leeren
						clearcache(BASEDIR.'temp/');
						
						if($sql) header('location: '.$admindomain.'index.php?p=glossary&action=overview&pp='.$_REQUEST['pp'].'&page='.$_REQUEST['page'].'&sort='.escs($_REQUEST['sort']));
						else $tmpl->assign('error', 'Es trat ein Fehler beim Speichern der Daten auf. Bitte versuchen Sie es erneut.');		
					}
					else $tmpl->assign('error', $error);
				}
				
				$tmpl->assign('content', $tmpl->fetch('glossary/form.tpl'));	
			}
		break;
		
		//===================================
		// Glossareintrag aktivieren / deaktivieren
		//===================================
		case "active":
			
			if(!permission($_REQUEST['p'],$_REQUEST['action'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				if($_REQUEST['id'] != '' && $_REQUEST['option'] != '') {
						
					$db->Query("UPDATE ".PREFIX."_glossary SET active = ".escs($_REQUEST['option'])." WHERE id = ".escs($_REQUEST['id']));

					// Cache leeren
					clearcache(BASEDIR.'temp/');
				}
				
				header('location: index.php?p=glossary&action=overview&pp='.$_REQUEST['pp'].'&page='.$_REQUEST['page'].'&sort='.escs($_REQUEST['sort']));
			}
		break;
		
		//===================================
		// Glossareintrag löschen
		//===================================
		case "delete":
		
			if(!permission($_REQUEST['p'],$_REQUEST['action'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				if($_REQUEST['id'] != '') {						
					// Datenbankeintrag entfernen
					$db->Query("DELETE FROM ".PREFIX."_glossary WHERE id = ".escs($_REQUEST['id']));
					
					// Cache leeren
					clearcache(BASEDIR.'temp/');
				}
				
				header('location: index.php?p=glossary&action=overview&pp='.$_REQUEST['pp'].'&page='.$_REQUEST['page'].'&sort='.escs($_REQUEST['sort']));
			}
		break;
	}
}
?>