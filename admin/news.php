<?
/*
 * Neuigkeiten verwalten
 *
 * by Sven Jungnickel
 */

if(!defined("BASEDIR")) exit;

if(!permission($_REQUEST['p'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
else {

	switch($_REQUEST['action']) {
	
		//===================================
		// Übersicht News
		//===================================
		default:
		case "":
		case "overview":
			
			if(!permission($_REQUEST['p'],$_REQUEST['action'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				// Anzahl News für Navigation ermitteln
				$sql_num = $db->Query("SELECT id FROM ".PREFIX."_news");
				$num = $sql_num->numrows();
				$tmpl->assign('num', $num);
				
				$limit = ($_REQUEST['pp'] == '') ? 20 : escs($_REQUEST['pp']);
				$page = ($_REQUEST['page'] == '') ? 1 : escs($_REQUEST['page']);
				$a = $page * $limit - $limit;
				
				// Navigation erzeugen
				if($num > $limit) {
					$link = "index.php?p=events&amp;action=overview&amp;pp=".$limit."&amp;page={s}";
					$seiten = ceil($num / $limit);
					$tmpl->assign('navi', pagenav_tpl($seiten, $link, 4, 4, 1));
				}
				
				// News  ermitteln
				$sql = $db->Query("SELECT * FROM ".PREFIX."_news ORDER BY ctime DESC LIMIT $a, $limit");
				$news = array();
				while($row = $sql->fetchrow()) {
					// Autoren ermitteln
					$sql_author = $db->Query("SELECT uname, authortoken FROM ".PREFIX."_admin_user WHERE id = ".$row->author);
					$row_author = $sql_author->fetchrow();
					$row->author = $row_author->uname.' ('.$row_author->authortoken.')';
					
					array_push($news, $row);
				}
				$tmpl->assign('news', $news);
		
				$tmpl->assign('content', $tmpl->fetch('news/overview.tpl'));	
			}
		break;
		
		//===================================
		// neue News anlegen
		//===================================
		case "new":
		
			if(!permission($_REQUEST['p'],$_REQUEST['action'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {				
				if($_REQUEST['save'] == 1) {
					$error = '';
					
					if($_REQUEST['title'] == '') $error.= 'Es muss Titel angegeben werden.<br>';
					if($_REQUEST['topic'] == '') $error.= 'Es muss ein Thema angegeben werden.<br>';
					if($_REQUEST['author'] == 0) $error.= 'Es muss der Autor angegeben werden.<br>';
					if($_REQUEST['teaser'] == '') $error.= 'Es muss ein Einleitungstext eingegeben werden.<br>';
					if($_REQUEST['text'] == '') $error.= 'Es muss der Inhalt eingegeben werden.<br>';
					
					if($error == '') {
						$text = stripslashes($_REQUEST['text']);
						$active = (permission($_REQUEST['p'],'active')) ? 1 : 0;
						
						$insert = $db->Query("INSERT INTO ".PREFIX."_news (uid,ctime,title,teaser,text,author,topic,active,metadescription,keywords)
											 VALUES(".UID.",".time().",'".escs($_REQUEST['title'])."','".escs($_REQUEST['teaser'])."','".$text."',".escs($_REQUEST['author']).",'".escs($_REQUEST['topic'])."',".$active.",'".escs($_REQUEST['metadescription'])."','".escs($_REQUEST['keywords'])."')");
						
						// Cache leeren
						clearcache(BASEDIR.'temp/');
						
						if($insert) header('location: '.$admindomain.'index.php?p=news&action=overview');
						else $tmpl->assign('error', 'Es trat ein Fehler beim Speichern der Daten auf. Bitte versuchen Sie es erneut.');		
					}
					else $tmpl->assign('error', $error);
				}
				
				// Autoren ermitteln
				$sql_author = $db->Query("SELECT id, uname, authortoken FROM ".PREFIX."_admin_user ORDER BY uname ASC");
				$authors = array();
				while($row_author = $sql_author->fetchrow()) {
					array_push($authors, $row_author);	
				}
				$tmpl->assign('authors', $authors);
				
				// CKEditor einbinden
				/*include_once(ADMINDIR.'/ckeditor/ckeditor.php');
				$CKEditor = new CKEditor();
				$CKEditor->basePath = '/admin/ckeditor/';
				$CKEditor->returnOutput = true;		
				$CKEditor->config['customConfig'] = 'admin_config.js';*/
				
				$text = ($error != '') ? stripslashes($_REQUEST['text']) : '';
				/*$tmpl->assign("text", $CKEditor->editor("text", $text));*/
				$tmpl->assign("text", $text);

				$tmpl->assign('content', $tmpl->fetch('news/form.tpl'));	
			}
		break;
		
		//===================================
		// News bearbeiten
		//===================================
		case "edit":
		
			if(!permission($_REQUEST['p'],$_REQUEST['action'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				$sql = $db->Query("SELECT * FROM ".PREFIX."_news WHERE id = ".escs($_REQUEST['id']));
				$row = $sql->fetchrow();
				
				if(!permission($_REQUEST['p'],'all') && $row->uid != UID) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
				else {
					$tmpl->assign('row', $row);
					
					if($_REQUEST['save'] == 1) {
						$error = '';
						
						if($_REQUEST['title'] == '') $error.= 'Es muss Titel angegeben werden.<br>';
						if($_REQUEST['topic'] == '') $error.= 'Es muss ein Thema angegeben werden.<br>';
						if($_REQUEST['author'] == 0) $error.= 'Es muss der Autor angegeben werden.<br>';
						if($_REQUEST['teaser'] == '') $error.= 'Es muss ein Einleitungstext eingegeben werden.<br>';
						if($_REQUEST['text'] == '') $error.= 'Es muss der Inhalt eingegeben werden.<br>';
						
						if($error == '') {	
							$text = stripslashes($_REQUEST['text']);
							$active = ($_REQUEST['active'] == 1) ? 1 : 0;
							
							$update = $db->Query("UPDATE ".PREFIX."_news SET active = ".$active.", title = '".escs($_REQUEST['title'])."', teaser = '".escs($_REQUEST['teaser'])."', text = '".$text."', author = ".escs($_REQUEST['author']).", topic = '".escs($_REQUEST['topic'])."', metadescription = '".escs($_REQUEST['metadescription'])."', keywords = '".escs($_REQUEST['keywords'])."' WHERE id = ".$row->id);
							
							// Cache leeren
							clearcache(BASEDIR.'temp/');
							
							if($update) header('location: '.$admindomain.'index.php?p=news&action=overview&pp='.$_REQUEST['pp'].'&page='.$_REQUEST['page']);
							else $tmpl->assign('error', 'Es trat ein Fehler beim Speichern der Daten auf. Bitte versuchen Sie es erneut.');		
						}
						else $tmpl->assign('error', $error);
					}	
					
					// Autoren ermitteln
					$sql_author = $db->Query("SELECT id, uname, authortoken FROM ".PREFIX."_admin_user ORDER BY uname ASC");
					$authors = array();
					while($row_author = $sql_author->fetchrow()) {
						array_push($authors, $row_author);	
					}
					$tmpl->assign('authors', $authors);
					
					// CKEditor einbinden
					/*include_once(ADMINDIR.'/ckeditor/ckeditor.php');
					$CKEditor = new CKEditor();
					$CKEditor->basePath = '/admin/ckeditor/';
					$CKEditor->returnOutput = true;		
					$CKEditor->config['customConfig'] = 'admin_config.js';*/

					$text = ($error != '') ? stripslashes($_REQUEST['text']) :$row->text;
					/*$tmpl->assign("text", $CKEditor->editor("text", $text));*/
					$tmpl->assign("text", $text);
					
					$tmpl->assign('content', $tmpl->fetch('news/form.tpl'));
				}
			}
		break;
		
		//===================================
		// News aktivieren / deaktivieren
		//===================================
		case "active":
			
			if(!permission($_REQUEST['p'],$_REQUEST['action'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				if($_REQUEST['id'] != '' && $_REQUEST['option'] != '') {
					$sql = $db->Query("SELECT uid FROM ".PREFIX."_news WHERE id = ".escs($_REQUEST['id']));
					$row = $sql->fetchrow();
					if(!permission($_REQUEST['p'],'all') && $row->uid != UID) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
					else {
						
						$db->Query("UPDATE ".PREFIX."_news SET active = ".escs($_REQUEST['option'])." WHERE id = ".escs($_REQUEST['id']));
	
						// Cache leeren
						clearcache(BASEDIR.'temp/');
						
						header('location: index.php?p=news&action=overview&pp='.$_REQUEST['pp'].'&page='.$_REQUEST['page']);
					}
				}
				else header('location: index.php?p=news&action=overview&pp='.$_REQUEST['pp'].'&page='.$_REQUEST['page']);
			}
		break;
		
		//===================================
		// News löschen
		//===================================
		case "delete":
		
			if(!permission($_REQUEST['p'],$_REQUEST['action'])) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
			else {
				if($_REQUEST['id'] != '') {
					$sql = $db->Query("SELECT uid FROM ".PREFIX."_news WHERE id = ".escs($_REQUEST['id']));
					$row = $sql->fetchrow();
					if(!permission($_REQUEST['p'],'all') && $row->uid != UID) $tmpl->assign('content', $tmpl->fetch('permission_error.tpl'));
					else {
						
						// Datenbankeintrag entfernen
						$db->Query("DELETE FROM ".PREFIX."_news WHERE id = ".escs($_REQUEST['id']));
						
						// Cache leeren
						clearcache(BASEDIR.'temp/');
						
						header('location: index.php?p=news&action=overview&pp='.$_REQUEST['pp'].'&page='.$_REQUEST['page']);
					}
				}
				else header('location: index.php?p=news&action=overview&pp='.$_REQUEST['pp'].'&page='.$_REQUEST['page']);
			}
		break;
	}
}
?>