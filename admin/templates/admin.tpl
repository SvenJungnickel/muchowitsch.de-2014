{strip}
{include file="head.tpl"}

{include file="navi.tpl"}

<div class="content">
  {$content}
</div>

{include file="foot.tpl"}
{/strip}