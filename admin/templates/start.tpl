<table width="100%" cellpadding="0" cellspacing="0" border="0">
  <tbody><tr>
    <td>
      <h1 class="left">Startseite</h1>
      <p class="right p_right bold"><a href="javascript:;" onclick="if(confirm('Wirklich ausloggen?')){ldelim}window.location.href='{$admindomain}index.php?p=logout';{rdelim}" class="red">Logout</a></p>  
    </td>
  </tr>
  <tr>
    <td>
      {if permission("news")}
      <div class="box235 h85 p_all m_all left bgc_grey rounded shadow">
        <p class="bold">Neuigkeiten</p>
        {if permission("news","overview")}<p class="b_bottom_dotted m_left"><a href="index.php?p=news&amp;action=overview" class="bold">&Uuml;bersicht</a></p>{/if}
        {if permission("news","new")}<p class="b_bottom_dotted m_left"><a href="index.php?p=news&amp;action=new" class="bold">Neuigkeit eintragen</a></p>{/if}
      </div>
      {/if}
           
      {if permission("pages")} 
      <div class="box235 h85 p_all m_all left bgc_grey rounded shadow">
        <p class="bold">Inhalte</p>
        {if permission("pages","overview")}<p class="b_bottom_dotted m_left"><a href="index.php?p=pages&amp;action=overview" class="bold">&Uuml;bersicht</a></p>{/if}
        {if permission("pages","new")}<p class="b_bottom_dotted m_left"><a href="index.php?p=pages&amp;action=new" class="bold">neue Seite mit Inhalten erstellen</a></p>{/if}
      </div>
      {/if}
      
      {if permission("glossary")} 
      <div class="box235 h85 p_all m_all left bgc_grey rounded shadow">
        <p class="bold">Glossareintr&auml;ge</p>
        {if permission("glossary","overview")}<p class="b_bottom_dotted m_left"><a href="index.php?p=glossary&amp;action=overview" class="bold">&Uuml;bersicht</a></p>{/if}
        {if permission("glossary","new")}<p class="b_bottom_dotted m_left"><a href="index.php?p=glossary&amp;action=new" class="bold">neuen Glossareintrag erstellen</a></p>{/if}
      </div>
      {/if}
      
      {if permission("printmedia")} 
      <div class="box235 h85 p_all m_all left bgc_grey rounded shadow">
        <p class="bold">Presseeintr&auml;ge</p>
        {if permission("printmedia","overview")}<p class="b_bottom_dotted m_left"><a href="index.php?p=printmedia&amp;action=overview" class="bold">&Uuml;bersicht</a></p>{/if}
        {if permission("printmedia","new")}<p class="b_bottom_dotted m_left"><a href="index.php?p=printmedia&amp;action=new" class="bold">neuen Presseeintrag erstellen</a></p>{/if}
      </div>
      {/if}
      
      {if permission("seminars")} 
      <div class="box235 h85 p_all m_all left bgc_grey rounded shadow">
        <p class="bold">Seminare</p>
        {if permission("seminars","overview")}<p class="b_bottom_dotted m_left"><a href="index.php?p=seminars&amp;action=overview" class="bold">&Uuml;bersicht</a></p>{/if}
        {if permission("seminars","new")}<p class="b_bottom_dotted m_left"><a href="index.php?p=seminars&amp;action=new" class="bold">neues Seminar eintragen</a></p>{/if}
      </div>
      {/if}
      
      {if permission("user")} 
      <div class="box235 h85 p_all m_all left bgc_grey rounded shadow">
        <p class="bold">Benutzer &amp; Gruppen</p>
        <p class="b_bottom_dotted m_left"><a href="index.php?p=user&amp;action=overview" class="bold">Benutzer</a></p>
        <p class="b_bottom_dotted m_left"><a href="index.php?p=user&amp;action=groups" class="bold">Gruppen</a></p>
      </div>
      {else}
      <div class="box235 h85 p_all m_all left bgc_grey rounded shadow">
        <p class="bold">eigene Profildaten</p>
        <p class="b_bottom_dotted m_left"><a href="index.php?p=own" class="bold">Einstellungen</a></p>
      </div>
      {/if}
    
      <div class="clear"></div>
    </td>
  </tr></tbody>
</table>