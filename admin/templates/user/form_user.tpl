{strip}
{if $smarty.request.action == 'new_user'}
  <h1>neuen Benutzer anlegen</h1>
{elseif $smarty.request.p == 'own'}
  <h1>Einstellungen</h1>
{else}
  <h1>Benutzer - {$row.uname} bearbeiten</h1>
{/if}

<form name="form" action="" method="post" enctype="multipart/form-data">
  <input type="hidden" name="save" value="1" />
  <table width="100%" cellpadding="3" cellspacing="1" border="0">
    <tbody><tr>
      {if $error != ''}
        <td class="cella" colspan="2">
          <font color="#FF0000">{$error}</font>
        </td>
      </tr>
      <tr>
      {elseif $smarty.request.edit == 1}
      <td class="cella" colspan="2">
          <font color="#00AA00">Die Daten wurden erfolgreich gespeichert.</font>
        </td>
      </tr>
      <tr>      
      {/if}
      <td class="cella">
        <span class="stdfont"><strong>Benutzername</strong></span>
      </td>
      <td class="cellb">
        <input type="text" name="uname" value="{if $error == ''}{$row.uname|sslash|escape:'html'}{else}{$smarty.request.uname|sslash|escape:'html'}{/if}" size="50" maxlength="255" tabindex="1" />
      </td>
    </tr>
    <tr>
      <td class="cella">
        <span class="stdfont"><strong>Email Adresse</strong></span>
      </td>
      <td class="cellb">
        <input type="text" name="email" value="{if $error == ''}{$row.email|sslash|escape:'html'}{else}{$smarty.request.email|sslash|escape:'html'}{/if}" size="50" maxlength="255" tabindex="2" />
      </td>
    </tr>
    <tr>
      <td class="cella">
        <span class="stdfont"><strong>Autorenk&uuml;rzel</strong></span>
      </td>
      <td class="cellb">
        <input type="text" name="authortoken" value="{if $error == ''}{$row.authortoken}{else}{$smarty.request.authortoken}{/if}" size="50" maxlength="255" tabindex="3" />
      </td>
    </tr>
    {if $smarty.request.p == 'user'}
    <tr>
      <td class="cella">
        <span class="stdfont"><strong>Gruppe</strong></span>
      </td>
      <td class="cellb">
        <select name="ugroup" tabindex="4">
          <option value="">- - -</option>
          {foreach from=$groups item=group}
            <option value="{$group.id}" {if $error == '' && $row.ugroup == $group.id || $error != '' && $smarty.request.ugroup == $group.id}selected="selected"{/if}>{$group.name}</option>
          {/foreach}
        </select>
      </td>
    </tr>
    {/if}
    <tr>
      <td class="cella">
        <span class="stdfont"><strong>{if $smarty.request.action == 'new_user'}Passwort{else}neues Passwort setzen{/if}</strong></span>
      </td>
      <td class="cellb">        
        <input type="password" name="password{if $smarty.request.action == 'edit_user' || $smarty.request.p == 'own'}_new{/if}" value="" size="50" maxlength="255" tabindex="5" />
      </td>
    </tr>
    <tr>
      <td class="cella">
        <span class="stdfont"><strong>{if $smarty.request.action == 'edit_user' || $smarty.request.p == 'own'}neues {/if}Passwort wiederholen</strong></span>
      </td>
      <td class="cellb">
          <input type="password" name="password_retry" value="" size="50" maxlength="255" tabindex="6" />
      </td>
    </tr>
    {if $smarty.request.p == 'user' && $smarty.request.action == 'edit_user'}
    <tr>
      <td class="cella" valign="top">
        <span class="stdfont"><strong>Aktiv?</strong></span>
      </td>
      <td class="cellb">
        <input type="checkbox" name="active" value="1" {if $error == '' && $row.active == 1 || $error != '' && $smarty.request.active == 1}checked="checked"{/if} tabindex="7" />
      </td>
    </tr>
    {elseif $smarty.request.action != 'new_user'}
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td class="cella" valign="top">
        <span class="stdfont"><strong>altes Passwort eingeben</strong></span>
      </td>
      <td class="cellb">
        <input type="password" name="password_old" value="" size="50" maxlength="255" tabindex="8" />
      </td>
    </tr>    
    {/if}
    <tr>
      <td class="cellb" colspan="2">
        <button type="submit" name="submit" value="Speichern" class="active" tabindex="9"><span class="tc_green">Speichern</span></button>
        {if $smarty.request.p == 'user' || UGROUP == 1}<button type="button" name="button" value="Zur&uuml;ck zur &Uuml;bersicht" class="active m_left" tabindex="10" onclick="window.location.href='{$admindomain}index.php?p=user&amp;action=overview&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}'"><span class="tc_red">Zur&uuml;ck zur &Uuml;bersicht</span></button>{/if}
      </td>
    </tr></tbody>
  </table> 
</form>
{/strip}