{strip}
<h1>Mannschaften - &Uuml;bersicht</h1>

<table width="100%" cellpadding="3" cellspacing="1" border="0">
  <tbody>
  {if $smarty.request.nodel == 1}
  <tr class="bgc_lgrey">
    <td colspan="3" class="tc_red">Diese Gruppe darf nicht gel&ouml;scht werden</td>
  </tr>
  {/if}
  <tr class="header">  
    <td>Gruppe</td>
    <td>Anzahl Benutzer</td>
    <td width="40">Aktionen</td>
  </tr>
  {foreach from=$groups item=group name=group}
  <tr class="{if $smarty.foreach.group.iteration%2 == 0}bgc_white{else}bgc_grey{/if}">
    <td class="bold">{$group.name}</td>
    <td>{$group.user}</td>
    <td>
      {*<a href="{$admindomain}index.php?p=user&amp;action=permissions&amp;id={$group.id}"><img src="{$domainname}{$img_path}icons/key_go.png" alt="key_go" title="Berechtigungen" /></a>&nbsp;*}
      <a href="{$admindomain}index.php?p=user&amp;action=edit_group&amp;id={$group.id}"><img src="{$domainname}{$img_path}icons/group_edit.png" alt="group_edit" title="Gruppe bearbeiten" /></a>&nbsp;
      <a href="javascript:;" onclick="if(confirm('Willst du diese Gruppe wirklich l&ouml;schen?')){ldelim}window.location.href='{$admindomain}index.php?p=user&amp;action=del_group&amp;id={$group.id}';{rdelim}"><img src="{$domainname}{$img_path}icons/group_delete.png" alt="group_delete" title="Gruppe l&ouml;schen" /></a>&nbsp;
    </td>
  </tr>
  {foreachelse}
  <tr class="bgc_grey">
    <td colspan="3" align="center">Es wurden noch keine Gruppen angelegt.</td>
  </tr>
  {/foreach}
  <tr class="bgc_grey">
    <td colspan="3">
      <button type="button" name="button" value="Neue Gruppe anlegen" class="active" tabindex="1" onclick="window.location.href='{$admindomain}index.php?p=user&amp;action=new_group'">Neue Gruppe anlegen</button>
    </td>
  </tr></tbody>
</table>
{/strip}