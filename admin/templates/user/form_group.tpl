{literal}
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script> 
<script type="text/javascript">
  function changeActions(action) {
    if(action.substring(0,3) == 'all') {
	  var id = action.substring(4,6);
	  $('.action_'+id).attr('checked', $('.all_'+id).attr('checked'));
	}
	else {
	  var id = action.substring(7,9);
	  $('.all_'+id).attr('checked', false);
	}
  }
</script>
{/literal}
{strip}
{if $smarty.request.action == 'new_group'}
  <h1>neue Gruppe anlegen</h1>
{else}
  <h1>Gruppe - {$row.name} bearbeiten</h1>
{/if}

<form name="form" action="" method="post" enctype="multipart/form-data">
  <input type="hidden" name="save" value="1" />
  <table width="100%" cellpadding="3" cellspacing="1" border="0">
    <tbody><tr>
      {if $error != ''}
        <td class="cella" colspan="2">
          <font color="#FF0000">{$error}</font>
        </td>
      </tr>
      <tr>
      {/if}
      <td class="cella">
        <span class="stdfont"><strong>Gruppenname</strong></span>
      </td>
      <td class="cellb">
        <input type="text" name="name" value="{if $error == ''}{$group.name|sslash|escape:'html'}{else}{$smarty.request.name|sslash|escape:'html'}{/if}" size="50" maxlength="255" tabindex="1" />
      </td>
    </tr>
    <tr>
      <td class="cella" colspan="2">&nbsp;
        
      </td>
    </tr>
    <tr>
      <td class="cella" colspan="2">
        <h1>Berechtigungen setzen</h1>
      </td>
    </tr>
    {if $smarty.request.id == 1}
    <tr>
      <td class="cella" colspan="2">
        <span class="stdfont">Die Berechtigungen der Administratoren k�nnen nicht bearbeitet werden.</span>
      </td>
    </tr>    
    {else}
    {foreach from=$moduls item=modul name=modul}
    <tr>
      <td class="cella" colspan="2">
        <span class="stdfont"><strong>{$modul.title}</strong></span>
      </td>
    </tr>
    <tr>
      <td class="cellb" colspan="2">
        <table border="0" cellpadding="3" cellspacing="1">
          <tr>
            <td class="cella" width="300"><strong>Alles</strong></td>
            <td class="cella">
              <input type="checkbox" name="modul[]" value="{$modul.id}" {if $error == '' && $smarty.request.action == 'edit_group' && in_array($modul.id,$permissions) || $error != '' && $smarty.request.modul != '' && in_array($modul.id,$smarty.request.modul)}checked="checked"{/if} tabindex="{$smarty.foreach.modul.iteration}00" class="all_{$modul.id}" onclick="changeActions('all_{$modul.id}')" />
            </td>
          </tr>
          {foreach from=$modul.actions item=action name=action}
          <tr>
            <td class="cella">{$action.title}</td>
            <td class="cella">
              <input type="checkbox" name="modul[]" value="{$action.id}" {if $error == '' && $smarty.request.action == 'edit_group' && in_array($action.id,$permissions) || $error != '' && $smarty.request.modul != '' && in_array($action.id,$smarty.request.modul)}checked="checked"{/if} tabindex="{$smarty.foreach.modul.iteration}{if $smarty.foreach.action.iteration < 10}0{/if}{$smarty.foreach.action.iteration}" class="action_{$modul.id}" onclick="changeActions('action_{$modul.id}')" />
            </td>
          </tr>
          {/foreach}
          {if $modul.id == 2}
          <tr>
            <td class="cella" colspan="2">
              <strong>die Gruppe einer oder mehrerer Mannschaften zuteilen</strong>
            </td>
          </tr>
          <tr>
            <td class="cella" colspan="2">
              <select name="teams[]" multiple="multiple" size="{$teamcount}">
             {foreach from=$teams item=team}
               <option value="{$team.id}"{if $error == '' && $group_teams && in_array($team.id,$group_teams) || $error != '' && $smarty.request.teams && in_array($team.id,$smarty.request.teams)} selected="selected"{/if}>{$team.name}</option>
             {/foreach}
              </select>
            </td>
          </tr>
          {/if}
        </table>        
      </td>
    </tr>
    {/foreach}
    <tr>
      <td class="cella">
        <span class="stdfont"><strong>Benutzer &amp; Gruppen</strong></span>
      </td>
      <td class="cellb">
        <input type="checkbox" disabled="disabled" /> <small>(Nur Administratoren haben Zugriff auf dieses Modul)</small>
      </td>
    </tr>
    {/if}
    <tr>
      <td class="cella" colspan="2">&nbsp;
        
      </td>
    </tr>
    <tr>
      <td class="cellb" colspan="2">
        <button type="submit" name="submit" value="Speichern" class="active" tabindex="1000"><span class="tc_green">Speichern</span></button>
        <button type="button" name="button" value="Zur&uuml;ck zur &Uuml;bersicht" class="active m_left" tabindex="1001" onclick="window.location.href='{$admindomain}index.php?p=user&amp;action=groups'"><span class="tc_red">Zur&uuml;ck zur &Uuml;bersicht</span></button>        
      </td>
    </tr></tbody>
  </table> 
</form>
{/strip}