{strip}
<h1>Benutzer - &Uuml;bersicht ({$num})</h1>

<table width="100%" cellpadding="3" cellspacing="1" border="0">
  <tbody><tr class="header">  
    <td>Benutzername</td>  
    <td>Gruppe</td>
    <td>Email</td>
    <td>Autorenk&uuml;rzel</td>
    <td width="60">Aktionen</td>
  </tr>
  {foreach from=$user item=usr name=usr}
  <tr class="{if $smarty.foreach.usr.iteration%2 == 0}bgc_white{else}bgc_grey{/if}">
    <td>
      {if $usr.active == 1}
        <span class="bold">{$usr.uname}</span>
      {else}
        {$usr.uname}
      {/if}  
    </td>
    <td>{$usr.group}</td>
    <td>{$usr.email}</td>
    <td>{$usr.authortoken}</td>
    <td>
      {if $usr.id != UID}
        {if $usr.active == 1}
          <a href="{$admindomain}index.php?p=user&amp;action=active_user&amp;id={$usr.id}&amp;option=0&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}"><img src="{$domainname}{$img_path}icons/accept.png" alt="accept" title="aktiviert, klicken um zu deaktivieren" /></a>&nbsp;
        {else}
          <a href="{$admindomain}index.php?p=user&amp;action=active_user&amp;id={$usr.id}&amp;option=1&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}"><img src="{$domainname}{$img_path}icons/cancel.png" alt="cancel" title="deaktiviert, klicken um zu aktivieren" /></a>&nbsp;
        {/if}
        <a href="{$admindomain}index.php?p=user&amp;action=edit_user&amp;id={$usr.id}&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}"><img src="{$domainname}{$img_path}icons/user_edit.png" alt="user_edit" title="Benutzer bearbeiten" /></a>&nbsp;
        <a href="javascript:;" onclick="if(confirm('Willst du diesen Benutzer wirklich l&ouml;schen?')){ldelim}window.location.href='{$admindomain}index.php?p=user&amp;action=del_user&amp;id={$usr.id}&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}';{rdelim}"><img src="{$domainname}{$img_path}icons/user_delete.png" alt="user_delete" title="Benutzer l&ouml;schen" /></a>&nbsp;
      {else}
        <a href="{$admindomain}index.php?p=own&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}"><img src="{$domainname}{$img_path}icons/user_edit.png" alt="user_edit" title="eigene Profildaten verwalten" /></a>&nbsp;
      {/if}
    </td>
  </tr>
  {foreachelse}
  <tr class="bgc_grey">
    <td colspan="5" align="center">keine Benutzer gefunden.</td>
  </tr>
  {/foreach}
  {if $navi}
  <tr class="bgc_grey">
    <td colspan="5">{$navi}</td>
  </tr>  
  {/if}
  <tr class="bgc_grey">
    <td colspan="5">
      <button type="button" name="button" value="Neuen Benutzer anlegen" class="active" tabindex="1" onclick="window.location.href='{$admindomain}index.php?p=user&amp;action=new_user'">Neuen Benutzer anlegen</button>
      <button type="button" name="button" value="eigene Profildaten verwalten" class="m_left active" tabindex="2" onclick="window.location.href='{$admindomain}index.php?p=own&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}'">eigene Profildaten verwalten</button>
    </td>
  </tr></tbody>
</table>
{/strip}