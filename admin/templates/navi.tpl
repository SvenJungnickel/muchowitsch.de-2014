{strip}
<div class="left m_top p_top m_bottom box235 rounded_left">
    
    <span class="right m_bottom m_right bold"><a href="javascript:;" onclick="if(confirm('Wirklich ausloggen?')){ldelim}window.location.href='{$admindomain}index.php?p=logout';{rdelim}" class="red">Logout</a></span>
    <div class="clear"></div>
    
    <div class="rounded_left p_all m_bottom_5 shadow_bottom {if $smarty.request.p == '' || $smarty.request.p == 'start'}bgc_flow{else}bgc_grey{/if}">
        <div class="b_bottom_dotted p_top m_bottom_5"><a href="index.php?p=start" class="bold">Startseite</a></div>
    </div>
    
    {if permission("news")}
    <div class="rounded_left p_all m_bottom_5 shadow_bottom {if $smarty.request.p == 'news'}bgc_flow{else}bgc_grey{/if}">
        <div class="m_bottom_5 bold">Neuigkeiten</div>
        {if permission("news","overview")}<div class="b_bottom_dotted m_left"><a href="index.php?p=news&amp;action=overview" class="bold">&Uuml;bersicht</a></div>{/if}
        {if permission("news","new")}<div class="b_bottom_dotted m_left"><a href="index.php?p=news&amp;action=new" class="bold">Neuigkeiten eintragen</a></div>{/if}
    </div>
    {/if}
    
    {if permission("pages")}
    <div class="rounded_left p_all m_bottom_5 shadow_bottom {if $smarty.request.p == 'pages'}bgc_flow{else}bgc_grey{/if}">
        <div class="m_bottom_5 bold">Inhalte</div>
        {if permission("pages","overview")}<div class="b_bottom_dotted m_left"><a href="index.php?p=pages&amp;action=overview" class="bold">&Uuml;bersicht</a></div>{/if}
        {if permission("pages","new")}<div class="b_bottom_dotted m_left"><a href="index.php?p=pages&amp;action=new" class="bold">neue Seite mit Inhalten erstellen</a></div>{/if}
    </div> 
    {/if}
    
    {if permission("glossary")}
    <div class="rounded_left p_all m_bottom_5 shadow_bottom {if $smarty.request.p == 'glossary'}bgc_flow{else}bgc_grey{/if}">
        <div class="m_bottom_5 bold">Glossareintr&auml;ge</div>
        {if permission("glossary","overview")}<div class="b_bottom_dotted m_left"><a href="index.php?p=glossary&amp;action=overview" class="bold">&Uuml;bersicht</a></div>{/if}
        {if permission("glossary","new")}<div class="b_bottom_dotted m_left"><a href="index.php?p=glossary&amp;action=new" class="bold">neuen Glossareintrag erstellen</a></div>{/if}
    </div> 
    {/if}
    
    {if permission("printmedia")} 
    <div class="rounded_left p_all m_bottom_5 shadow_bottom {if $smarty.request.p == 'printmedia'}bgc_flow{else}bgc_grey{/if}">
    	<div class="m_bottom_5 bold">Presseeintr&auml;ge</div>
    	{if permission("printmedia","overview")}<div class="b_bottom_dotted m_left"><a href="index.php?p=printmedia&amp;action=overview" class="bold">&Uuml;bersicht</a></div>{/if}
    	{if permission("printmedia","new")}<div class="b_bottom_dotted m_left"><a href="index.php?p=printmedia&amp;action=new" class="bold">neuen Presseeintrag erstellen</a></div>{/if}
    </div>
    {/if}
    
    {if permission("seminars")} 
    <div class="rounded_left p_all m_bottom_5 shadow_bottom {if $smarty.request.p == 'seminars'}bgc_flow{else}bgc_grey{/if}">
    	<div class="m_bottom_5 bold">Seminare</div>
    	{if permission("seminars","overview")}<div class="b_bottom_dotted m_left"><a href="index.php?p=seminars&amp;action=overview" class="bold">&Uuml;bersicht</a></div>{/if}
    	{if permission("seminars","new")}<div class="b_bottom_dotted m_left"><a href="index.php?p=seminars&amp;action=new" class="bold">neues Seminar eintragen</a></div>{/if}
    </div>
    {/if}
    
    {if permission("user")} 
    <div class="rounded_left p_all m_bottom_5 shadow_bottom {if $smarty.request.p == 'user' || $smarty.request.p == 'own'}bgc_flow{else}bgc_grey{/if}">
        <div class="m_bottom_5 bold">Benutzer &amp; Gruppen</div>
        <div class="b_bottom_dotted m_left"><a href="index.php?p=user&amp;action=overview" class="bold">Benutzer</a></div>
        <div class="b_bottom_dotted m_left"><a href="index.php?p=user&amp;action=groups" class="bold">Gruppen</a></div>
    </div>
    {else}
    <div class="rounded_left p_all m_bottom_5 shadow_bottom {if $smarty.request.p == 'user'}bgc_flow{else}bgc_grey{/if}">
        <div class="m_bottom_5 bold">eigene Profildaten</div>
        <div class="b_bottom_dotted m_left"><a href="index.php?p=own" class="bold">Einstellungen</a></div>
    </div>
    {/if}
</div>
{/strip}