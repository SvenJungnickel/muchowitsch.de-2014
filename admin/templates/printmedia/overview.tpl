{strip}
<h1>Presseeintr&auml;ge - &Uuml;bersicht ({$num})</h1>

<table width="100%" cellpadding="3" cellspacing="1" border="0">
  <tbody><tr>     
  <tr class="header">
    <td>Titel</td>
    <td>Ver&ouml;ffentlichungsdatum</td>
    {*<td>Link</td>*}
    <td>Beschreibung</td>
    <td width="100">Aktionen</td>
  </tr>
  {foreach from=$printmedia item=p name=p}
  <tr class="{if $smarty.foreach.p.iteration%2 == 0}bgc_white{else}bgc_grey{/if}">
    <td>
      {if $p->active == 1}
        <a href="{$domainname}index.php?p=printmedia&amp;id=93&amp;pid={$p->id}" target="_blank" class="bold">{$p->title|sslash}</a>
      {else}
        {$p->title|sslash}
      {/if}  
    </td>
    <td>{$p->date|date_format:'%d.%m.%Y'}</td>
    {*<td><a href="{$p->link}" target="_blank">{$p->link}</a></td>*}
    <td>{$p->descr|truncate:100}</td>
    <td>
      {if $p->active == 1}
        {if permission($smarty.request.p,'active')}
          <a href="{$admindomain}index.php?p=printmedia&amp;action=active&amp;id={$p->id}&amp;option=0&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}&amp;sort={$smarty.request.sort}"><img src="{$domainname}{$img_path}icons/accept.png" alt="accept" title="aktiviert, klicken um zu deaktivieren" /></a>&nbsp;
        {else}
          <img src="{$domainname}{$img_path}icons/accept.png" alt="accept" title="aktiviert" />&nbsp;
        {/if}
      {else}
        {if permission($smarty.request.p,'active')}
          <a href="{$admindomain}index.php?p=printmedia&amp;action=active&amp;id={$p->id}&amp;option=1&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}&amp;sort={$smarty.request.sort}"><img src="{$domainname}{$img_path}icons/cancel.png" alt="cancel" title="deaktiviert, klicken um zu aktivieren" /></a>&nbsp;
        {else}
          <img src="{$domainname}{$img_path}icons/cancel.png" alt="cancel" title="deaktiviert" />&nbsp;
        {/if}
      {/if}
      {if permission($smarty.request.p,'upload')}<a href="{$admindomain}index.php?p=printmedia&amp;action=upload&amp;id={$p->id}"><img src="{$domainname}{$img_path}icons/image_add.png" alt="images" title="Bilder hochladen" /></a>&nbsp;{/if}
      {if permission($smarty.request.p,'showpics') && $p->count_pics > 0}<a href="{$admindomain}index.php?p=printmedia&amp;action=showpics&amp;id={$p->id}"><img src="{$domainname}{$img_path}icons/images.png" alt="images" title="Bilder bearbeiten" /></a>&nbsp;{/if}
      {if permission($smarty.request.p,'edit')}<a href="{$admindomain}index.php?p=printmedia&amp;action=edit&amp;id={$p->id}&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}&amp;sort={$smarty.request.sort}"><img src="{$domainname}{$img_path}icons/date_edit.png" alt="date_edit" title="Presseeintrag bearbeiten" /></a>&nbsp;{/if}
      {if permission($smarty.request.p,'delete')}<a href="javascript:;" onclick="if(confirm('Willst du diesen Presseeintrag wirklich l&ouml;schen?')){ldelim}window.location.href='{$admindomain}index.php?p=printmedia&amp;action=delete&amp;id={$p->id}&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}&amp;sort={$smarty.request.sort}';{rdelim}"><img src="{$domainname}{$img_path}icons/date_delete.png" alt="date_delete" title="Presseeintrag l&ouml;schen" /></a>&nbsp;{/if}
    </td>
  </tr>
  {foreachelse}
  <tr class="bgc_grey">
    <td colspan="5" align="center">Es wurden noch keine Presseeintr&auml;ge angelegt.</td>
  </tr>
  {/foreach}
  {if $navi}
  <tr class="bgc_grey">
    <td colspan="5">{$navi}</td>
  </tr>  
  {/if}
  </tbody>
</table>
{/strip}