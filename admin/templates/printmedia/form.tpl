{strip}
{* JQuery Datepicker & JQuery Fancybox *}
<link type="text/css" rel="stylesheet" href="{$domainname}{$css_path}ui.theme.css" media="screen" />
<link type="text/css" rel="stylesheet" href="{$domainname}{$css_path}ui.core.css" media="screen" />
<link type="text/css" rel="stylesheet" href="{$domainname}{$css_path}ui.datepicker.css" media="screen" />
<link type="text/css" rel="stylesheet" href="{$domainname}{$js_path}jquery.fancybox/jquery.fancybox-1.3.1.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script> 
<script type="text/javascript" src="{$domainname}{$js_path}ui.datepicker/ui.datepicker.js"></script>
<script type="text/javascript" src="{$domainname}{$js_path}ui.datepicker/ui.datepicker-de.js"></script>
<script type="text/javascript" src="{$domainname}{$js_path}jquery.fancybox/jquery.fancybox-1.3.1.js"></script>
<script type="text/javascript">
jQuery(function($) {ldelim}
  $(".dateinput").datepicker();
  $.datepicker.setDefaults({ldelim}    
    dateFormat: 'dd.mm.yy'
  {rdelim});
  
  $("a.img").fancybox({ldelim}
    'padding': 1,
    'overlayOpacity': 0.5,
    'hideOnContentClick': false
  {rdelim});
{rdelim});

function checklink(object) {ldelim}
  var sub = object.value.substring(0,7);
  if(sub != 'http://' && object.value != '') object.value = 'http://'+object.value;
{rdelim}
</script>
{if $smarty.request.action == 'new'}
  <h1>neuen Presseeintrag erstellen</h1>
{else}
  <h1>Presseeintrag <i>{$row->title|sslash}</i> bearbeiten</h1>
{/if}

<form name="form" action="" method="post" enctype="multipart/form-data">
  <input type="hidden" name="save" value="1" />
  <table width="100%" cellpadding="3" cellspacing="3" border="0" class="rounded bgc_lgrey">
    <tbody><tr>
      {if $error != ''}
      <td class="cella" colspan="2">
        <font color="#FF0000">{$error}</font>
      </td>
    </tr>
    <tr>
      {/if}      
    <tr>
      <td class="cella">
        <span class="stdfont"><strong>Titel</strong></span>
      </td>
      <td class="cellb">
        <input type="text" name="title" class="rounded" value="{if $error == ''}{$row->title|sslash|escape:'html'}{else}{$smarty.request.title|sslash|escape:'html'}{/if}" size="50" maxlength="255" tabindex="1" />
      </td>
    </tr>
    <tr>
      <td class="cella">
        <span class="stdfont"><strong>Datum</strong></span>
      </td>
      <td class="cellb">
        <input type="text" name="date" class="dateinput rounded" value="{if $error == ''}{$row->date|date_format:'%d.%m.%Y'}{else}{$smarty.request.date}{/if}" size="10" maxlength="10" tabindex="2" />
      </td>
    </tr>
    <tr>
      <td class="cella">
        <span class="stdfont"><strong>Link/URL</strong></span>
      </td>
      <td class="cellb">
        <input type="text" name="link" class="rounded" value="{if $error == ''}{$row->link|sslash}{else}{$smarty.request.link|sslash}{/if}" onchange="javascript:checklink(this);" size="50" maxlength="255" tabindex="3" />
        {if $error == '' && $row->link != '' || $error != '' && $smarty.request.link != ''}
          <p><a href="{$row->link|sslash}" target="_blank">{$row->link|sslash}</a></p>
        {/if}
      </td>
    </tr>
    <tr>
      <td class="cella">
        <span class="stdfont"><strong>Zeitung</strong></span>
      </td>
      <td class="cellb">
        <input type="text" name="newspaper" class="rounded" value="{if $error == ''}{$row->newspaper|sslash|escape:'html'}{else}{$smarty.request.newspaper|sslash|escape:'html'}{/if}" size="50" maxlength="255" tabindex="4" />
      </td>
    </tr>
    <tr>
      <td class="cella">
        <span class="stdfont"><strong>Seite</strong></span>
      </td>
      <td class="cellb">
        <input type="text" name="newspage" class="rounded" value="{if $error == ''}{$row->newspage|sslash|escape:'html'}{else}{$smarty.request.newspage|sslash|escape:'html'}{/if}" size="15" maxlength="255" tabindex="5" />
      </td>
    </tr>
    {if $image != ''}
    <tr>
      <td class="cella">
        <span class="stdfont"><strong>Bild/Screenshot</strong></span>
      </td>
      <td class="cellb">
        <a class="img" href="{$domainname}{$upload_path}printmedia/big/{$image}">
          <img class="img" src="{$domainname}{$upload_path}printmedia/thumb/{$image}" alt="Bild" border="0" />
        </a>
        <input type="hidden" name="old_image" value="{$image}" />
      </td>
    </tr>
    <tr>
      <td class="cella">
        <span class="stdfont"><strong>Bild/Screenshot l&ouml;schen</strong></span>
      </td>
      <td class="cellb">
        <input type="checkbox" name="del_image" value="1" />
      </td>
    </tr>
    {/if}
    <tr>
      <td class="cella">
        <span class="stdfont"><strong>{if $image != ''}neues {/if}Bild/Screenshot hochladen</strong></span>
      </td>
      <td class="cellb">
        <iframe style="border:0px; width:100%; height:26px; margin:0px; padding:0px;" align="left" src="{$admindomain}image_upload.php" name="iframe_image_upload" id="iframe_image_upload"></iframe>
        <input type="hidden" name="image" id="image" />
      </td>
    </tr>   
    <tr>
      <td class="cella" valign="top">
        <span class="stdfont"><strong>Beschreibungstext</strong></span>
      </td>
      <td class="cellb">
        <textarea name="text">{$text}</textarea>        
      </td>
    </tr>
    <tr>
      <td class="cella" valign="top">
        <span class="stdfont"><strong>Aktiv?</strong></span>
      </td>
      <td class="cellb">
        <input type="checkbox" name="active" value="1" {if $error == '' && $row->active == 1 || $error != '' && $smarty.request.active == 1 || $smarty.request.action == 'new'}checked="checked"{/if} tabindex="9" />
      </td>
    </tr>
    <tr>
      <td class="cellb" colspan="2">
        <button type="submit" name="submit" value="Speichern" class="active" tabindex="10"><span class="tc_green">Speichern</span></button>
        <button type="button" name="button" value="Zur&uuml;ck zur &Uuml;bersicht" class="active m_left" tabindex="11" onclick="window.location.href='{$admindomain}index.php?p=printmedia&amp;action=overview&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}&amp;sort={$smarty.request.sort}'"><span class="tc_red">Zur&uuml;ck zur &Uuml;bersicht</span></button>        
      </td>
    </tr></tbody>
  </table>
</form>
<script type="text/javascript" src="{$admindomain}ckeditor/ckeditor.js"></script>
<script type="text/javascript">
	CKEDITOR.replace('text');
</script>
{/strip}