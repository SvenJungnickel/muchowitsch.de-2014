{strip}
{* jQuery Fancybox *}
<link href="{$domainname}{$js_path}jquery.fancybox/jquery.fancybox-1.3.1.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script> 
<script type="text/javascript" src="{$domainname}{$js_path}jquery.fancybox/jquery.fancybox-1.3.1.js"></script>
<script type="text/javascript">
$(document).ready(function(){ldelim}
  $("a.img").fancybox({ldelim}
    'padding': 1,
    'overlayOpacity': 0.5,
    'hideOnContentClick': false
  {rdelim});
  
  $("input[name='check_all_act']").click(function(){ldelim}
      if($(this).val()==0){ldelim}
        $(this).parents("table")
               .find("input:checkbox[class='active']")
               .attr("checked","checked");
		$(this).val(1);
      {rdelim}
      else {ldelim}
        $(this).parents("table")
               .find("input:checkbox[class='active']")
               .attr("checked","");
		$(this).val(0);
      {rdelim}   
  {rdelim});
  $("input[name='check_all_del']").click(function(){ldelim}
      if($(this).val()==0){ldelim}
        $(this).parents("table")
               .find("input:checkbox[class='delete']")
               .attr("checked","checked");
		$(this).val(1);
      {rdelim}
      else {ldelim}
        $(this).parents("table")
               .find("input:checkbox[class='delete']")
               .attr("checked","");
		$(this).val(0);
      {rdelim}
  {rdelim});
{rdelim});
</script>

<h1>Bilder zum Presseintrag - {$row->title|sslash}</h1>

<form name="kform" action="" method="post">
<input type="hidden" name="save" value="1" />
<table border="0" cellpadding="3" cellspacing="1" width="100%">
  <tbody><tr>
    <td>
      <table border="0" cellpadding="3" cellspacing="1" width="100%">
        <tbody><tr>
          <td>
            <span class="smallfont">Anzahl Bilder: <strong>{$count_pics}</strong></span>
          </td>
        </tr>
        <tr><td>
        {foreach from=$pics item=pic name=pic}
          <div style="width:200px; height:170px; float:left; margin:10px;" class="cella">
            <table border="0" cellpadding="3" cellspacing="0" width="100%">
              <tbody><tr>
              <tr>
                <td class="cella" align="center" height="110" colspan="2">
                  <a class="img" rel="group" href="{$domainname}{$upload_path}printmedia/{$pic->path}">
                    <img style="max-width:190px; max-height:140px" src="{$domainname}{$upload_path}printmedia/{$pic->thumb}" alt="Bild {$pic->number}" border="0">
                  </a>
                </td>
              </tr>
              <tr>
                <td align="center">
                  <input type="checkbox" name="active[{$pic->id}]" id="active[{$pic->id}]" value="{$pic->id}" class="active" {if $pic->active == 1}checked="checked"{/if} /><label for="active[{$pic->id}]" class="smallfont pointer">Aktiv</label>
                </td>
                <td align="center">
                  <input type="checkbox" name="delete[{$pic->id}]" id="delete[{$pic->id}]" value="{$pic->id}" class="delete" /><label for="delete[{$pic->id}]" class="smallfont pointer" style="color:#FF0000">L&ouml;schen</label>
                </td>
              </tr></tbody>
            </table>
          </div>
          {if $smarty.foreach.pic.last}<div style="clear:both"></div>{/if}
        {foreachelse}
        </td><tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td align="center">
            <span class="smallfont">Es wurden noch keine Bilder hochgeladen.</span>
          </td>
        </tr>
        {/foreach}
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <label for="allbox" class="smallfont pointer">Alle Bilder aktivieren/deaktiviern</label><input id="allbox" name="check_all_act" type="checkbox" {if $pic->active == 1}value="1" checked="checked"{else}value="0"{/if} />&nbsp;
      <label for="allbox2" class="smallfont pointer">Alle Bilder zum L&ouml;schen markieren</label><input id="allbox2" name="check_all_del" type="checkbox" value="0" />&nbsp;
    </td>
  </tr>
  <tr>
    <td>
      <button type="submit" name="submit" class="active" value="Speichern"><span class="tc_green">Speichern</span></button>
      <button type="button" name="button" class="active m_left" value="Zur&uuml;ck zur &Uuml;bersicht" onclick="window.location.href='{$admindomain}index.php?p=printmedia&amp;action=overview&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}&amp;sort={$smarty.request.sort}'"><span class="tc_red">Zur&uuml;ck zur &Uuml;bersicht</span></button>
      <button type="button" name="button2" value="Weitere Bilder hochladen" class="active m_left" onclick="window.location.href='{$admindomain}index.php?p=printmedia&amp;action=upload&amp;id={$row->id}&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}&amp;sort={$smarty.request.sort}'">Weitere Bilder hochladen</button>
    </td>
  </tr></tbody>
</table>
</form>
{/strip}