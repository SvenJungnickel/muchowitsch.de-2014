<style type="text/css">@import url({$domainname}swfupload/swfupload.css) all;</style>
<script type="text/javascript" src="{$domainname}swfupload/swfupload.js"></script>
<script type="text/javascript" src="{$domainname}swfupload/handlers.js"></script>
<script type="text/javascript">
domainname = '{$domainname}';
{literal}
  var swfu;
  
  window.onload = function () {
	swfu = new SWFUpload({
	  // Backend Settings
	  // Das Upload-Script muss vom System getrennt arbeiten, damit keine Probleme bei der Thumb-Erstellung auftreten
	  upload_url: "/swfupload/upload.php?id={/literal}{$smarty.request.id}&uploaddir={$uploaddir}{literal}",	// Relative to the SWF file
	  post_params: {"PHPSESSID": "{/literal}{$session_id}{literal}"},

	  // File Upload Settings
	  file_size_limit : "2 MB",	// 2MB
	  file_types : "*.jpg; *.gif; *.png",
	  file_types_description : "*.jpg, *.gif, *.png",
	  file_upload_limit : 0,

	  // Event Handler Settings - these functions as defined in Handlers.js
	  //  The handlers are not part of SWFUpload but are part of my website and control how
	  //  my website reacts to the SWFUpload events.
	  file_queue_error_handler : fileQueueError,
	  file_dialog_complete_handler : fileDialogComplete,
	  upload_progress_handler : uploadProgress,
	  upload_error_handler : uploadError,
	  upload_success_handler : uploadSuccess,
	  upload_complete_handler : uploadComplete,

	  // Button Settings
	  button_image_url : domainname+"swfupload/images/SmallSpyGlassWithTransperancy_17x18.png",
	  button_placeholder_id : "spanButtonPlaceholder",
	  button_width: 180,
	  button_height: 18,
	  button_text : '<span class="button">Bilder auswaehlen <span class="buttonSmall">(max 2 MB)</span></span>',
	  button_text_style : '.button { font-family: Helvetica, Arial, sans-serif; font-size: 12pt; } .buttonSmall { font-size: 10pt; }',
	  button_text_top_padding: 0,
	  button_text_left_padding: 18,
	  button_window_mode: SWFUpload.WINDOW_MODE.TRANSPARENT,
	  button_cursor: SWFUpload.CURSOR.HAND,

	  // Flash Settings
	  flash_url : domainname+"swfupload/swfupload.swf",	// Relative to this file

	  custom_settings : {
		upload_target : "divFileProgressContainer"
	  },

	  // Debug Settings
	  debug: false
	});
  };
{/literal}
</script>
{strip}
<h1>Bildergalerie zum Presseeintrag <i>{$row->title|sslash}</i> hochladen</h1>

{*<form action="{$domainname}swfupload/upload.php?id={$smarty.request.id}&uploaddir={$uploaddir}&uid={$uid}" method="post" name="kform" id="kform" enctype="multipart/form-data">
    <input type="file" name="Filedata" />
    <input type="submit" value="speichern" /></form>*}
        
    <div style="display: inline; border: solid 1px #7FAAFF; background-color: #C5D9FF; padding: 2px;">
        <span id="spanButtonPlaceholder"></span>
    </div>
    <div id="divFileProgressContainer" style="height: 75px;"></div>
    <div id="thumbnails"></div>
{*</form>*}

<form action="" method="post" name="kform" id="kform">
  <input type="hidden" name="send" value="1" />
  <button type="submit" name="submit" class="m_top active" value="Fertigstellen" tabindex="1"><span class="tc_green">Fertigstellen</span></button>
  <button type="button" name="button" class="active m_top m_left" value="Abbrechen" onclick="if(confirm('Wirklich Abbrechen?')){ldelim}window.location.href='{$admindomain}index.php?p=printmedia&amp;action=overview&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}&amp;sort={$smarty.request.sort}'{rdelim};" tabindex="2" ><span class="tc_red">Abbrechen</span></button>
</form>
{/strip}