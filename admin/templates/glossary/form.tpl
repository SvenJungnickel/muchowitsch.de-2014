{strip}
{if $smarty.request.action == 'new'}
  <h1>neuen Glossareintrag erstellen</h1>
{else}
  <h1>Glossareintrag <i>{$row->title|sslash}</i> bearbeiten</h1>
{/if}

<form name="form" action="" method="post" enctype="multipart/form-data">
  <input type="hidden" name="save" value="1" />
  <table width="100%" cellpadding="3" cellspacing="3" border="0" class="rounded bgc_lgrey">
    <tbody><tr>
      {if $error != ''}
      <td class="cella" colspan="2">
        <font color="#FF0000">{$error}</font>
      </td>
    </tr>
    <tr>
      {/if}      
    <tr>
      <td class="cella">
        <span class="stdfont"><strong>Titel</strong></span>
      </td>
      <td class="cellb">
        <input type="text" name="title" class="rounded" value="{if $error == ''}{$row->title|sslash|escape:'html'}{else}{$smarty.request.title|sslash|escape:'html'}{/if}" size="50" maxlength="255" tabindex="1" />
      </td>
    </tr>
    <tr>
      <td class="cella" valign="top">
        <span class="stdfont"><strong>Beschreibungstext</strong></span>
      </td>
      <td class="cellb">
        <textarea name="descr" class="rounded" tabindex="2" style="width:600px; height:70px;">{if $error == ''}{$row->descr|sslash|escape:'html'}{else}{$smarty.request.descr|sslash|escape:'html'}{/if}</textarea>        
      </td>
    </tr>
    <tr>
      <td class="cella" valign="top">
        <span class="stdfont"><strong>Aktiv?</strong></span>
      </td>
      <td class="cellb">
        <input type="checkbox" name="active" value="1" {if $error == '' && $row->active == 1 || $error != '' && $smarty.request.active == 1 || $smarty.request.action == 'new'}checked="checked"{/if} tabindex="3" />
      </td>
    </tr>
    <tr>
      <td class="cellb" colspan="2">
        <button type="submit" name="submit" value="Speichern" class="active" tabindex="7"><span class="tc_green">Speichern</span></button>
        <button type="button" name="button" value="Zur&uuml;ck zur &Uuml;bersicht" class="active m_left" tabindex="8" onclick="window.location.href='{$admindomain}index.php?p=glossary&amp;action=overview&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}&amp;sort={$smarty.request.sort}'"><span class="tc_red">Zur&uuml;ck zur &Uuml;bersicht</span></button>
      </td>
    </tr></tbody>
  </table>
</form>
{/strip}