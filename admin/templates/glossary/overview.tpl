{strip}
<h1>Glossareintr&auml;ge - &Uuml;bersicht ({$num})</h1>

<table width="100%" cellpadding="3" cellspacing="1" border="0">
  <tbody><tr>
    <td colspan="3" width="100%">
      <span class="bold">Sortieren nach Buchstaben:&nbsp;</span>
      {foreach from=$alphabet item=item}
        <a href="{$admindomain}index.php?p=glossary&amp;sort={$item}" class="m_right_5{if $smarty.request.sort != $item} bold fs_12{else} red fs_14{/if}">{$item}</a>
      {/foreach}
    </td>
  </tr>  
  <tr class="header"> 
    <td>Titel</td>  
    <td>Beschreibung</td>
    <td width="100">Aktionen</td>
  </tr>
  {foreach from=$glossary item=g name=g}
    {if $g->firstletter}
    <tr>
      <td colspan="3"><strong>{$g->title|truncate:1:""|upper}</strong></td>
    </tr>
    {/if}
  <tr class="{if $smarty.foreach.g.iteration%2 == 0}bgc_white{else}bgc_grey{/if}">
    <td>
      {if $g->active == 1}
        <a href="{$domainname}index.php?p=page&id=41&sort={$g->title|truncate:1:""|upper}" target="_blank" class="bold">{$g->title|sslash}</a>
      {else}
        {$g->title|sslash}
      {/if}  
    </td>
    <td>{$g->descr|truncate:100}</td>
    <td>
      {if $g->active == 1}
        {if permission($smarty.request.p,'active')}
          <a href="{$admindomain}index.php?p=glossary&amp;action=active&amp;id={$g->id}&amp;option=0&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}&amp;sort={$smarty.request.sort}"><img src="{$domainname}{$img_path}icons/accept.png" alt="accept" title="aktiviert, klicken um zu deaktivieren" /></a>&nbsp;
        {else}
          <img src="{$domainname}{$img_path}icons/accept.png" alt="accept" title="aktiviert" />&nbsp;
        {/if}
      {else}
        {if permission($smarty.request.p,'active')}
          <a href="{$admindomain}index.php?p=glossary&amp;action=active&amp;id={$g->id}&amp;option=1&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}&amp;sort={$smarty.request.sort}"><img src="{$domainname}{$img_path}icons/cancel.png" alt="cancel" title="deaktiviert, klicken um zu aktivieren" /></a>&nbsp;
        {else}
          <img src="{$domainname}{$img_path}icons/cancel.png" alt="cancel" title="deaktiviert" />&nbsp;
        {/if}
      {/if}
      {if permission($smarty.request.p,'edit')}<a href="{$admindomain}index.php?p=glossary&amp;action=edit&amp;id={$g->id}&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}&amp;sort={$smarty.request.sort}"><img src="{$domainname}{$img_path}icons/date_edit.png" alt="date_edit" title="Glossareintrag bearbeiten" /></a>&nbsp;{/if}
      {if permission($smarty.request.p,'delete')}<a href="javascript:;" onclick="if(confirm('Willst du diesen Glossareintrag wirklich l&ouml;schen?')){ldelim}window.location.href='{$admindomain}index.php?p=glossary&amp;action=delete&amp;id={$g->id}&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}&amp;sort={$smarty.request.sort}';{rdelim}"><img src="{$domainname}{$img_path}icons/date_delete.png" alt="date_delete" title="Glossareintrag l&ouml;schen" /></a>&nbsp;{/if}
    </td>
  </tr>
  {foreachelse}
  <tr class="bgc_grey">
    <td colspan="4" align="center">Es wurden noch keine Glossareintr&auml;ge angelegt.</td>
  </tr>
  {/foreach}
  {if $navi}
  <tr class="bgc_grey">
    <td colspan="4">{$navi}</td>
  </tr>  
  {/if}
  </tbody>
</table>
{/strip}