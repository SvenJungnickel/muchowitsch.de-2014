{strip}
{* JQuery Datepicker & JQuery Fancybox 
<link type="text/css" rel="stylesheet" href="{$domainname}{$css_path}ui.theme.css" media="screen" />
<link type="text/css" rel="stylesheet" href="{$domainname}{$css_path}ui.core.css" media="screen" />
<link type="text/css" rel="stylesheet" href="{$domainname}{$css_path}ui.datepicker.css" media="screen" />
<link type="text/css" rel="stylesheet" href="{$domainname}{$js_path}jquery.fancybox/jquery.fancybox-1.3.1.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script> 
<script type="text/javascript" src="{$domainname}{$js_path}ui.datepicker/ui.datepicker.js"></script>
<script type="text/javascript" src="{$domainname}{$js_path}ui.datepicker/ui.datepicker-de.js"></script>
<script type="text/javascript" src="{$domainname}{$js_path}jquery.fancybox/jquery.fancybox-1.3.1.js"></script>
<script type="text/javascript">
jQuery(function($) {ldelim}
  $("#dateinput").datepicker();
  $.datepicker.setDefaults({ldelim}    
    dateFormat: 'dd.mm.yy'
  {rdelim});
  
  $("a.img").fancybox({ldelim}
    'padding': 1,
    'overlayOpacity': 0.5,
    'hideOnContentClick': false
  {rdelim});
{rdelim});
</script>*}
{if $smarty.request.action == 'new'}
  <h1>Neuigkeit eintragen</h1>
{else}
  <h1>Neuigkeit <i>{$row->title|sslash}</i> bearbeiten</h1>
{/if}

<form name="form" action="" method="post" enctype="multipart/form-data">
  <input type="hidden" name="save" value="1" />
  <table width="100%" cellpadding="3" cellspacing="3" border="0" class="rounded bgc_lgrey">
    <tbody><tr>
      {if $error != ''}
      <td class="cella" colspan="2">
        <font color="#FF0000">{$error}</font>
      </td>
    </tr>
    <tr>
      {/if}      
    <tr>
      <td class="cella">
        <span class="stdfont"><strong>Titel</strong></span>
      </td>
      <td class="cellb">
        <input type="text" name="title" class="rounded" value="{if $error == ''}{$row->title|sslash|escape:'html'}{else}{$smarty.request.title|sslash|escape:'html'}{/if}" size="50" maxlength="255" tabindex="1" />
      </td>
    </tr>
    <tr>
      <td class="cella">
        <span class="stdfont"><strong>Thema</strong></span>
      </td>
      <td class="cellb">
        <input type="text" name="topic" class="rounded" value="{if $error == ''}{$row->topic|sslash|escape:'html'}{else}{$smarty.request.topic|sslash|escape:'html'}{/if}" size="50" maxlength="255" tabindex="2" />
      </td>
    </tr>
    <tr>
      <td class="cella">
        <span class="stdfont"><strong>Autor</strong></span>
      </td>
      <td class="cellb">
        {*<input type="text" name="author" class="rounded" value="{if $error == ''}{$row->author|sslash}{else}{$smarty.request.author|sslash}{/if}" size="50" maxlength="255" tabindex="3" />*}
        <select name="author" class="rounded" tabindex="3">
          <option value="0">Bitte w&auml;hlen</option>
          {foreach from=$authors item=author}
            <option value="{$author->id}" {if $error != '' && $smarty.request.author == $author->id || $error == '' && $row->author == $author->id || $smarty.request.author == '' && $row->author == '' && $author->id == UID} selected="selected"{/if}>{$author->uname} ({$author->authortoken})</option>
          {/foreach}
        </select>
      </td>
    </tr>
    <tr>
      <td class="cella" valign="top">
        <span class="stdfont"><strong>Einleitungstext</strong></span>
      </td>
      <td class="cellb">
        <textarea name="teaser" class="rounded" tabindex="4" style="width:600px; height:70px;">{if $error == ''}{$row->teaser|sslash|escape:'html'}{else}{$smarty.request.teaser|sslash|escape:'html'}{/if}</textarea>        
      </td>
    </tr>    
    <tr>
      <td class="cella" valign="top">
        <span class="stdfont"><strong>Inhalt</strong></span>
      </td>
      <td class="cellb">
        <textarea name="text">{$text}</textarea>
      </td>
    </tr>
    <tr>
        <td class="cella" valign="top">
            <span class="stdfont"><strong>SEO Beschreibung</strong></span>
        </td>
        <td class="cellb">
            <textarea name="metadescription" cols="70" maxlength="255" tabindex="3">{if $error == ''}{$row->metadescription|sslash|escape:'html'}{else}{$smarty.request.metadescription|sslash|escape:'html'}{/if}</textarea>
        </td>
    </tr>
    <tr>
        <td class="cella" valign="top">
            <span class="stdfont"><strong>SEO Keywords</strong></span>
        </td>
        <td class="cellb">
            <input type="text" name="keywords" class="rounded" value="{if $error == ''}{$row->keywords|sslash|escape:'html'}{else}{$smarty.request.keywords|sslash|escape:'html'}{/if}" size="100" maxlength="255" tabindex="4" />
        </td>
    </tr>
    {if $smarty.request.action == 'edit'}
    <tr>
      <td class="cella" valign="top">
        <span class="stdfont"><strong>Aktiv?</strong></span>
      </td>
      <td class="cellb">
        <input type="checkbox" name="active" value="1" {if $error == '' && $row->active == 1 || $error != '' && $smarty.request.active == 1}checked="checked"{/if} tabindex="6" {if !permission($smarty.request.p,'active')}disabled="disabled"{/if} />
        {if !permission($smarty.request.p,'active')}<input type="hidden" name="active" value="{$row->active}" />{/if}
      </td>
    </tr>
    {/if}
    <tr>
      <td class="cellb" colspan="2">
        <button type="submit" name="submit" value="Speichern" class="active" tabindex="7"><span class="tc_green">Speichern</span></button>
        <button type="button" name="button" value="Zur&uuml;ck zur &Uuml;bersicht" class="active m_left" tabindex="8" onclick="window.location.href='{$admindomain}index.php?p=news&amp;action=overview&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}'"><span class="tc_red">Zur&uuml;ck zur &Uuml;bersicht</span></button>
      </td>
    </tr></tbody>
  </table>
</form>
<script type="text/javascript" src="{$admindomain}ckeditor/ckeditor.js"></script>
<script type="text/javascript">
	CKEDITOR.replace('text');
</script>
{/strip}