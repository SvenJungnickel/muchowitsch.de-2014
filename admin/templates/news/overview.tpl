{strip}
<h1>Neuigkeiten - &Uuml;bersicht ({$num})</h1>

<table width="100%" cellpadding="3" cellspacing="1" border="0">
  <tbody><tr class="header">  
    <td>Neuigkeit</td>  
    <td>Erstell-Datum</td>
    <td>Uhrzeit</td>
    <td>Thema</td>
    <td>Autor</td>
    <td width="100">Aktionen</td>
  </tr>
  {foreach from=$news item=n name=n}
  <tr class="{if $smarty.foreach.n.iteration%2 == 0}bgc_white{else}bgc_grey{/if}">
    <td>
      {if $n->active == 1}
        <a href="{$domainname}index.php?p=archive&amp;action=details&amp;aid={$n->id}" target="_blank" class="bold">{$n->title|sslash}</a>
      {else}
        {$n->title|sslash}
      {/if}  
    </td>
    <td>{$n->ctime|date_format:"%d.%m.%Y"}</td>
    <td>{$n->ctime|date_format:"%H:%M"} Uhr</td>
    <td>{$n->topic}</td>
    <td>{$n->author}</td>
    <td>
      {if $n->active == 1}
        {if permission($smarty.request.p,'active') && (permission($smarty.request.p,'all') || UID == $n->uid)}
          <a href="{$admindomain}index.php?p=news&amp;action=active&amp;id={$n->id}&amp;option=0&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}"><img src="{$domainname}{$img_path}icons/accept.png" alt="accept" title="aktiviert, klicken um zu deaktivieren" /></a>&nbsp;
        {else}
          <img src="{$domainname}{$img_path}icons/accept.png" alt="accept" title="aktiviert" />&nbsp;
        {/if}
      {else}
        {if permission($smarty.request.p,'active') && (permission($smarty.request.p,'all') || UID == $n->uid)}
          <a href="{$admindomain}index.php?p=news&amp;action=active&amp;id={$n->id}&amp;option=1&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}"><img src="{$domainname}{$img_path}icons/cancel.png" alt="cancel" title="deaktiviert, klicken um zu aktivieren" /></a>&nbsp;
        {else}
          <img src="{$domainname}{$img_path}icons/cancel.png" alt="cancel" title="deaktiviert" />&nbsp;
        {/if}
      {/if}
      {if permission($smarty.request.p,'edit') && (permission($smarty.request.p,'all') || UID == $n->uid)}<a href="{$admindomain}index.php?p=news&amp;action=edit&amp;id={$n->id}&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}"><img src="{$domainname}{$img_path}icons/date_edit.png" alt="date_edit" title="Neuigkeit bearbeiten" /></a>&nbsp;{/if}
      {if permission($smarty.request.p,'delete') && (permission($smarty.request.p,'all') || UID == $n->uid)}<a href="javascript:;" onclick="if(confirm('Willst du diese Neuigkeit wirklich l&ouml;schen?')){ldelim}window.location.href='{$admindomain}index.php?p=news&amp;action=delete&amp;id={$n->id}&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}';{rdelim}"><img src="{$domainname}{$img_path}icons/date_delete.png" alt="date_delete" title="Neuigkeit l&ouml;schen" /></a>&nbsp;{/if}
    </td>
  </tr>
  {foreachelse}
  <tr class="bgc_grey">
    <td colspan="6" align="center">Es wurden noch keine Neuigkeiten angelegt.</td>
  </tr>
  {/foreach}
  {if $navi}
  <tr class="bgc_grey">
    <td colspan="6">{$navi}</td>
  </tr>  
  {/if}
  </tbody>
</table>
{/strip}