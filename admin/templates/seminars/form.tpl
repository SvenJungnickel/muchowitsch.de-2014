{strip}
{* JQuery Datepicker *}
<link type="text/css" rel="stylesheet" href="{$domainname}{$css_path}ui.theme.css" media="screen" />
<link type="text/css" rel="stylesheet" href="{$domainname}{$css_path}ui.core.css" media="screen" />
<link type="text/css" rel="stylesheet" href="{$domainname}{$css_path}ui.datepicker.css" media="screen" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script> 
<script type="text/javascript" src="{$domainname}{$js_path}ui.datepicker/ui.datepicker.js"></script>
<script type="text/javascript" src="{$domainname}{$js_path}ui.datepicker/ui.datepicker-de.js"></script>
<script type="text/javascript">
    jQuery(function($) {ldelim}
        $(".dateinput").datepicker();
        $.datepicker.setDefaults({ldelim}
            dateFormat: 'dd.mm.yy'
        {rdelim});
    {rdelim});

    function updateTimeEnd(sourceObj,targetObj) {ldelim}
        targetObj.val(sourceObj.val());
    {rdelim}
</script>
{if $smarty.request.action == 'new'}
  <h1>Neues Seminar eintragen</h1>
{else}
  <h1>Seminar am <i>{$row->time_start|date_format:"%d.%m.%Y"} in {$row->city|sslash}</i> bearbeiten</h1>
{/if}

<form name="form" action="" method="post" enctype="multipart/form-data">
  <input type="hidden" name="save" value="1" />
  <table width="100%" cellpadding="3" cellspacing="3" border="0" class="rounded bgc_lgrey">
    <tbody><tr>
      {if $error != ''}
      <td class="cella" colspan="2">
        <font color="#FF0000">{$error}</font>
      </td>
    </tr>
    <tr>
      {/if}      
    <tr>
      <td class="cella">
        <span class="stdfont"><strong>Typ</strong></span>
      </td>
      <td class="cellb">
        <select name="type" tabindex="1">
          <option value="">Bitte w&auml;hlen</option>
          {foreach from=$types item=t}
            <option value="{$t->id}"{if $error == '' && $row->tid == $t->id || $error != '' && $smarty.request.type == $t->id} selected="selected"{/if}>{$t->title|sslash}{if $t->type != ''} ({$t->type|sslash}){/if}</option>
          {/foreach}
        </select>
      </td>
    </tr>
    <tr>
      <td class="cella">
        <span class="stdfont"><strong>Stadt</strong></span>
      </td>
      <td class="cellb">
        <select name="city" tabindex="2">
          <option value="">Bitte w&auml;hlen</option>
          {foreach from=$cities item=c}
            <option value="{$c->id}"{if $error == '' && $row->cid == $c->id || $error != '' && $smarty.request.city == $c->id} selected="selected"{/if}>{$c->city|sslash} ({$c->state|sslash})</option>
          {/foreach}
        </select>
      </td>
    </tr>
    <tr>
      <td class="cella" valign="top">
        <span class="stdfont"><strong>Start des Seminars</strong></span>
      </td>
      <td class="cellb">
        <input type="text" name="time_start" id="time_start" class="dateinput rounded" value="{if $error == '' && $row}{$row->time_start|date_format:'%d.%m.%Y'}{elseif $error != ''}{$smarty.request.time_start}{else}{$smarty.now|date_format:'%d.%m.%Y'}{/if}" size="10" maxlength="10" tabindex="3" onchange="updateTimeEnd($(this),$('#time_end'))" />
      </td>
    </tr>
    <tr>
      <td class="cella" valign="top">
        <span class="stdfont"><strong>Ende des Seminars</strong></span>
      </td>
      <td class="cellb">
        <input type="text" name="time_end" id="time_end" class="dateinput rounded" value="{if $error == '' && $row}{$row->time_end|date_format:'%d.%m.%Y'}{elseif $error != ''}{$smarty.request.time_end}{else}{$timeend|date_format:'%d.%m.%Y'}{/if}" size="10" maxlength="10" tabindex="4" />
      </td>
    </tr>
    <tr>
      <td class="cella" valign="top">
        <span class="stdfont"><strong>Seminarzeiten</strong></span>
      </td>
      <td class="cellb">
        <input type="text" name="time" class="rounded" value="{if $error == ''}{$row->time|sslash}{else}{$smarty.request.time|sslash}{/if}" size="50" maxlength="255" tabindex="5" />   
      </td>
    </tr>
    <tr>
      <td class="cella" valign="top">
        <span class="stdfont"><strong>Aktiv?</strong></span>
      </td>
      <td class="cellb">
        <input type="checkbox" name="active" value="1" {if $error == '' && $row->active == 1 || $error != '' && $smarty.request.active == 1 || $smarty.request.action == 'new'}checked="checked"{/if} tabindex="6" />
      </td>
    </tr>
    <tr>
      <td class="cellb" colspan="2">
        <button type="submit" name="submit" value="Speichern" class="active" tabindex="7"><span class="tc_green">Speichern</span></button>
        <button type="button" name="button" value="Zur&uuml;ck zur &Uuml;bersicht" class="active m_left" tabindex="8" onclick="window.location.href='{$admindomain}index.php?p=seminars&amp;action=overview&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}'"><span class="tc_red">Zur&uuml;ck zur &Uuml;bersicht</span></button>
      </td>
    </tr></tbody>
  </table>
</form>
{/strip}