{strip}
<h1>Seminartypen - &Uuml;bersicht ({$num})</h1>

<table width="100%" cellpadding="3" cellspacing="1" border="0">
  <tbody><tr class="header">  
    <td>Titel</td>
    <td>Kurz-Titel</td>
    <td>Typ</td> 
    <td width="100">Aktionen</td>
  </tr>
  {foreach from=$types item=t name=t}
  <tr class="{if $smarty.foreach.s.iteration%2 == 0}bgc_white{else}bgc_grey{/if}">
    <td>
      <span class="bold">{$t->title|sslash}</span>
    </td>
    <td>{$t->shorttitle|sslash}</td>
    <td>{$t->type|sslash}</td>
    <td>
      <a href="{$admindomain}index.php?p=seminars&amp;action=type_edit&amp;id={$t->id}&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}"><img src="{$domainname}{$img_path}icons/date_edit.png" alt="date_edit" title="Seminartyp bearbeiten" /></a>&nbsp;
      <a href="javascript:;" onclick="if(confirm('Willst du diesen Seminartyp wirklich l&ouml;schen?')){ldelim}window.location.href='{$admindomain}index.php?p=seminars&amp;action=type_delete&amp;id={$t->id}&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}';{rdelim}"><img src="{$domainname}{$img_path}icons/date_delete.png" alt="date_delete" title="Seminartyp l&ouml;schen" /></a>&nbsp;
    </td>
  </tr>
  {foreachelse}
  <tr class="bgc_grey">
    <td colspan="4" align="center">Es wurden noch keine Seminartypen angelegt.</td>
  </tr>
  {/foreach}
  {if $navi}
  <tr class="bgc_grey">
    <td colspan="4">{$navi}</td>
  </tr>  
  {/if}
  <tr class="bgc_white">
  	<td colspan="4">
  	  <button type="button" name="button" value="neuen Seminartyp anlegen" class="active" onclick="window.location.href='{$admindomain}index.php?p=seminars&amp;action=type_new'">neuen Seminartyp anlegen</button>
  	  <button type="button" name="button" value="Zur&uuml;ck" class="active m_left" tabindex="8" onclick="window.location.href='{$admindomain}index.php?p=seminars&amp;action=overview'"><span class="tc_red">Zur&uuml;ck</span></button> 
  	</td>  	
  </tr>
  </tbody>
</table>
{/strip}