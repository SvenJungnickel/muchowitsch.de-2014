{strip}
<h1>St&auml;dte - &Uuml;bersicht ({$num})</h1>

<table width="100%" cellpadding="3" cellspacing="1" border="0">
  <tbody><tr class="header">  
    <td>Stadt</td>  
    <td>Bundesland</td>
    <td width="100">Aktionen</td>
  </tr>
  {foreach from=$cities item=c name=c}
  <tr class="{if $smarty.foreach.s.iteration%2 == 0}bgc_white{else}bgc_grey{/if}">
    <td>
      <span class="bold">{$c->title|sslash}</span>
    </td>
    <td>{$c->state|sslash}</td>
    <td>
      <a href="{$admindomain}index.php?p=seminars&amp;action=city_edit&amp;id={$c->id}&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}"><img src="{$domainname}{$img_path}icons/date_edit.png" alt="date_edit" title="Stadt bearbeiten" /></a>&nbsp;
      <a href="javascript:;" onclick="if(confirm('Willst du diese Stadt wirklich l&ouml;schen?')){ldelim}window.location.href='{$admindomain}index.php?p=seminars&amp;action=city_delete&amp;id={$c->id}&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}';{rdelim}"><img src="{$domainname}{$img_path}icons/date_delete.png" alt="date_delete" title="Stadt l&ouml;schen" /></a>&nbsp;
    </td>
  </tr>
  {foreachelse}
  <tr class="bgc_grey">
    <td colspan="3" align="center">Es wurden noch keine St&auml;dte angelegt.</td>
  </tr>
  {/foreach}
  {if $navi}
  <tr class="bgc_grey">
    <td colspan="3">{$navi}</td>
  </tr>  
  {/if}
  <tr class="bgc_white">
  	<td colspan="3">
  	  <button type="button" name="button" value="neue Stadt anlegen" class="active" onclick="window.location.href='{$admindomain}index.php?p=seminars&amp;action=city_new'">neue Stadt anlegen</button>
  	  <button type="button" name="button" value="Zur&uuml;ck" class="active m_left" tabindex="8" onclick="window.location.href='{$admindomain}index.php?p=seminars&amp;action=overview'"><span class="tc_red">Zur&uuml;ck</span></button> 
  	</td>  	
  </tr>
  </tbody>
</table>
{/strip}