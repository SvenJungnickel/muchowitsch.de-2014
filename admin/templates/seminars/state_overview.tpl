{strip}
<h1>Bundesl&auml;nder - &Uuml;bersicht ({$num})</h1>

<table width="100%" cellpadding="3" cellspacing="1" border="0">
  <tbody><tr class="header">  
    <td>Bundesland</td>  
    <td>St&auml;dte</td>
    <td width="100">Aktionen</td>
  </tr>
  {foreach from=$states item=s name=s}
  <tr class="{if $smarty.foreach.s.iteration%2 == 0}bgc_white{else}bgc_grey{/if}">
    <td>
      <span class="bold">{$s->title|sslash}</span>
    </td>
    <td>{foreach from=$s->cities item=c name=c}{$c->title|sslash}{if !$smarty.foreach.c.last}, {/if}{foreachelse}<i>Keine St&auml;dte</i>{/foreach}</td>
    <td>
      <a href="{$admindomain}index.php?p=seminars&amp;action=state_edit&amp;id={$s->id}&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}"><img src="{$domainname}{$img_path}icons/date_edit.png" alt="date_edit" title="Bundesland bearbeiten" /></a>&nbsp;
      <a href="javascript:;" onclick="if(confirm('Willst du dieses Bundesland wirklich l&ouml;schen?')){ldelim}window.location.href='{$admindomain}index.php?p=seminars&amp;action=state_delete&amp;id={$s->id}&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}';{rdelim}"><img src="{$domainname}{$img_path}icons/date_delete.png" alt="date_delete" title="Bundesland l&ouml;schen" /></a>&nbsp;
    </td>
  </tr>
  {foreachelse}
  <tr class="bgc_grey">
    <td colspan="3" align="center">Es wurden noch keine Bundesl&auml;nder angelegt.</td>
  </tr>
  {/foreach}
  {if $navi}
  <tr class="bgc_grey">
    <td colspan="3">{$navi}</td>
  </tr>  
  {/if}
  <tr class="bgc_white">
  	<td colspan="3">
  	  <button type="button" name="button" value="neues Bundesland anlegen" class="active" onclick="window.location.href='{$admindomain}index.php?p=seminars&amp;action=state_new'">neues Bundesland anlegen</button>
  	  <button type="button" name="button" value="Zur&uuml;ck" class="active m_left" tabindex="8" onclick="window.location.href='{$admindomain}index.php?p=seminars&amp;action=overview'"><span class="tc_red">Zur&uuml;ck</span></button> 
  	</td>  	
  </tr>
  </tbody>
</table>
{/strip}