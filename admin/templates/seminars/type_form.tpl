{strip}
{if $smarty.request.action == 'type_new'}
  <h1>Neuen Seminartyp eintragen</h1>
{else}
  <h1>Seminartyp <i>{$row->title|sslash}</i> bearbeiten</h1>
{/if}

<form name="form" action="" method="post" enctype="multipart/form-data">
  <input type="hidden" name="save" value="1" />
  <table width="100%" cellpadding="3" cellspacing="3" border="0" class="rounded bgc_lgrey">
    <tbody><tr>
      {if $error != ''}
      <td class="cella" colspan="2">
        <font color="#FF0000">{$error}</font>
      </td>
    </tr>
    <tr>
      {/if}      
    <tr>
      <td class="cella">
        <span class="stdfont"><strong>Titel</strong></span>
      </td>
      <td class="cellb">
        <input type="text" name="title" class="rounded" value="{if $error == ''}{$row->title|sslash|escape:'html'}{else}{$smarty.request.title|sslash|escape:'html'}{/if}" size="50" maxlength="255" tabindex="1" />
      </td>
    </tr>
    <tr>
      <td class="cella">
        <span class="stdfont"><strong>Kurz-Titel</strong></span>
      </td>
      <td class="cellb">
        <input type="text" name="shorttitle" class="rounded" value="{if $error == ''}{$row->shorttitle|sslash|escape:'html'}{else}{$smarty.request.shorttitle|sslash|escape:'html'}{/if}" size="50" maxlength="255" tabindex="1" />
      </td>
    </tr>
    <tr>
      <td class="cella">
        <span class="stdfont"><strong>Typ</strong></span>
      </td>
      <td class="cellb">
        <input type="text" name="type" class="rounded" value="{if $error == ''}{$row->type|sslash|escape:'html'}{else}{$smarty.request.type|sslash|escape:'html'}{/if}" size="50" maxlength="255" tabindex="2" />
      </td>
    </tr>    
    <tr>
      <td class="cellb" colspan="2">
        <button type="submit" name="submit" value="Speichern" class="active" tabindex="3"><span class="tc_green">Speichern</span></button>
        <button type="button" name="button" value="Zur&uuml;ck zur &Uuml;bersicht" class="active m_left" tabindex="4" onclick="window.location.href='{$admindomain}index.php?p=seminars&amp;action=type&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}'"><span class="tc_red">Zur&uuml;ck zur &Uuml;bersicht</span></button>
      </td>
    </tr></tbody>
  </table>
</form>
{/strip}