{strip}
<h1>Seminare - &Uuml;bersicht ({$num})</h1>

<table width="100%" cellpadding="3" cellspacing="1" border="0">
  <tbody><tr class="header">  
    <td>Title</td>  
    <td>Start</td>
    <td>Ende</td>
    <td>Uhrzeit</td>
    <td>Ort</td>
    <td width="100">Aktionen</td>
  </tr>
  {foreach from=$seminars item=s name=s}
  <tr class="{if $smarty.foreach.s.iteration%2 == 0}bgc_white{else}bgc_grey{/if}">
    <td>
      {if $s->active == 1 && $s->time_end|date_format:"%Y%m%d" >= $smarty.now|date_format:"%Y%m%d"}
        <a href="{$domainname}index.php?p=seminars&amp;id={$s->id}" target="_blank" class="bold">{$s->type|sslash}</a>
      {else}
        {$s->type|sslash}
      {/if}  
    </td>
    <td>{$s->time_start|date_format:"%A %d.%m.%Y"}</td>
    <td>{$s->time_end|date_format:"%A %d.%m.%Y"}</td>
    <td>{$s->time|sslash}</td>
    <td><span class="bold">{$s->city|sslash}</span> ({$s->state|sslash})</td>
    <td>
      {if $s->active == 1}
        {if permission($smarty.request.p,'active') && (permission($smarty.request.p,'all') || UID == $n->uid)}
          <a href="{$admindomain}index.php?p=seminars&amp;action=active&amp;id={$s->id}&amp;option=0&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}"><img src="{$domainname}{$img_path}icons/accept.png" alt="accept" title="aktiviert, klicken um zu deaktivieren" /></a>&nbsp;
        {else}
          <img src="{$domainname}{$img_path}icons/accept.png" alt="accept" title="aktiviert" />&nbsp;
        {/if}
      {else}
        {if permission($smarty.request.p,'active') && (permission($smarty.request.p,'all') || UID == $n->uid)}
          <a href="{$admindomain}index.php?p=seminars&amp;action=active&amp;id={$s->id}&amp;option=1&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}"><img src="{$domainname}{$img_path}icons/cancel.png" alt="cancel" title="deaktiviert, klicken um zu aktivieren" /></a>&nbsp;
        {else}
          <img src="{$domainname}{$img_path}icons/cancel.png" alt="cancel" title="deaktiviert" />&nbsp;
        {/if}
      {/if}
      {if permission($smarty.request.p,'edit')}<a href="{$admindomain}index.php?p=seminars&amp;action=edit&amp;id={$s->id}&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}"><img src="{$domainname}{$img_path}icons/date_edit.png" alt="date_edit" title="Neuigkeit bearbeiten" /></a>&nbsp;{/if}
      {if permission($smarty.request.p,'delete')}<a href="javascript:;" onclick="if(confirm('Willst du dieses Seminar wirklich l&ouml;schen?')){ldelim}window.location.href='{$admindomain}index.php?p=seminars&amp;action=delete&amp;id={$s->id}&amp;pp={$smarty.request.pp}&amp;page={$smarty.request.page}';{rdelim}"><img src="{$domainname}{$img_path}icons/date_delete.png" alt="date_delete" title="Seminar l&ouml;schen" /></a>&nbsp;{/if}
    </td>
  </tr>
  {foreachelse}
  <tr class="bgc_grey">
    <td colspan="6" align="center">Es wurden noch keine Seminare eingetragen.</td>
  </tr>
  {/foreach}
  {if $navi}
  <tr class="bgc_grey">
    <td colspan="6">{$navi}</td>
  </tr>  
  {/if}
  <tr class="bgc_white">
  	<td colspan="6">
  	  <button type="button" name="button" value="Bundesl&auml;nder" class="active" onclick="window.location.href='{$admindomain}index.php?p=seminars&amp;action=state'">Bundesl&auml;nder</button>
  	  <button type="button" name="button" value="St&auml;dte" class="active m_left" onclick="window.location.href='{$admindomain}index.php?p=seminars&amp;action=city'">St&auml;dte</button>
  	  <button type="button" name="button" value="Seminartypen" class="active m_left" onclick="window.location.href='{$admindomain}index.php?p=seminars&amp;action=type'">Seminartypen</button>
  	</td>  	
  </tr>
  </tbody>
</table>
{/strip}