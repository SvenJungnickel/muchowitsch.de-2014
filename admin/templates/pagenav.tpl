{strip}
  <div style="text-align:center; padding:5px;">
    <div style="float:left">
      <button type="button"{if $site != 1} class="active"{/if} value="Erste Seite"{if $site != 1} onclick="window.location.href = '{$link_first}';"{/if}>Erste Seite</button>
    </div>
      
    <div style="float:right">
      <button type="button"{if $site != $maxsites} class="active"{/if} value="Letzte Seite"{if $site != $maxsites} onclick="window.location.href = '{$link_last}';"{/if}>Letzte Seite</button>
    </div>

    <button type="button"{if $site != 1} class="active"{/if} value="&laquo; zur&uuml;ck"{if $site != 1} onclick="window.location.href = '{$link_prev}';"{/if}>&laquo; zur&uuml;ck</button>
    
    {foreach from=$pages item=seite}
      {if $seite.current_site == $site}
        <button type="button" class="m_left" style="color:#1B6ECC;" value="{$site}">{$site}</button>
      {else}
        <button type="button" class="m_left active" value="{$seite.current_site}" onclick="window.location.href = '{$seite.link}';">{$seite.current_site}</button>
      {/if}
    {/foreach}
    
    <button type="button" class="m_left{if $site != $maxsites} active{/if}" value="weiter &raquo;"{if $site != $maxsites} onclick="window.location.href = '{$link_next}';"{/if}>weiter &raquo;</button>

    <div style="clear:both;"></div>
  </div>
{/strip}