{strip}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <title>{$title}</title>
    <link rel="stylesheet" type="text/css" href="{$domainname}{$css_path}standard.css" />
    <link rel="stylesheet" type="text/css" href="{$admindomain}templates/admin.css" />
    {if $error == ''}<meta http-equiv="Refresh" content="3;URL={$admindomain}index.php?p=start" />{/if}
</head>
<body class="admin_body">

<form name="form" action="" method="post" enctype="multipart/form-data">
<input type="hidden" name="send" value="1" />
<div style="margin-top:200px;">
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tbody><tr align="center">
      <td>        
        <table class="border-col rounded p_all shadow" border="0" cellpadding="3" cellspacing="1" width="450">
          <tbody><tr>
            <td align="center" style="background-image:url({$domainname}{$img_path}logo-fortuna-spacer.jpg);background-repeat:repxeat-x;">
              <h2>Passwort best&auml;tigen</h2>
            </td>
          </tr>
          <tr>
            <td>
              <table cellpadding="3" cellspacing="1" border="0" width="100%">
                <tbody><tr>
                  {if $error != ''}
                    <td class="cella" colspan="2">
                      <font color="#FF0000">{$error}</font>
                    </td>
                  </tr>
                  <tr>
                  {else}
                  <td class="cella" colspan="2">
                      <font color="#009900">Dein Passwort wurde nun ge�ndert. Du wirst gleich weitergeleitet...</font><br /><br  />
                      <a href="{$admindomain}index.php?p=start">Klicke hier wenn du sofort weitergeleitet werden m&ouml;chtest.</a>
                    </td>
                  </tr>
                  {/if} 
                </tbody>
              </table>
            </td>
          </tr></tbody>
        </table>
      </td>
    </tr></tbody>
  </table>
</div>
</form>

{include file="foot.tpl"}
{/strip}