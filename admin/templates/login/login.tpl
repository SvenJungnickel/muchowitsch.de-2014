{strip}
{include file="head.tpl"}

<form name="form" action="" method="post" enctype="multipart/form-data">
<input type="hidden" name="send" value="1" />
<div style="margin-top:200px;">  
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tbody><tr align="center">
      <td>        
        <table class="border-col b_all bc_lgrey rounded p_all shadow loginbg" border="0" cellpadding="3" cellspacing="1" width="250">
          <tbody><tr>
            <td align="center">
              <h2>Administrationsbereich muchowitsch.de - Login</h2>
            </td>
          </tr>
          <tr>
            <td>
              <table cellpadding="3" cellspacing="1" border="0" width="100%">
                <tbody><tr>
                  {if $error != ''}
                    <td class="cella" colspan="2">
                      <font color="#FF0000">{$error}</font>
                    </td>
                  </tr>
                  <tr>
                  {/if}
                  <td class="cella">
                    <span class="stdfont"><strong>Benutzername</strong></span>
                  </td>
                  <td class="cellb">
                    <input type="text" name="uname" value="{$smarty.request.uname}" size="50" maxlength="255" tabindex="1" />
                  </td>
                </tr>
                <tr>
                  <td class="cella">
                    <span class="stdfont"><strong>Passwort</strong></span>
                  </td>
                  <td class="cellb">
                    <input type="password" name="password" value="" size="50" maxlength="255" tabindex="2" />
                  </td>
                </tr>
                <tr>
                  <td class="cella">
                    <span class="stdfont"><strong>Login speichen?</strong></span>
                  </td>
                  <td class="cellb">
                    <input type="checkbox" name="setcookie" value="1" tabindex="3" />
                  </td>
                </tr>
                <tr>
                  <td class="cellb" colspan="2">
                    <a href="{$admindomain}index.php?p=login&amp;action=pw_recover" class="bold">Passwort vergessen?</a>
                  </td>
                </tr>
                <tr>
                  <td class="cellb" colspan="2">
                    <button type="submit" name="submit" value="Login" class="active" tabindex="4">Login</button>
                  </td>
                </tr></tbody>
              </table>
            </td>
          </tr></tbody>
        </table>
      </td>
    </tr></tbody>
  </table>
</div>
</form>

{include file="foot.tpl"}
{/strip}