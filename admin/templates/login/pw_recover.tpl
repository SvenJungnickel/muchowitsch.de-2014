{strip}
{include file="head.tpl"}

<form name="form" action="" method="post" enctype="multipart/form-data">
<input type="hidden" name="send" value="1" />
<div style="margin-top:200px;">
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tbody><tr align="center">
      <td>        
        <table class="border-col rounded p_all shadow" border="0" cellpadding="3" cellspacing="1" width="250">
          <tbody><tr>
            <td align="center" style="background-image:url({$domainname}{$img_path}logo-fortuna-spacer.jpg);background-repeat:repxeat-x;">
              <h2>Passwort vergessen?</h2>
            </td>
          </tr>
          <tr>
            <td>
              <table cellpadding="3" cellspacing="1" border="0" width="100%">
                <tbody><tr>
                  {if $error != ''}
                    <td class="cella" colspan="2">
                      <font color="#FF0000">{$error}</font>
                    </td>
                  </tr>
                  <tr>
                  {elseif $thankyou == 1}
                  <td class="cella" colspan="2">
                      <font color="#009900">Dein neues Passwort wurde dir auf die angegebene Email Adresse geschickt.<br />
                        Klicke auf den Link in der Email, um das neue Passwort zu best&auml;tigen.</font>
                    </td>
                  </tr>
                  <tr>
                  {/if}
                  <td class="cella">
                    <span class="stdfont"><strong>Benutzername</strong></span>
                  </td>
                  <td class="cellb">
                    <input type="text" name="uname" value="{$smarty.request.uname}" size="50" maxlength="255" tabindex="1" />
                  </td>
                </tr>
                <tr>
                  <td class="cella">
                    <span class="stdfont"><strong>Email</strong></span>
                  </td>
                  <td class="cellb">
                    <input type="text" name="email" value="{$smarty.request.email}" size="50" maxlength="255" tabindex="2" />
                  </td>
                </tr>
                <tr>
                  <td class="cellb" colspan="2">
                    <a href="{$admindomain}index.php?p=login&" class="bold">Zur&uuml;ck zum Login</a>
                  </td>
                </tr>        
                {if $thankyou != 1}
                <tr>
                  <td class="cellb" colspan="2">
                    <button type="submit" name="submit" value="Neues Passwort genieren" class="active" tabindex="4">Neues Passwort genieren</button>
                  </td>
                </tr>
                {/if}
                </tbody>
              </table>
            </td>
          </tr></tbody>
        </table>
      </td>
    </tr></tbody>
  </table>
</div>
</form>

{include file="foot.tpl"}
{/strip}