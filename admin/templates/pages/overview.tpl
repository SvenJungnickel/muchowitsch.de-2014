{strip}
<h1>Inhalte - &Uuml;bersicht</h1>

<table width="100%" cellpadding="3" cellspacing="0" border="0">
  <tbody><tr class="header">  
    <td width="250">Seite</td>
    <td>Inhalt</td>
    <td width="100">Aktionen</td>
  </tr>
  {foreach from=$pages item=page name=page}
  <tr class="bgc_grey">
    <td width="250">
      {if $page->active == 1}
        <a href="{$domainname}index.php?p=page&amp;id={$page->id}" target="_blank" class="bold fs_14">{$page->title|sslash}</a>
      {else}
        <span class="fs_14">{$page->title|sslash}</span>
      {/if}
    </td>
    <td>{$page->content}</td>
    <td width="100" align="right">
      {if $page->active == 1}
        {if permission($smarty.request.p,'active')}
          <a href="{$admindomain}index.php?p=pages&amp;action=active&amp;id={$page->id}&amp;option=0"><img src="{$domainname}{$img_path}icons/accept.png" alt="accept" title="aktiviert, klicken um zu deaktivieren" /></a>&nbsp;
        {else}
          <img src="{$domainname}{$img_path}icons/accept.png" alt="accept" title="aktiviert" />&nbsp;
        {/if}
      {else}
        {if permission($smarty.request.p,'active')}
          <a href="{$admindomain}index.php?p=pages&amp;action=active&amp;id={$page->id}&amp;option=1"><img src="{$domainname}{$img_path}icons/cancel.png" alt="cancel" title="deaktiviert, klicken um zu aktivieren" /></a>&nbsp;
        {else}
          <img src="{$domainname}{$img_path}icons/cancel.png" alt="cancel" title="deaktiviert" />&nbsp;
        {/if}
      {/if}
      {if $page->display == 1}
        {if permission($smarty.request.p,'active')}
          <a href="{$admindomain}index.php?p=pages&amp;action=display&amp;id={$page->id}&amp;option=0"><img src="{$domainname}{$img_path}icons/tick.png" alt="tick" title="eingeblendet, klicken um auszublenden" /></a>&nbsp;
        {else}
          <img src="{$domainname}{$img_path}icons/tick.png" alt="tick" title="eingeblendet" />&nbsp;
        {/if}
      {else}
        {if permission($smarty.request.p,'active')}
          <a href="{$admindomain}index.php?p=pages&amp;action=display&amp;id={$page->id}&amp;option=1"><img src="{$domainname}{$img_path}icons/cross.png" alt="cross" title="ausgeblendet, klicken um einzublenden" /></a>&nbsp;
        {else}
          <img src="{$domainname}{$img_path}icons/cross.png" alt="cross" title="ausgeblendet" />&nbsp;
        {/if}
      {/if}
      {if permission($smarty.request.p,'edit')}        
        <a href="{$admindomain}index.php?p=pages&amp;action=order&amp;id={$page->id}"><img src="{$domainname}{$img_path}icons/application_cascade.png" alt="application_cascade" title="Reihenfolge bearbeiten" /></a>&nbsp;
        <a href="{$admindomain}index.php?p=pages&amp;action=edit&amp;id={$page->id}"><img src="{$domainname}{$img_path}icons/date_edit.png" alt="date_edit" title="Inhalt bearbeiten" /></a>&nbsp;
      {/if}
      {if permission($smarty.request.p,'delete')}<a href="javascript:;" onclick="if(confirm('Willst du diese Inhaltseite wirklich l&ouml;schen?')){ldelim}window.location.href='{$admindomain}index.php?p=pages&amp;action=delete&amp;id={$page->id}';{rdelim}"><img src="{$domainname}{$img_path}icons/date_delete.png" alt="date_delete" title="Inhaltseite l&ouml;schen" /></a>&nbsp;{/if}
    </td>
  </tr>
  {if $page->subnavi}
  <tr>
    <td colspan="3">
      {subpages id=$page->id}
    </td>
  </tr>
  {/if}
  {foreachelse}
  <tr class="bgc_grey">
    <td colspan="4" align="center">Es wurden noch keine Inhaltsseiten angelegt.</td>
  </tr>
  {/foreach}
  </tbody>
</table>
{/strip}