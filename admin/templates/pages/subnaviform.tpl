{foreach from=$subnavi->subnavi item=subnavi name=subnavi}
<li>
  <input type="radio" name="menu" value="{$subnavi->id}" id="radio_{$subnavi->id}" {if $error == '' && $row->parent == $subnavi->id || $error != '' && $smarty.request.menu == $subnavi->id}checked="checked"{/if}{if $subnavi->id == $smarty.request.id} disabled="disabled"{/if} />
  {if $subnavi->id == $smarty.request.id}
    {$subnavi->title}
  {else}  
    <label for="radio_{$subnavi->id}" class="pointer">{$subnavi->title}</label>
  {/if}
  {if $subnavi->subnavi}
    <ul>{subpages id=$subnavi->id}</ul>
  {/if}
</li>
{/foreach}