<link type="text/css" rel="stylesheet" href="{$domainname}{$css_path}ui.theme.css" media="screen" />
<style>
	#sortable {ldelim} list-style-type: none; margin: 0; padding: 0; width: 60%; {rdelim}
	#sortable li {ldelim} margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; {rdelim}
	#sortable li span {ldelim} position: absolute; margin-left: -1.3em; {rdelim}
</style>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="{$domainname}{$js_path}jquery.ui/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="{$domainname}{$js_path}jquery.ui/jquery.ui.widget.min.js"></script>
<script type="text/javascript" src="{$domainname}{$js_path}jquery.ui/jquery.ui.mouse.min.js"></script>
<script type="text/javascript" src="{$domainname}{$js_path}jquery.ui/jquery.ui.sortable.min.js"></script>
<script type="text/javascript">
{literal}
$(function() { 
  $( "#sortable" ).sortable({
  	update: function(){
  	  var order = $(this).sortable('serialize', {key: 'order[]'});
  	  $.post('{/literal}{$admindomain}{literal}index.php?p=pages&action=updateOrder&'+order, function(res){
  	  	if(res != 1) alert(res);
  	  	else {
  	  	  $('#showSaveSuccess').show().fadeOut(1000);
  	  	}
  	  });
  	}  	
  });
  $( "#sortable" ).disableSelection();
});
{/literal}
</script>

{strip}
<h1>Inhalte - Reihenfolge &auml;ndern</h1>

<div id="showSaveSuccess" class="m_all dpl_none">
  <p class="tc_green">Gespeichert...</p>
</div>

  <ul id="sortable">  		
  {foreach from=$pages item=page name=page}
    <li class="ui-state-default box235" id="id_{$page->id}">
      <span{if $page->active == 1} class="bold"{/if}>
        {$page->title|sslash}
      </span>
    </li>
  {/foreach}
  </ul>

<button type="button" name="button" value="Zur&uuml;ck zur &Uuml;bersicht" class="active m_top" tabindex="8" onclick="window.location.href='{$admindomain}index.php?p=pages&amp;action=overview'"><span class="tc_red">Zur&uuml;ck zur &Uuml;bersicht</span></button>
{/strip}