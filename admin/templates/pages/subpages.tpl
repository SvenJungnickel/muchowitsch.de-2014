{strip}
{foreach from=$subnavi->subnavi item=subnavi name=subnavi}
<table width="100%" cellpadding="3" cellspacing="0" border="0" class="bgc_white m_left">
  <tr>
    <td width="250">
      {if $subnavi->active == 1}
        <a href="{$domainname}index.php?p=page&amp;id={$subnavi->id}" target="_blank" class="bold">{$subnavi->title|sslash}</a>
      {else}
        {$subnavi->title|sslash}
      {/if}
    </td>
    <td>{$subnavi->content}</td>
    <td width="100" align="right">
      {if $subnavi->active == 1}
        {if permission($smarty.request.p,'active')}
          <a href="{$admindomain}index.php?p=pages&amp;action=active&amp;id={$subnavi->id}&amp;option=0"><img src="{$domainname}{$img_path}icons/accept.png" alt="accept" title="aktiviert, klicken um zu deaktivieren" /></a>&nbsp;
        {else}
          <img src="{$domainname}{$img_path}icons/accept.png" alt="accept" title="aktiviert" />&nbsp;
        {/if}
      {else}
        {if permission($smarty.request.p,'active')}
          <a href="{$admindomain}index.php?p=pages&amp;action=active&amp;id={$subnavi->id}&amp;option=1"><img src="{$domainname}{$img_path}icons/cancel.png" alt="cancel" title="deaktiviert, klicken um zu aktivieren" /></a>&nbsp;
        {else}
          <img src="{$domainname}{$img_path}icons/cancel.png" alt="cancel" title="deaktiviert" />&nbsp;
        {/if}
      {/if}
      {if $subnavi->display == 1}
        {if permission($smarty.request.p,'active')}
          <a href="{$admindomain}index.php?p=pages&amp;action=display&amp;id={$subnavi->id}&amp;option=0"><img src="{$domainname}{$img_path}icons/tick.png" alt="tick" title="eingeblendet, klicken um auszugeblenden" /></a>&nbsp;
        {else}
          <img src="{$domainname}{$img_path}icons/tick.png" alt="tick" title="eingeblendet" />&nbsp;
        {/if}
      {else}
        {if permission($smarty.request.p,'active')}
          <a href="{$admindomain}index.php?p=pages&amp;action=display&amp;id={$subnavi->id}&amp;option=1"><img src="{$domainname}{$img_path}icons/cross.png" alt="cross" title="ausgeblendet, klicken um einzublenden" /></a>&nbsp;
        {else}
          <img src="{$domainname}{$img_path}icons/cross.png" alt="cross" title="ausgeblendet" />&nbsp;
        {/if}
      {/if}
      {if permission($smarty.request.p,'edit')}        
        <a href="{$admindomain}index.php?p=pages&amp;action=order&amp;id={$subnavi->id}"><img src="{$domainname}{$img_path}icons/application_cascade.png" alt="application_cascade" title="Reihenfolge bearbeiten" /></a>&nbsp;
        <a href="{$admindomain}index.php?p=pages&amp;action=edit&amp;id={$subnavi->id}"><img src="{$domainname}{$img_path}icons/date_edit.png" alt="date_edit" title="Inhalt bearbeiten" /></a>&nbsp;
      {/if}
      {if permission($smarty.request.p,'delete')}<a href="javascript:;" onclick="if(confirm('Willst du diese Inhaltseite wirklich l&ouml;schen?')){ldelim}window.location.href='{$admindomain}index.php?p=pages&amp;action=delete&amp;id={$subnavi->id}';{rdelim}"><img src="{$domainname}{$img_path}icons/date_delete.png" alt="date_delete" title="Inhaltseite l&ouml;schen" /></a>&nbsp;{/if}
    </td>
  </tr>
  {if $subnavi->subnavi}
  <tr>
    <td colspan="3">    
      {subpages id=$subnavi->id}
    </td>
  </tr>
  {/if}
</table>
{/foreach}
{/strip}