{strip}
{if $smarty.request.action == 'new'}
  <h1>Neuen Inhalt eintragen</h1>
{else}
  <h1>Inhalt <i>{$row->title|sslash}</i> bearbeiten</h1>
{/if}

<form name="form" action="" method="post" enctype="multipart/form-data">
  <input type="hidden" name="save" value="1" />
  <table width="100%" cellpadding="3" cellspacing="3" border="0" class="rounded bgc_lgrey">
    <tbody><tr>
      {if $error != ''}
      <td class="cella" colspan="2">
        <font color="#FF0000">{$error}</font>
      </td>
    </tr>
    <tr>
      {/if}      
    <tr>
      <td class="cella">
        <span class="stdfont"><strong>Titel</strong></span>
      </td>
      <td class="cellb">
        <input type="text" name="title" class="rounded" value="{if $error == ''}{$row->title|sslash|escape:'html'}{else}{$smarty.request.title|sslash|escape:'html'}{/if}" size="50" maxlength="255" tabindex="1" />
      </td>
    </tr>
    <tr>
      <td class="cella" valign="top">
        <span class="stdfont"><strong>Inhalt</strong></span>
      </td>
      <td class="cellb">
        <textarea name="text">{$text}</textarea>
      </td>
    </tr>
    <tr>
      <td class="cella" valign="top">
        <span class="stdfont"><strong>SEO Beschreibung</strong></span>
      </td>
      <td class="cellb">        
      	<textarea name="metadescription" cols="70" maxlength="255" tabindex="3">{if $error == ''}{$row->metadescription|sslash|escape:'html'}{else}{$smarty.request.metadescription|sslash|escape:'html'}{/if}</textarea>
      </td>
    </tr>
    <tr>
      <td class="cella" valign="top">
        <span class="stdfont"><strong>SEO Keywords</strong></span>
      </td>
      <td class="cellb">
        <input type="text" name="keywords" class="rounded" value="{if $error == ''}{$row->keywords|sslash|escape:'html'}{else}{$smarty.request.keywords|sslash|escape:'html'}{/if}" size="100" maxlength="255" tabindex="4" />
      </td>
    </tr>
    <tr>
      <td class="cella" valign="top">
        <span class="stdfont"><strong>&Uuml;bergeordneten Men&uuml;punkt w&auml;hlen</strong></span>
      </td>
      <td class="cellb">
        <ul>       
            <li>
              <input type="radio" name="menu" value="0" id="radio_0" {if $error == '' && $row->parent == 0 || $error != '' && $smarty.request.menu == 0}checked="checked"{/if} />
              <label for="radio_0" class="pointer">Hauptmen&uuml;punkt</label>
            </li>
        {foreach from=$menue item=m name=m}
            <li>
              <input type="radio" name="menu" value="{$m->id}" id="radio_{$m->id}" {if $error == '' && $row->parent == $m->id || $error != '' && $smarty.request.menu == $m->id}checked="checked"{/if}{if $m->id == $smarty.request.id} disabled="disabled"{/if} />
              {if $m->id == $smarty.request.id}
                {$m->title}
              {else}
                <label for="radio_{$m->id}" class="pointer">{$m->title}</label>
              {/if}
              {if $m->subnavi}
                <ul>{subpages id=$m->id}</ul>
              {/if}
            </li>
        {/foreach}
        </ul>
      </td>
    </tr>
    <tr>
      <td class="cella" valign="top">
        <span class="stdfont"><strong>Men&uuml;punkt Anzeigen?</strong></span>
      </td>
      <td class="cellb">
        <input type="checkbox" name="display" value="1" {if $error == '' && $row->display == 1 || $error != '' && $smarty.request.display == 1}checked="checked"{/if} tabindex="4" {if !permission($smarty.request.p,'active')}disabled="disabled"{/if} />
        {if !permission($smarty.request.p,'active')}<input type="hidden" name="display" value="{$row->display}" />{/if}
      </td>
    </tr>
    {if $smarty.request.action == 'edit'}
    <tr>
      <td class="cella" valign="top">
        <span class="stdfont"><strong>Aktiv?</strong></span>
      </td>
      <td class="cellb">
        <input type="checkbox" name="active" value="1" {if $error == '' && $row->active == 1 || $error != '' && $smarty.request.active == 1}checked="checked"{/if} tabindex="5" {if !permission($smarty.request.p,'active')}disabled="disabled"{/if} />
        {if !permission($smarty.request.p,'active')}<input type="hidden" name="active" value="{$row->active}" />{/if}
      </td>
    </tr>
    {/if}
    <tr>
      <td class="cellb" colspan="2">
        <button type="submit" name="submit" value="Speichern" class="active" tabindex="7"><span class="tc_green">Speichern</span></button>
        <button type="button" name="button" value="Zur&uuml;ck zur &Uuml;bersicht" class="active m_left" tabindex="8" onclick="window.location.href='{$admindomain}index.php?p=pages&amp;action=overview'"><span class="tc_red">Zur&uuml;ck zur &Uuml;bersicht</span></button>
      </td>
    </tr></tbody>
  </table>
</form>
<script type="text/javascript" src="{$admindomain}ckeditor/ckeditor.js"></script>
<script type="text/javascript">
	CKEDITOR.replace('text');
</script>
{/strip}