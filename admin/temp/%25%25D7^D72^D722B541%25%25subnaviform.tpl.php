<?php /* Smarty version 2.6.24, created on 2014-08-19 21:47:20
         compiled from pages/subnaviform.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'subpages', 'pages/subnaviform.tpl', 10, false),)), $this); ?>
<?php $_from = $this->_tpl_vars['subnavi']->subnavi; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['subnavi'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['subnavi']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['subnavi']):
        $this->_foreach['subnavi']['iteration']++;
?>
<li>
  <input type="radio" name="menu" value="<?php echo $this->_tpl_vars['subnavi']->id; ?>
" id="radio_<?php echo $this->_tpl_vars['subnavi']->id; ?>
" <?php if ($this->_tpl_vars['error'] == '' && $this->_tpl_vars['row']->parent == $this->_tpl_vars['subnavi']->id || $this->_tpl_vars['error'] != '' && $this->_supers['request']['menu'] == $this->_tpl_vars['subnavi']->id): ?>checked="checked"<?php endif; ?><?php if ($this->_tpl_vars['subnavi']->id == $this->_supers['request']['id']): ?> disabled="disabled"<?php endif; ?> />
  <?php if ($this->_tpl_vars['subnavi']->id == $this->_supers['request']['id']): ?>
    <?php echo $this->_tpl_vars['subnavi']->title; ?>

  <?php else: ?>  
    <label for="radio_<?php echo $this->_tpl_vars['subnavi']->id; ?>
" class="pointer"><?php echo $this->_tpl_vars['subnavi']->title; ?>
</label>
  <?php endif; ?>
  <?php if ($this->_tpl_vars['subnavi']->subnavi): ?>
    <ul><?php echo subnavi_show(array('id' => $this->_tpl_vars['subnavi']->id), $this);?>
</ul>
  <?php endif; ?>
</li>
<?php endforeach; endif; unset($_from); ?>