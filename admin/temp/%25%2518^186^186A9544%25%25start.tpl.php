<?php /* Smarty version 2.6.24, created on 2014-08-22 14:52:57
         compiled from start.tpl */ ?>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
  <tbody><tr>
    <td>
      <h1 class="left">Startseite</h1>
      <p class="right p_right bold"><a href="javascript:;" onclick="if(confirm('Wirklich ausloggen?')){window.location.href='<?php echo $this->_tpl_vars['admindomain']; ?>
index.php?p=logout';}" class="red">Logout</a></p>  
    </td>
  </tr>
  <tr>
    <td>
      <?php if (permission ( 'news' )): ?>
      <div class="box235 h85 p_all m_all left bgc_grey rounded shadow">
        <p class="bold">Neuigkeiten</p>
        <?php if (permission ( 'news' , 'overview' )): ?><p class="b_bottom_dotted m_left"><a href="index.php?p=news&amp;action=overview" class="bold">&Uuml;bersicht</a></p><?php endif; ?>
        <?php if (permission ( 'news' , 'new' )): ?><p class="b_bottom_dotted m_left"><a href="index.php?p=news&amp;action=new" class="bold">Neuigkeit eintragen</a></p><?php endif; ?>
      </div>
      <?php endif; ?>
           
      <?php if (permission ( 'pages' )): ?> 
      <div class="box235 h85 p_all m_all left bgc_grey rounded shadow">
        <p class="bold">Inhalte</p>
        <?php if (permission ( 'pages' , 'overview' )): ?><p class="b_bottom_dotted m_left"><a href="index.php?p=pages&amp;action=overview" class="bold">&Uuml;bersicht</a></p><?php endif; ?>
        <?php if (permission ( 'pages' , 'new' )): ?><p class="b_bottom_dotted m_left"><a href="index.php?p=pages&amp;action=new" class="bold">neue Seite mit Inhalten erstellen</a></p><?php endif; ?>
      </div>
      <?php endif; ?>
      
      <?php if (permission ( 'glossary' )): ?> 
      <div class="box235 h85 p_all m_all left bgc_grey rounded shadow">
        <p class="bold">Glossareintr&auml;ge</p>
        <?php if (permission ( 'glossary' , 'overview' )): ?><p class="b_bottom_dotted m_left"><a href="index.php?p=glossary&amp;action=overview" class="bold">&Uuml;bersicht</a></p><?php endif; ?>
        <?php if (permission ( 'glossary' , 'new' )): ?><p class="b_bottom_dotted m_left"><a href="index.php?p=glossary&amp;action=new" class="bold">neuen Glossareintrag erstellen</a></p><?php endif; ?>
      </div>
      <?php endif; ?>
      
      <?php if (permission ( 'printmedia' )): ?> 
      <div class="box235 h85 p_all m_all left bgc_grey rounded shadow">
        <p class="bold">Presseeintr&auml;ge</p>
        <?php if (permission ( 'printmedia' , 'overview' )): ?><p class="b_bottom_dotted m_left"><a href="index.php?p=printmedia&amp;action=overview" class="bold">&Uuml;bersicht</a></p><?php endif; ?>
        <?php if (permission ( 'printmedia' , 'new' )): ?><p class="b_bottom_dotted m_left"><a href="index.php?p=printmedia&amp;action=new" class="bold">neuen Presseeintrag erstellen</a></p><?php endif; ?>
      </div>
      <?php endif; ?>
      
      <?php if (permission ( 'seminars' )): ?> 
      <div class="box235 h85 p_all m_all left bgc_grey rounded shadow">
        <p class="bold">Seminare</p>
        <?php if (permission ( 'seminars' , 'overview' )): ?><p class="b_bottom_dotted m_left"><a href="index.php?p=seminars&amp;action=overview" class="bold">&Uuml;bersicht</a></p><?php endif; ?>
        <?php if (permission ( 'seminars' , 'new' )): ?><p class="b_bottom_dotted m_left"><a href="index.php?p=seminars&amp;action=new" class="bold">neues Seminar eintragen</a></p><?php endif; ?>
      </div>
      <?php endif; ?>
      
      <?php if (permission ( 'user' )): ?> 
      <div class="box235 h85 p_all m_all left bgc_grey rounded shadow">
        <p class="bold">Benutzer &amp; Gruppen</p>
        <p class="b_bottom_dotted m_left"><a href="index.php?p=user&amp;action=overview" class="bold">Benutzer</a></p>
        <p class="b_bottom_dotted m_left"><a href="index.php?p=user&amp;action=groups" class="bold">Gruppen</a></p>
      </div>
      <?php else: ?>
      <div class="box235 h85 p_all m_all left bgc_grey rounded shadow">
        <p class="bold">eigene Profildaten</p>
        <p class="b_bottom_dotted m_left"><a href="index.php?p=own" class="bold">Einstellungen</a></p>
      </div>
      <?php endif; ?>
    
      <div class="clear"></div>
    </td>
  </tr></tbody>
</table>