<?php /* Smarty version 2.6.24, created on 2014-02-10 00:46:00
         compiled from pages/order.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'sslash', 'pages/order.tpl', 42, false),)), $this); ?>
<link type="text/css" rel="stylesheet" href="<?php echo $this->_tpl_vars['domainname']; ?>
<?php echo $this->_tpl_vars['css_path']; ?>
ui.theme.css" media="screen" />
<style>
	#sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
	#sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; }
	#sortable li span { position: absolute; margin-left: -1.3em; }
</style>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['domainname']; ?>
<?php echo $this->_tpl_vars['js_path']; ?>
jquery.ui/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['domainname']; ?>
<?php echo $this->_tpl_vars['js_path']; ?>
jquery.ui/jquery.ui.widget.min.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['domainname']; ?>
<?php echo $this->_tpl_vars['js_path']; ?>
jquery.ui/jquery.ui.mouse.min.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['domainname']; ?>
<?php echo $this->_tpl_vars['js_path']; ?>
jquery.ui/jquery.ui.sortable.min.js"></script>
<script type="text/javascript">
<?php echo '
$(function() { 
  $( "#sortable" ).sortable({
  	update: function(){
  	  var order = $(this).sortable(\'serialize\', {key: \'order[]\'});
  	  $.post(\''; ?>
<?php echo $this->_tpl_vars['admindomain']; ?>
<?php echo 'index.php?p=pages&action=updateOrder&\'+order, function(res){
  	  	if(res != 1) alert(res);
  	  	else {
  	  	  $(\'#showSaveSuccess\').show().fadeOut(1000);
  	  	}
  	  });
  	}  	
  });
  $( "#sortable" ).disableSelection();
});
'; ?>

</script>

<?php echo '<h1>Inhalte - Reihenfolge &auml;ndern</h1><div id="showSaveSuccess" class="m_all dpl_none"><p class="tc_green">Gespeichert...</p></div><ul id="sortable">'; ?><?php $_from = $this->_tpl_vars['pages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['page'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['page']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['page']):
        $this->_foreach['page']['iteration']++;
?><?php echo '<li class="ui-state-default box235" id="id_'; ?><?php echo $this->_tpl_vars['page']->id; ?><?php echo '"><span'; ?><?php if ($this->_tpl_vars['page']->active == 1): ?><?php echo ' class="bold"'; ?><?php endif; ?><?php echo '>'; ?><?php echo ((is_array($_tmp=$this->_tpl_vars['page']->title)) ? $this->_run_mod_handler('sslash', true, $_tmp) : stripslashes($_tmp)); ?><?php echo '</span></li>'; ?><?php endforeach; endif; unset($_from); ?><?php echo '</ul><button type="button" name="button" value="Zur&uuml;ck zur &Uuml;bersicht" class="active m_top" tabindex="8" onclick="window.location.href=\''; ?><?php echo $this->_tpl_vars['admindomain']; ?><?php echo 'index.php?p=pages&amp;action=overview\'"><span class="tc_red">Zur&uuml;ck zur &Uuml;bersicht</span></button>'; ?>