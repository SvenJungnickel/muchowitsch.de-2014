<?php /* Smarty version 2.6.24, created on 2013-08-09 15:47:53
         compiled from printmedia/upload.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'sslash', 'printmedia/upload.tpl', 58, false),)), $this); ?>
<style type="text/css">@import url(<?php echo $this->_tpl_vars['domainname']; ?>
swfupload/swfupload.css) all;</style>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['domainname']; ?>
swfupload/swfupload.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['domainname']; ?>
swfupload/handlers.js"></script>
<script type="text/javascript">
domainname = '<?php echo $this->_tpl_vars['domainname']; ?>
';
<?php echo '
  var swfu;
  
  window.onload = function () {
	swfu = new SWFUpload({
	  // Backend Settings
	  // Das Upload-Script muss vom System getrennt arbeiten, damit keine Probleme bei der Thumb-Erstellung auftreten
	  upload_url: "/swfupload/upload.php?id='; ?>
<?php echo $this->_supers['request']['id']; ?>
&uploaddir=<?php echo $this->_tpl_vars['uploaddir']; ?>
<?php echo '",	// Relative to the SWF file
	  post_params: {"PHPSESSID": "'; ?>
<?php echo $this->_tpl_vars['session_id']; ?>
<?php echo '"},

	  // File Upload Settings
	  file_size_limit : "2 MB",	// 2MB
	  file_types : "*.jpg; *.gif; *.png",
	  file_types_description : "*.jpg, *.gif, *.png",
	  file_upload_limit : 0,

	  // Event Handler Settings - these functions as defined in Handlers.js
	  //  The handlers are not part of SWFUpload but are part of my website and control how
	  //  my website reacts to the SWFUpload events.
	  file_queue_error_handler : fileQueueError,
	  file_dialog_complete_handler : fileDialogComplete,
	  upload_progress_handler : uploadProgress,
	  upload_error_handler : uploadError,
	  upload_success_handler : uploadSuccess,
	  upload_complete_handler : uploadComplete,

	  // Button Settings
	  button_image_url : domainname+"swfupload/images/SmallSpyGlassWithTransperancy_17x18.png",
	  button_placeholder_id : "spanButtonPlaceholder",
	  button_width: 180,
	  button_height: 18,
	  button_text : \'<span class="button">Bilder auswaehlen <span class="buttonSmall">(max 2 MB)</span></span>\',
	  button_text_style : \'.button { font-family: Helvetica, Arial, sans-serif; font-size: 12pt; } .buttonSmall { font-size: 10pt; }\',
	  button_text_top_padding: 0,
	  button_text_left_padding: 18,
	  button_window_mode: SWFUpload.WINDOW_MODE.TRANSPARENT,
	  button_cursor: SWFUpload.CURSOR.HAND,

	  // Flash Settings
	  flash_url : domainname+"swfupload/swfupload.swf",	// Relative to this file

	  custom_settings : {
		upload_target : "divFileProgressContainer"
	  },

	  // Debug Settings
	  debug: false
	});
  };
'; ?>

</script>
<?php echo '<h1>Bildergalerie zum Presseeintrag <i>'; ?><?php echo ((is_array($_tmp=$this->_tpl_vars['row']->title)) ? $this->_run_mod_handler('sslash', true, $_tmp) : stripslashes($_tmp)); ?><?php echo '</i> hochladen</h1>'; ?><?php echo '<div style="display: inline; border: solid 1px #7FAAFF; background-color: #C5D9FF; padding: 2px;"><span id="spanButtonPlaceholder"></span></div><div id="divFileProgressContainer" style="height: 75px;"></div><div id="thumbnails"></div>'; ?><?php echo '<form action="" method="post" name="kform" id="kform"><input type="hidden" name="send" value="1" /><button type="submit" name="submit" class="m_top active" value="Fertigstellen" tabindex="1"><span class="tc_green">Fertigstellen</span></button><button type="button" name="button" class="active m_top m_left" value="Abbrechen" onclick="if(confirm(\'Wirklich Abbrechen?\'))'; ?>{<?php echo 'window.location.href=\''; ?><?php echo $this->_tpl_vars['admindomain']; ?><?php echo 'index.php?p=printmedia&amp;action=overview&amp;pp='; ?><?php echo $this->_supers['request']['pp']; ?><?php echo '&amp;page='; ?><?php echo $this->_supers['request']['page']; ?><?php echo '&amp;sort='; ?><?php echo $this->_supers['request']['sort']; ?><?php echo '\''; ?>}<?php echo ';" tabindex="2" ><span class="tc_red">Abbrechen</span></button></form>'; ?>