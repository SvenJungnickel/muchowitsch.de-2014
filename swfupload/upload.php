<?
//====================================
// SWFUpload-Script für Bildergalerie 
//====================================

// BASEDIR definieren
define("BASEDIR", substr(dirname(__FILE__),0,-9),true);

// Config, Klassen und Funtionen einbinden
include_once(BASEDIR.'/config/config.php');
include_once(BASEDIR.'/class/Database.Class.php');
include_once(BASEDIR.'/functions/Func.ResizeImage.php');

// Datenbank-Prefix setzen
define("PREFIX", $db_prefix);

// Datenbank initialisieren
$db = DB::getInstance();
$db->setConnectionData($db_host, $db_user, $db_pass, $db_name);

if(isset($_REQUEST['id'])) {
	if (isset($_POST["PHPSESSID"])) {
		session_id($_POST["PHPSESSID"]);
	} else if (isset($_GET["PHPSESSID"])) {
		session_id($_GET["PHPSESSID"]);
	}
	session_start();
	
	if (!isset($_FILES["Filedata"]) || !is_uploaded_file($_FILES["Filedata"]["tmp_name"]) || $_FILES["Filedata"]["error"] != 0 || empty($_FILES['Filedata']['name'])) {
		echo "keine Dateien zum Hochladen angegeben";
		exit(0);
	} else {		
		$maxFileSize = 2097152;    	// Maximale Dateigröße des Bildes in Byte (hier: 2 MB)
				
		$uploaddir = $_REQUEST['uploaddir'];
		$sql_anz = $db->Query("SELECT count(id) as anz FROM ".PREFIX."_printmedia_pics WHERE pid = ".$_REQUEST['id']);
		$row_anz = $sql_anz->fetchrow();
		
		if(@filesize($_FILES['Filedata']['tmp_name']) > 0 && @filesize($_FILES['Filedata']['tmp_name']) <= $maxFileSize) {
			$end = strtolower(substr($_FILES['Filedata']['name'], -4));
			if($end == "jpeg") $end=".jpg";
			$number = $row_anz->anz+1;
			
			// Neuen Dateinamen erstellen
			if($number < 10) $number = '000'.$number;
			elseif($number > 9 && $number < 100) $number = '00'.$number;
			elseif($number > 99 && $number < 1000) $number = '0'.$number;
			if($end == 'jpeg') $end = '.jpeg';
			$file_name = $number.$end;
			$new_ori = BASEDIR.$printmediaUploadPath.$uploaddir.$file_name;
			$new_thumb = BASEDIR.$printmediaUploadPath.$uploaddir.'thumb/'.$file_name;
						
			// Originalbild Speichern
			$old_img = imagecreatefromjpeg($_FILES['Filedata']['tmp_name']);
			$imginfo = getimagesize($_FILES['Filedata']['tmp_name']);
			$width = $imginfo[0];
			$height = $imginfo[1];
			if($maxImageWidth > 0 && $width > $maxImageWidth) $new_width = $maxImageWidth;
			elseif($maxImageWidth > 0 && $width <= $maxImageWidth) $new_width = $width;
			else $new_width = round($maxImageHeight * $width / $height);
			if($maxImageHeight > 0 && $height > $maxImageHeight) $new_height = $maxImageHeight;
			elseif($maxImageHeight > 0 && $height <= $maxImageHeight) $new_height = $height;
			else $new_height = round($maxImageWidth * $height / $width);
			if($maxImageWidth > 0 && $new_width > $maxImageWidth || $maxImageHeight > 0 && $new_height > $maxImageHeight) {
				if($new_width > $maxImageWidth) {
					$new_width = $maxImageWidth;
					$new_height = round($maxImageWidth * $height / $width);
				}
				if($new_height > $maxImageHeight) {
					$new_height = $maxImageHeight;
					$new_width = round($maxImageHeight * $width / $height);
				}
			}
			
			$new_img = imagecreatetruecolor($new_width,$new_height);
			imagecopyresampled($new_img,$old_img,0,0,0,0,$new_width,$new_height,$width,$height);			
			imagejpeg($new_img,$new_ori,$imageQuali);
			imagedestroy($new_img);
			chmod($new_ori, 0777);
			
			// Thumb erstellen
			/*if($maxThumbWidth > 0 && $width > $maxThumbWidth) $thumb_w = $maxThumbWidth;
			elseif($maxThumbWidth > 0 && $width <= $maxThumbWidth) $thumb_w = $width;
			else $thumb_w = round($maxThumbHeight * $width / $height);
			if($maxThumbHeight > 0 && $height > $maxThumbHeight) $thumb_h = $maxThumbHeight;
			elseif($maxThumbHeight > 0 && $height <= $maxThumbHeight) $thumb_h = $height;
			else $new_height = round($maxThumbWidth * $height / $width);
			if($maxThumbWidth > 0 && $thumb_w > $maxThumbWidth || $maxThumbHeight > 0 && $thumb_h > $maxThumbHeight) {
				if($thumb_w > $maxThumbWidth) {
					$thumb_w = $maxThumbWidth;
					$thumb_h = round($maxThumbWidth * $height / $width);
				}
				if($thumb_h > $maxThumbHeight) {
					$thumb_h = $maxThumbHeight;
					$thumb_w = round($maxThumbHeight * $width / $height);
				}
			}*/
			/*if($height > $width) {
				$thumb_extract_h = $width / $thumb_w * $thumb_h;
				$start_h = ($height / 2) - ($thumb_extract_h / 2);
			}
			else $thumb_extract_h = $height;
			$dst_img = imagecreatetruecolor($thumb_w,$thumb_h);
			imagecopyresampled($dst_img,$old_img,0,0,0,$start_h,$thumb_w,$thumb_h,$width,$thumb_extract_h);
			imagejpeg($dst_img,$new_thumb,$thumbQuali);*/
			#chmod($new_thumb, 0777);
			
			// einen weiteren Thumb für Rückgabe an SFWUpload-Tool erzeugen
			/*ob_start();
			imagejpeg($dst_img);
			$thumb = ob_get_contents();
			ob_end_clean();
			imagedestroy($dst_img);*/
			$thumb = resize_img($file_name, BASEDIR.$printmediaUploadPath.$uploaddir, BASEDIR.$printmediaUploadPath.$uploaddir.'thumb/', $maxThumbWidth, $maxThumbHeight, $thumbQuali, 1);
						
			if(file_exists($new_ori) && file_exists($new_thumb)) {				
				// Datenbankeinträge erstellen
				$sql = $db->Query("INSERT INTO ".PREFIX."_printmedia_pics(pid,path,active)
										VALUES(".$_REQUEST['id'].",'".$uploaddir.$file_name."',1)");

				// Wenn Upload erfolgreich, dann irgendwas ausgeben, damit das SWFUpload-Flash es erkennt				
				if($sql->result) {				
					if (!isset($_SESSION["file_info"])) $_SESSION["file_info"] = array();				
					$file_id = md5($_FILES["Filedata"]["tmp_name"] + rand()*100000);						
					$_SESSION["file_info"][$file_id] = $thumb;
					echo "FILEID:".$file_id;
				}
			}
		}
	}
}
?>