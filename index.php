<?
/*
 * Hauptdatei zum Verwalten der Module und Templates
 *
 * by Sven Jungnickel
 */

// Anfangszeitpunkt festlegen
$Anfangszeit = microtime(true);

// Komprimierung starten
if(!ob_start("ob_gzhandler")) ob_start();

// Session starten
session_start();

// BASEDIR definieren
define("BASEDIR", dirname(__FILE__));

// Sprache auf Deutsch stellen
setlocale(LC_ALL,'de_DE');

// Config, Klassen und Funtionen einbinden
include_once(BASEDIR.'/config/config.php');
include_once(BASEDIR.'/class/Smarty.class.php');
include_once(BASEDIR.'/class/Database.Class.php');
include_once(BASEDIR.'/functions/Func.Init.php');

// Datenbank-Prefix setzen
define("PREFIX", $db_prefix);

// Datenbank initialisieren
$db = DB::getInstance();
$db->setConnectionData($db_host, $db_user, $db_pass, $db_name);

// Template-Engine initialisieren
$tmpl = new Smarty;
$tmpl->compile_dir = BASEDIR.'/temp';

// Caching Einstellungen
$tmpl->compile_check = false;
$tmpl->force_compile = true;
$tmpl->caching = 0;#2
$tmpl->cache_dir = BASEDIR.'/temp';
$tmpl->cache_lifetime = 100;
$tmpl->cache_modified_check = true;

// Smarty-Block "dynamic" generieren für nichtcachbare Templateblöcke
function smarty_block_dynamic($param, $content, &$smarty) {
    return $content;
}
$tmpl->register_block('dynamic', 'smarty_block_dynamic', false);

// Funktionen für Smarty registrieren
$tmpl->register_function("in_array", "in_array");
$tmpl->register_function("stripslashes", "stripslashes");
$tmpl->register_function("nl2br", "nl2br");
$tmpl->register_modifier("sslash", "stripslashes");
$tmpl->register_function("navigation", "navigation");
$tmpl->register_function("subnavi", "subnavi_show");
$tmpl->register_function("showGlossary", "showGlossary");
$tmpl->register_function("showPrintMedia", "showPrintMedia");
$tmpl->register_function("showSeminars", "showSeminars");
$tmpl->register_function("showNextSeminars", "showNextSeminars");

// globale Variablen initialisieren
$tmpl->assign('img_path', $img_path);
$tmpl->assign('js_path', $js_path);
$tmpl->assign('css_path', $css_path);
$tmpl->assign('upload_path', $upload_path);
$tmpl->assign('gallery_path', $galleryUploadPath);

// Domain-Adresse definieren
$tmpl->assign('domainname', $domainname);

// Browsercache benutzen
/*$LAST_CHANGE_SERVERSIDE = time()-3600;
#echo '#'.$_SERVER['HTTP_IF_MODIFIED_SINCE'].'#';
if(isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && $LAST_CHANGE_SERVERSIDE <= strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
	echo '<span class="tc_lgrey">##jo##</span>';
	header("HTTP/1.1 304 Not Modified");
	exit;
}*/

// Startframe festlegen
if(!isset($_REQUEST['p']) || $_REQUEST['p'] == '' || $_REQUEST['p'] == 'index') $_REQUEST['p'] = 'start';
#if(!isset($_REQUEST['p']) || $_REQUEST['p'] == '' || $_REQUEST['p'] == 'index') $template = 'frame.tpl';
#else {
	
	// Test-Template initialisieren
	/*if(isset($_REQUEST['test']) && $_REQUEST['test'] == 1) {
		$template = 'main2.tpl';		
	}
	else $template = 'main.tpl';*/
	$template = 'main2.tpl';
	
	// Module einbinden
	if(file_exists(BASEDIR.'/system/'.$_REQUEST['p'].'.php')) include_once(BASEDIR.'/system/'.$_REQUEST['p'].'.php');
	else include_once(BASEDIR.'/system/error.php');
#}

// Header Daten initialisieren
$tmpl->assign('title', $title);
$tmpl->assign('description', $description);
$tmpl->assign('keywords', $keywords);

// Template in Variable schreiben
$prepage = $tmpl->fetch($template);


// Benutzerdaten Anhand der Cookies ermitteln
/*if(isset($_COOKIE['uid']) && $_COOKIE['uid'] != '' && $_COOKIE['uid'] != 0 && isset($_COOKIE['pass']) && $_COOKIE['pass'] != '' && $_COOKIE['pass'] != 0) {
	// Überprüfen der Benutzerdaten
	$sql = mysql_query("SELECT id, uname, ugroup FROM ".PREFIX."_admin_user WHERE id = ".escs($_COOKIE['uid'])." AND password = '".escs($_COOKIE['pass'])."' AND active = 1");
	
	if(mysql_num_rows($sql) > 0) {
		$row = mysql_fetch_assoc($sql);
		$_SESSION['uid'] = $row['id'];
		$_SESSION['ugroup'] = $row['ugroup'];
	}
}*/

// Statistik für eingeloggte Admins ausgeben
#if(isset($_SESSION['uid']) && $_SESSION['uid'] != '' && $_SESSION['uid'] != 0 && $_SESSION['ugroup'] == 1) {
	
	/*$prepage = substr($prepage,0,strlen($prepage)-14);
	$dbstats = $db->getStatistic();
	$prepage.= "<br /><center><p>Query-Zeit: <b>".number_format($dbstats['total_query_time'], 4, ",", ".")."</b>";
	$prepage.= " SQL-Abfragen: <b>".$dbstats['total_number_of_queries']."</b> ";
	$Endzeit = microtime(true);
	$rendertime = number_format($Endzeit-$Anfangszeit, 4, ",", ".");
	$prepage.= "Renderzeit: <b>".$rendertime."</b></p></center>";*/
#}
// Datenbank schließen
$db->close();

// ModRewrite anwenden
include_once(BASEDIR.'/class/ModRewrite.class.php');

// Template ausgeben
#if(isset($_REQUEST['test']) && $_REQUEST['test'] == 1) echo utf8_encode($prepage);
#else echo $prepage;
echo utf8_encode($prepage);
ob_end_flush();
?>