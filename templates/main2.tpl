{strip}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>{dynamic}{if $title}{$title} - {/if}{/dynamic}Andreas Muchowitsch Unternehmensberatung GmbH</title>
  <meta http-equiv="pragma" content="no-cache" />
  <meta http-equiv="imagetoolbar" content="no" />
  <meta name="robots" content="INDEX,FOLLOW" />
  <meta http-equiv="language" content="DE" />
  <meta http-equiv="expires" content="0" />
  <meta name="publisher" content="Andreas Muchowitsch Unternehmensberatung GmbH 2010" />
  <meta name="copyright" content="&copy; Andreas Muchowitsch Unternehmensberatung GmbH 2010 - {$smarty.now|date_format:"%Y"}" />
  <meta name="description" content="{if $row->metadescription}{$row->metadescription|htmlspecialchars}{elseif $description != ''}{$description}{else}Andreas Muchowitsch Unternehmensberatung GmbH - Ihr Ansprechpartner zum Thema Unternehmensberatung, Existenzgr&uuml;ndung und Nebenberufliche Selbstst&auml;ndigkeit{/if}" />
  <meta name="keywords" content="{if $row->keywords}{$row->keywords}{elseif $keywords != ''}{$keywords}{else}Unternehmensberatung, Existenzgr&uuml;ndung, Existenzgr&uuml;ndungerseminar, Nebenberufliche Selbstst&auml;ndigkeit, Businessplan, Buchf&uuml;hrung, Unternehmensf&uuml;hrung{/if}" />
  <meta name="abstract" content="www.muchowitsch.de" />
  <link rel="shortcut icon" type="image/x-icon" href="{$domainname}{$img_path}favicon.ico" />
  <link rel="stylesheet" type="text/css" href="{$domainname}templates/css/style2.css" />
  <!--[if lt IE 8]><link rel="stylesheet" type="text/css" href="{$domainname}templates/css/ie.css" /><![endif]-->
  <script type="text/javascript" src="{$domainname}{$js_path}jquery-1.7.2.min.js"></script>
  <script type="text/javascript" src="{$domainname}{$js_path}jquery.scrollTo-min.js"></script>
  <script type="text/javascript" src="{$domainname}{$js_path}jquery.localscroll-min.js"></script>
    {* Google ReCaptcha *}
    <script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=de"></script>
</head>
<body>
	<div id="main" itemscope itemtype="http://schema.org/Organization">

     	<noscript>
        	<p class="ta_center tc_red">Bitte aktivieren Sie Javascript, um alle Funktionen nutzen zu k&ouml;nnen!</p>
        </noscript>

    	<div id="bar">&nbsp;</div>

     	<div id="navi">
            <div class="m_top_20 m_left_20 m_bottom_20 m_right_20 left">
                <form action="{$domainname}index.php?p=search" method="post" id="searchform">
                    <input type="text" name="search" id="search" value="{if $searchword}{$searchword}{else}Suchbegriff eingeben{/if}" onclick="if(this.value == 'Suchbegriff eingeben'){ldelim}this.value = ''{rdelim}" onfocus="if(this.value == 'Suchbegriff eingeben'){ldelim}this.value = ''{rdelim}" onselect="if(this.value == 'Suchbegriff eingeben'){ldelim}this.value = ''{rdelim}" onblur="if(this.value == ''){ldelim}this.value = 'Suchbegriff eingeben'{rdelim}" />
                    <img src="{$domainname}{$img_path}icons/magnifier.png" alt="suchen" class="m_left_5 pointer" onclick="$('#searchform').submit();" />
                </form>
            </div>

            <a href="{$domainname}index.php?p=contact" class="left m_left_20 m_top_20" style="text-decoration: none;">&raquo; Kontakt</a>

            <div class="clear"></div>
  			{navigation}
        </div>

        <div id="mainbox">
            <div class="right m_right_20 m_top">
                <a href="{$domainname}"><img src="{$domainname}/templates/images/muchowitsch-logo.png" alt="Andreas Muchowitsch Unternehmensberatung GmbH" itemprop="logo" /></a>
            </div>
            <div class="clear"></div>

            <div id="content">
                {dynamic}{$content}{/dynamic}
            </div>
  		</div>

        <div class="clear"></div>

        <div id="feet">
        	<div class="right" style="margin-right: 40px;">
                <a id="scroll" class="pointer">Zum Seitenanfang</a>
            </div>

            <div class="ta_center box450 m_auto">
                &copy; Andreas Muchowitsch Unternehmensberatung GmbH 2010 - {$smarty.now|date_format:"%Y"}<br />
                <a href="{$domainname}" style="color: #075; text-decoration: none;">www.muchowitsch.de</a>
			</div>
        </div>

	</div>

    {literal}
    <script type="text/javascript">
        var _gaq = _gaq || [];
        var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
        _gaq.push(['_require', 'inpage_linkid', pluginUrl]);
        _gaq.push(['_setAccount', 'UA-20613982-1']);
        _gaq.push (['_gat._anonymizeIp']);
        _gaq.push(['_trackPageview']);
        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

        $(document).ready(function() {
            $('#bar').height($('#main').height());
            $('#scroll').click(function(){
                $.scrollTo(0,800);
            });

            $.localScroll({ hash:true });

            $('#searchform').submit(function(e) {
                if($('#search').val() == '' || $('#search').val() == 'Suchbegriff eingeben') {
                    e.preventDefault();
                }
            });
        });
    </script>
    {/literal}
</body>
</html>
{/strip}