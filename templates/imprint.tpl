{strip}
<h1>IMPRESSUM</h1>

<p>gem&auml;&szlig; &sect; 5 Telemediengesetz (TMG)</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p class="bold" itemprop="name">Andreas Muchowitsch Unternehmensberatung GmbH</p>
<p>&nbsp;</p>
<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
    <table width="600">
            <tr>
                <td width="50%">Sitz und Anschrift der Gesellschaft:</td>
                <span itemprop="addressCountry" class="dpl_none">DE</span>
                <td width="50%">D-<span itemprop="postalCode">01069</span> <span itemprop="addressLocality">Dresden</span> &middot; <span itemprop="streetAddress">Werdauer Stra&szlig;e 1-3</span></td>
            </tr>
        <tr>
            <td>Postanschrift:</td>
            <td>D-01014 Dresden &middot; Postfach 32 02 55</td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td>Telefon:</td>
            <td><span  itemprop="telephone">+49 (0) 3 51 / 4 82 46 29</span></td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td>E-Mail:</td>
            <td>kontakt [at] muchowitsch [dot] de</td>
        </tr>
        <tr>
            <td>Internet:</td>
            <td><a href="{$domainname}" itemprop="url">www.muchowitsch.de</a></td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td>Register:</td>
            <td>Amtsgericht Dresden &middot; HR B 25206</td>
        </tr>
        <tr>
            <td>Gesch&auml;ftsf&uuml;hrer:</td>
            <td>Andreas Muchowitsch</td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td>Verantwortlich gem&auml;&szlig; &sect; 55 RStV:</td>
            <td>Andreas Muchowitsch</td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td>USt-IdNr.:</td>
            <td>DE242535897</td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td>Zweigstelle Meppen</td>
            <td>D-49716 Meppen &middot; Haseld&uuml;nner Stra&szlig;e 4 a</td>
        </tr>
        <tr>
            <td>Telefon</td>
            <td>+49 (0) 59 31 / 5 90 20 41</td>
        </tr>
    </table>

    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p class="bold">Layout, Technische Umsetzung und Administration</p>
    <p>&nbsp;</p>
    <p>Andreas Muchowitsch Unternehmensberatung GmbH</p>
    <p>Layout und technische Umsetzung: Sven Jungnickel</p>
    <p>&nbsp;</p>
    <table width="600">
        <tr>
            <td width="50%">Administration:</td>
            <td width="50%">admin [at] muchowitsch [dot] de</td>
        </tr>
    </table>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p class="bold">Haftungsausschluss / Urheberschutz</p>
    <p>&nbsp;</p>
    <p>Die Andreas Muchowitsch Unternehmensberatung GmbH &uuml;bernimmt keine Haftung f&uuml;r die Inhalte fremder Internetseiten, auf die direkt oder indirekt verwiesen wird (Links). F&uuml;r die Inhalte der verlinkten Seiten sind ausschlie&szlig;lich deren Betreiber verantwortlich. Wir erkl&auml;ren, dass zum Zeitpunkt der Linksetzung die jeweilige verlinkte Seite offensichtlich frei von rechtswidrigen und illegalen Inhalten war. Auf aktuelle und zuk&uuml;nftige Gestaltung, Inhalte oder Urheberschaft der verlinkten/verkn&uuml;pften Seiten haben wir keinen Einfluss. Deshalb distanzieren wir uns hierdurch ausdr&uuml;cklich von allen Inhalten aller verlinkten/verkn&uuml;pften Seiten, die nach der Linksetzung ver&auml;ndert wurden/werden. Diese Distanzierung gilt auch f&uuml;r alle Links (und den Inhalten zu den gelinkten Seiten) in Fremdeintr&auml;gen innerhalb unseres Internetangebotes eingerichteter Diskussionsforen, Mailinglisten und &auml;hnlichen Angeboten. F&uuml;r fehlerhafte, unvollst&auml;ndige oder illegale Inhalte und insbesondere f&uuml;r Sch&auml;den, die aus der Nutzung oder Nichtnutzung derart dargebotener Informationen entstehen, haftet allein der Anbieter der Seite, auf welche verwiesen wurde, nicht derjenige, der &uuml;ber Links lediglich auf die jeweilige Ver&ouml;ffentlichung verweist.</p>
    <p>&nbsp;</p>
    <p>Die Inhalte der Internetpr&auml;senz www.muchowitsch.de wurden nach bestem Wissen und Gewissen erstellt. Sie werden in Abst&auml;nden &uuml;berpr&uuml;ft und aktualisiert. Es kann jedoch auch bei notwendiger Sorgfalt nicht ausgeschlossen werden, dass sich Informationen und Daten zwischenzeitlich ge&auml;ndert haben. Die Andreas Muchowitsch Unternehmensberatung GmbH und jeweilige Autoren &uuml;bernehmen keine Garantie und keine Haftung f&uuml;r die Richtigkeit, Aktualit&auml;t, Korrektheit und Vollst&auml;ndigkeit der Inhalte und Informationen dieser Website. &Auml;nderungen, Erg&auml;nzungen und L&ouml;schungen von Inhalten sowie die teilweise oder vollst&auml;ndige Einstellung der Website k&ouml;nnen durch die Andreas Muchowitsch Unternehmensberatung jederzeit und ohne vorherige Ank&uuml;ndigung vorgenommen werden. Die Informationen, Empfehlungen, Hinweise und Angebote auf der Website sind unverbindlich. Haftungsanspr&uuml;che gegen die Andreas Muchowitsch Unternehmensberatung GmbH oder Autoren, welche sich auf Sch&auml;den materieller oder ideeller Art beziehen, die durch die Nutzung oder Nichtnutzung der dargebotenen Informationen bzw. durch die Nutzung fehlerhafter und unvollst&auml;ndiger Informationen verursacht wurden sind grunds&auml;tzlich ausgeschlossen, sofern seitens des Autors kein nachweislich grob fahrl&auml;ssiges oder vors&auml;tzliches Verschulden vorliegt.</p>
    <p>&nbsp;</p>
    <p>Die Internetpr&auml;senz enth&auml;lt partiell namentlich oder anonym gekennzeichnete Artikel, Beitr&auml;ge, Meinungen etc. Dritter. Diese geben nicht unbedingt die Meinung der Andreas Muchowitsch Unternehmensberatung GmbH wieder.</p>
    <p>&nbsp;</p>
    <p>Struktur und Inhalte der Website muchowitsch.de sind urheberrechtlich gesch&uuml;tzt und unterliegen deutschem Urheberrecht. Die Vervielf&auml;ltigung von Informationen oder Daten, insbesondere die Verwendung von Texten, Textteilen, Grafiken, Bildmaterial, Ton-, Video-, Film- und Animationsdateien bedarf der vorherigen ausdr&uuml;cklichen Zustimmung der Andreas Muchowitsch Unternehmensberatung GmbH. Ausgenommen sind zum Download bereitgestellte Dokumente.</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p class="bold">Hinweis gem&auml;&szlig; Verbraucherstreitbeilegungsgesetz (VSBG)</p>
    <p>&nbsp;</p>
    <p>Die Andreas Muchowitsch Unternehmensberatung GmbH ist nicht bereit und nicht verpflichtet, an Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle teilzunehmen. </p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p class="bold">Datenschutz</p>
    <p>&nbsp;</p>
    <p>Soweit auf dieser Website die M&ouml;glichkeit zur Eingabe pers&ouml;nlicher oder gesch&auml;ftlicher Daten (Name, Vorname, Firma, Anschrift, E-Mail-Adresse etc.) oder der &Uuml;bermittlung von Dokumenten etc. besteht, so erfolgt die Heraus- und Eingabe dieser Daten, Dokumente etc. seitens des Users/Nutzers ausdr&uuml;cklich auf freiwilliger Basis. Wir weisen in dem Zusammenhang darauf hin, dass der Schutz der in das Internet eingestellten und &uuml;bertragenen Daten, Dokumente und Informationen vor Zugriffen Dritter nie vollkommen gew&auml;hrleistet werden kann und kaum den Bestimmungen des Artikels 10 GG gleichzusetzen ist. Wir empfehlen daher, f&uuml;r die &Uuml;bermittlung sensibler Daten/Dokumente den Postweg oder das pers&ouml;nliche Gespr&auml;ch zu w&auml;hlen.</p>
    <p>&nbsp;</p>
    <p>Bei der Erhebung von Daten, welche uns grunds&auml;tzlich freiwillig zur Verf&uuml;gung gestellt werden, verfolgen wir das Prinzip der Datensparsamkeit. Die Daten werden ausschlie&szlig;lich f&uuml;r die jeweiligen Zwecke (z.B. Kontaktaufnahme &uuml;ber das Kontaktformular, Be- und Abbestellung des Newsletters) verwendet. Eine Weitergabe der Daten an Dritte erfolgt nicht. Eine Ausnahme bildet die ausdr&uuml;ckliche Einwilligung in eine Weitergabe an Dritte.</p>
    <p>&nbsp;</p>
    <p>In Verbindung mit der Nutzung unserer Website werden kurzfristig auf unseren Servern Daten f&uuml;r Sicherungszwecke gespeichert, die m&ouml;glicherweise eine Identifizierung zulassen k&ouml;nnten (zum Beispiel IP-Adresse, Datum, Uhrzeit, betrachtete Seiten). Die IP-Adresse wird anschlie&szlig;end anonymisiert. Sie wird nicht zur Identifizierung von Nutzern verwendet. Es werden keine Nutzerprofile erstellt.</p>
    <p>&nbsp;</p>
    <p>Die im Impressum ver&ouml;ffentlichten Kontaktdaten (Anschrift, Telefonnummer, E-Mail-Adressen) sind nicht in der Form zu verstehen, diese rechtswidrig oder f&uuml;r nicht ausdr&uuml;cklich angeforderte Informationen, Angebote etc. per Anruf, Brief oder Spam-Mails verwenden zu d&uuml;rfen. Die Einleitung juristischer Schritte bei derartigen Verst&ouml;&szlig;en behalten wir uns vor.</p>
    <p>&nbsp;</p>
    <p>Diese Website verwendet Google Analytics, einen Webanalysedienst der Google Inc. (Google). Google Analytics verwendet sog. „Cookies“, Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie erm&ouml;glichen. Die durch den Cookie erzeugten Informationen &uuml;ber Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA &uuml;bertragen und dort gespeichert. Im Falle der Aktivierung der IP-Anonymisierung auf dieser Webseite, wird Ihre IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europ&auml;ischen Union oder in anderen Vertragsstaaten des Abkommens &uuml;ber den Europ&auml;ischen Wirtschaftsraum zuvor gek&uuml;rzt. Nur in Ausnahmef&auml;llen wird die volle IP-Adresse an einen Server von Google in den USA &uuml;bertragen und dort gek&uuml;rzt.</p>
    <p>&nbsp;</p>
    <p>Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports &uuml;ber die Websiteaktivit&auml;ten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegen&uuml;ber dem Websitebetreiber zu erbringen. Die im Rahmen von Google Analytics von Ihrem Browser &uuml;bermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengef&uuml;hrt.</p>
    <p>&nbsp;</p>
    <p>Sie k&ouml;nnen die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht s&auml;mtliche Funktionen dieser Website vollumf&auml;nglich werden nutzen k&ouml;nnen.</p>
    <p>&nbsp;</p>
    <p>Sie k&ouml;nnen dar&uuml;ber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem sie das unter dem folgenden Link verf&uuml;gbare Browser-Plugin herunterladen und installieren: <a href="http://tools.google.com/dlpage/gaoptout?hl=de" target="_blank">http://tools.google.com/dlpage/gaoptout?hl=de</a></p>
    <p>&nbsp;</p>
    <p>Auf der Website befinden sich Button, die eine Verlinkung zu Google+, Twitter, Xing und LinkedIn herstellen. Diese Verlinkungen f&uuml;hren ausschlie&szlig;lich zu den jeweiligen Accounts der Andreas Muchowitsch Unternehmensberatung GmbH. Es werden hierf&uuml;r keine Cookies verwendet und keine Daten der Nutzer ausgelesen.</p>
    <p>&nbsp;</p>
    <p>Die Andreas Muchowitsch Unternehmensberatung beh&auml;lt sich das Recht vor, jederzeit die Datenschutzhinweise zu &auml;ndern. Die aktuelle Fassung befindet sich auf der Website. </p>
</div>
{/strip}