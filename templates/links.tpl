{strip}
<div class="m_bottom_15">
	<span>Links</span>
</div>

<h1>Links</h1>

<p><a href="#01">Ministerien / Behörden</a></p>
<p><a href="#02">Gesetze</a></p>
<p><a href="#03">Steuern</a></p>
<p><a href="#04">Existenzgründerportale</a></p>
<p><a href="#05">Technologiegründungen / Schutzrechte</a></p>
<p><a href="#06">Franchise</a></p>
<p><a href="#07">Unternehmensnachfolge</a></p>
<p><a href="#08">Förder- / Kreditinstitute</a></p>
<p><a href="#09">Beratungen</a></p>
<p><a href="#10">Versicherungen</a></p>
<p><a href="#11">Statistik</a></p>
<p>&nbsp;</p>

<a name="01"></a>
<p class="bold">Ministerien / Behörden</p>
<p>&nbsp;</p>

<table cellspacing="5" width="100%">
	<tr>
    	<th>Domain</th>
        <th>Betreiber</th>
    	<th>Wesentliche Inhalte</th>
	</tr>
    <tr>
    	<td valign="top" width="33%"><a href="http://www.bmwi.de" target="_blank">www.bmwi.de</a></td>
        <td valign="top" width="33%">Bundesministerium für Wirtschaft und Technologie (BMWi)</td>
		<td valign="top" width="33%">Wirtschaft, Technologie, Mittelstand in Deutschland</td>
    </tr>
    <tr>
    	<td valign="top"><a href="http://www.existenzgruender.de" target="_blank">www.existenzgruender.de</a></td>
        <td valign="top">Bundesministerium für Wirtschaft und Technologie (BMWi)</td>
		<td valign="top">Existenzgründung Hinweise, Arbeitshilfen, Publikationen</td>
    </tr>
    <tr>
    	<td valign="top"><a href="http://www.bafa.de" target="_blank">www.bafa.de</a></td>
        <td valign="top">Bundesamt für Wirtschaft und Ausführkontrolle (BAFA)</td>
		<td valign="top">Förderprogramme unter anderem zu Unternehmensberatungen</td>
    </tr>
    <tr>
    	<td valign="top"><a href="http://www.bundesfinanzministerium.de" target="_blank">www.bundesfinanzministerium.de</a></td>
        <td valign="top">Bundesministerium der Finanzen (BMF)</td>
		<td valign="top">Aktuelle Steuerpolitik, Steuergesetze, BMF-Schreiben Abschreibungstabellen</td>
    </tr>
    <tr>
    	<td valign="top"><a href="http://www.finanzamt.de" target="_blank">www.finanzamt.de</a></td>
        <td valign="top">Bundeszentralamt für Steuern (BZSt)</td>
		<td valign="top">Vergabe der USt-IdNr., Umsatzsteuervergütung, Bauabzugssteuer</td>
    </tr>
    <tr>
    	<td valign="top"><a href="http://www.formulare-bfinv.de" target="_blank">http://www.formulare-bfinv.de</a></td>
        <td valign="top">Bundesministerium der Finanzen (BMF)</td>
		<td valign="top">Online-Dienstleistungen und interaktive Formulare der Bundesfinanzverwaltung</td>
    </tr>
    <tr>
    	<td valign="top"><a href="http://www.elster.de" target="_blank">www.elster.de</a></td>
        <td valign="top">Bayerisches Landesamt für Steuern</td>
		<td valign="top">Online-(Elektronische) Steuererklärungen und -Meldungen</td>
    </tr>
</table>
<p>&nbsp;</p>

<a name="02"></a>
<p class="bold">Gesetze</p>
<p>&nbsp;</p>

<table cellspacing="5" width="100%">
	<tr>
    	<th width="33%">Domain</th>
        <th width="33%">Betreiber</th>
    	<th width="33%">Wesentliche Inhalte</th>
	</tr>
    <tr>
    	<td valign="top"><a href="http://www.bundesjustizministerium.de" target="_blank">www.bundesjustizministerium.de</a></td>
        <td valign="top"></td>
		<td valign="top"></td>
    </tr>
    <tr>
    	<td valign="top"><a href="http://www.gesetze-im-internet.de" target="_blank">www.gesetze-im-internet.de</a></td>
        <td valign="top"></td>
		<td valign="top"></td>
    </tr>
</table>
{/strip}