{strip}
{if $text}
	{$text|sslash}
{else}
	<h1>WILLKOMMEN BEI MUCHOWITSCH.DE</h1>
    <p>Die Andreas Muchowitsch Unternehmensberatung GmbH pr&auml;sentiert sich mit einem inhaltlich neuen Webauftritt. Zeitgleich haben wir unser Unternehmen und die Internetpr&auml;senz mit einem neuen Layout bedacht. </p>
    <p>&nbsp;</p>
    <p>Dem Bed&uuml;rfnis nach Informationen, News, konkreter Hilfestellung und insbesondere dem interaktiven Austausch wird nunmehr vermehrt Rechnung getragen.</p>
    <p>&nbsp;</p>
    <p>Unsere Angebote rund um die Themen Gr&uuml;ndung, Unternehmen und Wirtschaft werden in den n&auml;chsten Wochen f&uuml;r Sie weiter ausgebaut. Anregungen, W&uuml;nsche und Hinweise sind jederzeit herzlich willkommen.  </p>
    <p>&nbsp;</p>
    <p>Wir w&uuml;nschen Ihnen einen angenehmen, informativen und unterhaltsamen Aufenthalt auf unserer neuen Online - Pr&auml;senz.</p>
{/if}

<p>&nbsp;</p>

<div class="m_top_20 shadow rounded b_all bc_lgrey">
  <div class="shadow_bottom b_bottom bc_lgrey p_all_10">
    <p class="bold m_left">Aktuelles</p>
  </div>
  <div class="bc_lgrey rounded_bottom">
  {foreach from=$news item=n name=n}
    <div class="p_all_10 m_left m_right {if !$smarty.foreach.n.last} b_bottom_dotted{/if}">
	  <p>{$n->topic|sslash}</p>
      <p>{$n->ctime|date_format:"%m/%Y"} - {$n->author}</p>

      <a href="{$domainname}index.php?p=archive&amp;action=details&amp;aid={$n->id}" style="text-decoration: none; color: #075;" title="{$n->title|sslash|escape}">
          <h2>{$n->title|sslash|upper}</h2>
      </a>
      
      <p>{$n->teaser|sslash}</p>
      <p><a href="{$domainname}index.php?p=archive&amp;action=details&amp;aid={$n->id}" title="mehr zu {$n->title|sslash}">mehr</a>...</p>
      <p>&nbsp;</p>
    </div>
  {/foreach}
  </div>
</div>
{/strip}