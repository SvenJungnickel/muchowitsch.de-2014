{strip}
<h1 class="p_left_10">Fehler - Seite nicht gefunden!</h1>
<p class="p_all_10 bold">Diese Seite existiert nicht mehr! Entweder wurde sie entfernt oder Sie haben einen fehlerhaften Link benutzt.</p>
<a href="{$domainname}" class="p_all_10">zur Startseite</a>
{/strip}