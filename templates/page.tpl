{strip}
{if $path}
  <div class="m_bottom_15">
  {foreach from=$path item=navipath name=navipath}
    {if $navipath.id != ''}
    	 <a href="{$domainname}index.php?p=page&amp;id={$navipath.id}">{$navipath.title}</a>&nbsp;&raquo;&nbsp;
    {else}
    	<span>{$navipath.title}</span>
    {/if}    
  {/foreach}
  </div>
{/if}

<h1>{$row->title|sslash|upper}</h1>

{if $row->content != ''}
	{eval var=$row->content}
{else}
	{include file="error.tpl"}
{/if}
{/strip}
<link rel="stylesheet" href="{$domainname}templates/js/jquery.cluetip/jquery.cluetip.css" type="text/css" />
<script src="{$domainname}templates/js/jquery.cluetip/lib/jquery.hoverIntent.js" type="text/javascript"></script>
<script src="{$domainname}templates/js/jquery.cluetip/jquery.cluetip.js" type="text/javascript"></script>
{literal}
<script type="text/javascript">
$(document).ready(function() {
  //$('a.glossary').cluetip({splitTitle: '|'});
  $('a.glossary').cluetip({
	splitTitle: '|',
	cluetipClass: 'muchowitsch',
	clickThrough: true
  });
});
</script>
{/literal}