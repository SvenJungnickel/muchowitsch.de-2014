{strip}
{foreach from=$alphabet item=item}
  {if $smarty.request.sort == $item || $smarty.request.sort == '' && $item == 'Alle'}
    <span class="m_right_5 fs_16 tc_green">{$item}</span>
  {else}
    <a href="{$domainname}index.php?p=page&amp;id={$smarty.request.id}&amp;sort={$item}" class="m_right_5 bold">{$item}</a>
  {/if}
{/foreach}

<div class="m_top_20 m_right_20 shadow rounded b_all bc_lgrey">
{foreach from=$glossary item=g name=g}
  {if $g->firstletter == 1}
    <div class="shadow_bottom b_bottom {if !$smarty.foreach.g.first} b_all rounded_top m_top_20{/if} bc_lgrey p_top_10 p_left_10">
      <h2 class="m_left">{$g->title|truncate:1:""|upper}</h2>
    </div> 
  {/if}
  <div class="p_all_10 m_left m_right">{*{if !$smarty.foreach.g.last} b_bottom_dotted{/if}*}
	<p class="bold">{$g->title|sslash}</p>
    <p>{$g->descr|nl2br}</p>
  </div>
{foreachelse}
  <h2 class="m_top p_left_10 m_left">Keine Eintr&auml;ge vorhanden</h2>
{/foreach}
</div>
{if $navi}<div class="m_top_20 m_left_10m">{$navi}</div>{/if}
{/strip}