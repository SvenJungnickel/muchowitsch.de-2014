{strip}
<div>
  <p class="bold">Bundesland:</p>
  {foreach from=$states item=s}
    <p><a {*href="#state{$s->id}"*} href="javascript:scrollTo({$s->id});">{$s->title|sslash|escape:"html"}</a></p>
  {/foreach}
  <p>&nbsp;</p>
  
  {foreach from=$states item=s}
    <p class="bold fs_16 m_top_20 m_bottom" id="state{$s->id}">Bundesland {$s->title|sslash|escape:"html"}</p>
    {if $s->num_seminars == 0}
      <p>Zurzeit liegen keine Seminartermine vor.</p>
    {else}
      {foreach from=$s->cities item=c name=c}
        <h2{if $smarty.foreach.c.index > 0} class="m_top"{/if}>{$c->title|sslash|escape:"html"}</h2>
        
        <div class="m_right_20 shadow rounded b_all bc_lgrey">
		{foreach from=$c->seminars item=se name=se}
		  <table cellpadding="0" cellspacing="0" border="0" width="100%">
          {if $se->showtype == 1}
		    <tr>
		      <td colspan="3">
			    <div class="shadow_bottom b_bottom m_bottom {if !$smarty.foreach.se.first} b_left b_top b_right rounded_top m_top_20{/if} bc_lgrey p_top_10 p_left_10">
			      {if $se->type != ''}<p>{$se->type|sslash|escape:"html"}</p>{/if}
			      <p class="bold p_bottom">{$se->title|sslash|escape:"html"}</p>
			    </div>
		      </td>
		    </tr>
		  {/if}
		   {*<div class="p_all_10 m_left m_right">*}
		    <tr>
		      <td width="33%">
			  	<span class="m_left left p_all"> 
			  	  {$se->time_start|date_format:'%A'}{if $se->time_start != $se->time_end} - {$se->time_end|date_format:'%A'}{/if}
			  	</span>
			  </td>
			  <td width="33%">
			    <span class="m_left left p_all">
			      {if $se->time_start|date_format:"%m" == $se->time_end|date_format:"%m"}
				    {if $se->time_start != $se->time_end}
				  	  {$se->time_start|date_format:'%d.'} - {$se->time_end|date_format:'%d. %B %Y'}	 
				  	{else}
				  	  {$se->time_start|date_format:'%d. %B %Y'}
				  	{/if}
				  {else}
				    {$se->time_start|date_format:'%d. %B'} - {$se->time_end|date_format:'%d. %B %Y'}
				  {/if}
				</span>
			  </td>
			  <td width="33%">
			  	<span class="m_left left p_all">{$se->time|sslash}</span>
			  </td>
		    </tr>
		  </table>
		  {*</div>*}
		{/foreach}
		
		</div>
      {/foreach}
    {/if}
    <p>&nbsp;</p>
  {/foreach}  
</div>
{/strip}
{literal}
<script type="text/javascript">  
  function scrollTo(id) {
  	$.scrollTo($('#state'+id),800);
  }
</script>
{/literal}