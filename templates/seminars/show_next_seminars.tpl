{strip}
{if $seminars}
<div class="m_top_20 m_left_20 m_bottom_20 shadow rounded b_all bc_lgrey box275" style="margin-top:40px;">
  <div class="shadow_bottom b_bottom bc_lgrey p_all_10">
    <p class="bold m_left">Seminartermine</p>
  </div>
  <div class="bc_lgrey rounded_bottom">
  {foreach from=$seminars item=s name=s}
    <div class="p_all_10 m_left m_right {if !$smarty.foreach.s.last} b_bottom_dotted{/if}">
	  <p class="i">
	  	{if $s->time_start|date_format:"%m" == $s->time_end|date_format:"%m"}
	  	  {if $s->time_start != $s->time_end}
	  	    {$s->time_start|date_format:'%d.'} - {$s->time_end|date_format:'%d. %B'}	 
	  	  {else}
	  	    {$s->time_start|date_format:'%d. %B'}
	  	  {/if}
	  	{else}
	  	  {$s->time_start|date_format:'%d. %B'} - {$s->time_end|date_format:'%d. %B'}
	  	{/if}
	  </p>
      <p>
      	{if $s->tid < 8}
      	  <a href="{$domainname}index.php?p=page&amp;id=98#{if $s->tid == 3}existenzgruender{elseif $s->tid == 4}buchfuehrung{elseif $s->tid == 5}selbststaendigkeit{elseif $s->tid == 7}existenzgruender2{elseif $s->tid == 8}contolling{/if}">{$s->type|sslash|escape:"html"}</a></p>
        {else}
          {$s->type|sslash|escape:"html"}
        {/if}
      </p>
      <p class="fs_11">{$s->city|sslash|escape:"html"}</p>
    </div>
  {/foreach}
  </div>
</div>
{/if}
{/strip}
