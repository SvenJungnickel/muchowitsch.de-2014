{strip}
<h1>SUCHE</h1>

{if $error}
  <p class="bold m_top_20">{$error}</p>
{else}

  <p>Sie haben gesucht nach: <span class="bold">{$searchword|sslash|escape:"htmlall"}</span></p>
  <p>Anzahl Treffer: <span class="bold">{$hits}</span></p>
  <p>&nbsp;</p>
  
  {if $hits > 0}
  <div class="m_top_20 m_right_20 shadow rounded b_all bc_lgrey">
    <div class="shadow_bottom b_bottom bc_lgrey p_all_10">
      <p class="bold m_left">Suchergebnisse</p>
    </div>
    <div class="bc_lgrey rounded_bottom">
    {foreach from=$result item=res name=res}
      <div class="p_all_10 m_left m_right {if !$smarty.foreach.res.last} b_bottom_dotted{/if}">
        {if $res->isnews == 1}<p>Aktuelles - {$res->topic}</p>{/if}
	    <h2>
          <a href="{$domainname}index.php?p={if $res->isnews == 1}archive&amp;action=details&amp;aid={else}page&amp;id={/if}{$res->id}" style="color: #075;" title="{$res->title|sslash}">
            {$res->title|sslash|upper}
          </a>
        </h2>
 
        <p>{$res->content}</p>
        <p>&nbsp;</p>
      </div>
    {foreachelse}
      <p class="bold m_top_20">Keine Ergebnisse!</p>
    {/foreach}
    </div>
  </div>
  {/if}
  
{/if}

{/strip}