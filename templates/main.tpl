{strip}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de">
<head>
  {*<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />*}
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <title>www.muchowitsch.de &raquo; {dynamic}{$title}{/dynamic} - Andreas Muchowitsch Unternehmensberatung GmbH </title>
  <meta http-equiv="pragma" content="no-cache" />
  <meta http-equiv="imagetoolbar" content="no" />
  <meta name="robots" content="INDEX,FOLLOW" />
  <meta http-equiv="content-language" content="de" />
  <meta http-equiv="expires" content="0" />
  <meta name="publisher" content="muchowitsch.de" /> 
  <meta name="copyright" content="muchowitsch.de" />
  <meta name="description" content="Andreas Muchowitsch Unternehmensberatung GmbH - Ihr Ansprechpartner zum Thema Unternehmensberatung, Existenzgr&uuml;ndung und Nebenberufliche Selbstst&auml;ndigkeit" />
  <meta name="keywords" content="muchowitsch.de" />
  <meta name="abstract" content="muchowitsch.de" />
  <link rel="shortcut icon" type="image/x-icon" href="{$domainname}{$img_path}favicon.ico" />
  <link rel="stylesheet" type="text/css" href="{$domainname}templates/css/style.css" />
  <!--[if lt IE 8]><link rel="stylesheet" type="text/css" href="{$domainname}templates/css/ie.css" /><![endif]-->
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" {*async="true"*}></script>  
</head>
<body>

<div id="bar">&nbsp;</div>

<div id="header">
  {*<div class="{*box500*}{* left ta_right" style="margin-top: 50px;">
    <a href="{$domainname}index.php?p=imprint" class="m_left_20 fs_11" style="text-decoration:none">&raquo; Impressum</a>
    <a href="{$domainname}index.php?p=contact" class="m_left_20 fs_11" style="text-decoration:none">&raquo; Kontakt</a>
    <a href="{$domainname}index.php?p=archive" class="m_left_20 fs_11" style="text-decoration:none">&raquo; Archiv</a>    
  </div>*}
  
  <div class="left m_left_20" style="margin-top:40px;">
    <form action="{$domainname}index.php?p=search" method="post" id="searchform">
      <input type="text" name="search" id="search" value="{if $searchword}{$searchword}{else}Suchbegriff eingeben{/if}" onclick="if(this.value == 'Suchbegriff eingeben'){ldelim}this.value = ''{rdelim}" onfocus="if(this.value == 'Suchbegriff eingeben'){ldelim}this.value = ''{rdelim}" onselect="if(this.value == 'Suchbegriff eingeben'){ldelim}this.value = ''{rdelim}" onblur="if(this.value == ''){ldelim}this.value = 'Suchbegriff eingeben'{rdelim}" />
      {*<input type="submit" name="submit" value="Suchen" />*}
      <img src="{$domainname}{$img_path}icons/magnifier.png" alt="suchen" class="m_left_5 pointer" onclick="javascript:document.getElementById('searchform').submit();" />
    </form>
  </div>
  
  <div id="logo">
    <a href="{$domainname}"><img src="{$domainname}/templates/images/muchowitsch-logo.png" alt="Andreas Muchowitsch Unternehmensberatung GmbH" /></a>
  </div>
</div>

<div id="main">

  {navigation}

  
  <div id="content">
    {dynamic}{$content}{/dynamic}    
  
    {*<div id="feet"></div>
    <a href="{$domainname}index.php?p=imprint" class="m_left_20 fs_11" style="text-decoration:none">&raquo; Impressum</a>
    <a href="{$domainname}index.php?p=contact" class="m_left_20 fs_11" style="text-decoration:none">&raquo; Kontakt</a>
    <a href="{$domainname}index.php?p=archive" class="m_left_20 fs_11" style="text-decoration:none">&raquo; Archiv</a>*}
  </div>
  
  <div class="clear"></div>
</div>

{* Google Analytics Code *}
{literal}
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-20613982-1']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
{/literal}
</body>
</html>
{/strip}