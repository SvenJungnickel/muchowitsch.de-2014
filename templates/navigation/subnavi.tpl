{strip}
{if $subnavi->subnavi}
      <li class="sub nav_button">
        <a href="javascript:;" class="subnavi pointer"{*{if $subnavi->linked2} style="text-decoration: none"{/if}*}>
          {if $subnavi->linked2}
            <span class="tc_green bold">{$subnavi->title}</span>
          {else}
            {$subnavi->title}
          {/if}
        </a>
        <div class="subnavi_content{if !$subnavi->linked} dpl_none{/if}">                
          <ul>
          {foreach from=$subnavi->subnavi item=subnav name=subnav}
   			{if $subnav->subnavi}
              {subnavi id=$subnav->id}
            {else}
              <li>
                <a href="{$domainname}index.php?p=page&amp;id={$subnav->id}{*page={$subnav->link}*}"{if $subnav->linked} style="text-decoration: none"{/if}>
                  {if $subnav->linked}
                    <span class="tc_green bold">{$subnav->title}</span>
                  {else}
                    {$subnav->title}
                  {/if}
                </a>
              </li>
            {/if}
          {/foreach}
          </ul>
        </div>
      </li>
{else}
    <li class="nav_button">
      <a href="{$domainname}index.php?p=page&amp;id={$subnavi->id}{*page={$subnavi->link}*}" {if $subnavi->linked}style="text-decoration: none"{/if}>
        {if $subnavi->linked}
          <span class="tc_green bold">{$subnavi->title}</span>
        {else}
          {$subnavi->title}
        {/if}
      </a>
    </li>
{/if}
{/strip}