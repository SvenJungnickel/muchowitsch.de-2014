{strip}
<ul>
{foreach from=$navigation item=navi name=navi}
    {if $navi->subnavi}
        {subnavi id=$navi->id}
    {else}
        <li class="nav_button">
          <a href="{$domainname}{if $navi->id != 1}index.php?p=page&amp;id={$navi->id}{/if}" {*{if $navi->linked || $smarty.request.p == '' && $navi->id == 1}style="text-decoration: none"{/if}*}>
            {if $navi->linked || $smarty.request.p == '' && $navi->id == 1}
              <span class="tc_green bold">{$navi->title}</span>
            {else}
              {$navi->title}
            {/if}
          </a>
        </li>
    {/if}
{/foreach}
</ul>

{showNextSeminars limit=3}

<div id="subnavi" class="m_left_20 fs_11">
  <a href="{$domainname}index.php?p=imprint" class="{if $smarty.request.p == 'imprint'} bold{/if}">&raquo; Impressum</a><br />
  <a href="{$domainname}index.php?p=archive" class="{if $smarty.request.p == 'archive'} bold{/if}">&raquo; Archiv</a><br />
  <a href="{$domainname}admin/" target="_blank">&raquo; Admin</a><br />
  <a href="http://intranet.muchowitsch.de" target="_blank">&raquo; Intranet</a><br />
  
  <a href="http://www.xing.com/profile/Andreas_Muchowitsch" target="_blank" rel="me" title="Andreas Muchowitsch auf Xing">
    <img src="http://www.xing.com/img/buttons/10_en_btn.gif" width="85" height="23" alt="Xing Icon" class="m_top_15">
  </a>
  <a href="https://twitter.com/Muchowitsch" target="_blank" title="Andreas Muchowitsch Unternehmensberatung auf Twitter">
    <img src="{$domainname}{$img_path}twitter.png" width="23" height="23" alt="Twitter Icon" class="m_left_15">
  </a>
  <a href="https://plus.google.com/+MuchowitschDe" rel="publisher" target="_blank" title="Andreas Muchowitsch Unternehmensberatung auf Google+">
    <img src="{$domainname}{$img_path}googleplus.png" width="23" height="23" alt="Google Plus Icon" class="m_left_15">
  </a>
</div>


{/strip}
{literal}
<script type="text/javascript">
$(document).ready(function() {
  $('.subnavi').click(function(){
    $(this).parent('li').siblings('li').children('.subnavi_content').hide(0);
	$(this).siblings('.subnavi_content').toggle();
  });
});
</script>
{/literal}