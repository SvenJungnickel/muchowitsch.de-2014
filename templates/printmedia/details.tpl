{strip}
<link type="text/css" rel="stylesheet" href="{$domainname}{$js_path}jquery.fancybox/jquery.fancybox-1.3.1.css" />
<script type="text/javascript" src="{$domainname}{$js_path}jquery.fancybox/jquery.fancybox-1.3.1.js"></script>
<script type="text/javascript">
jQuery(function($) {ldelim} 
  $("a.img").fancybox({ldelim}
    'padding': 1,
    'overlayOpacity': 0.5,
    'hideOnContentClick': false,
    'autoScale': false
  {rdelim});
{rdelim});
</script>
<div class="m_bottom_15"><a href="http://www.muchowitsch.de/%C3%BCber_uns/6.htm">&Uuml;ber Uns</a>&nbsp;&raquo;&nbsp;<a href="http://www.muchowitsch.de/%C3%BCber_uns/presse_und_medien/93.htm">Presse und Medien</a>&nbsp;&raquo;&nbsp;<span>{$row->title|sslash}</span></div>

<h1>{$row->title|sslash|upper}</h1>
<small>{$row->date|date_format:"%d. %B %Y"}{if $row->newspaper != ''}, {$row->newspaper|sslash}{/if}{if $row->newspage != ''}, Seite {$row->newspage|sslash}{/if}</small>
{if $row->link != ''}<br /><small>Quelle: <a href="{$row->link|sslash}" target="_blank">{$row->shortlink|sslash}</a></small>{/if}

{if $row->image != '' || $row->descr != '' && $row->descr|count_characters > 20}
  <p class="m_top_20">
    {if $row->image != ''}
      <a href="{$domainname}{$upload_path}printmedia/big/{$row->image}" class="img left m_right m_bottom_5" rel="group"><img src="{$domainname}{$upload_path}printmedia/thumb/{$row->image}" alt="{$row->title|sslash|escape:'html'}" /></a>
    {/if}
    {$row->descr|sslash}
    <div class="clear"></div>
  </p>
{/if}

{if $pics}
  <p class="m_top">
    {foreach from=$pics item=pic name=pic}
      <a class="img" rel="group" href="{$domainname}{$upload_path}printmedia/{$pic->path}">
        <img {*style="max-width:190px; max-height:140px"*} src="{$domainname}{$upload_path}printmedia/{$pic->thumb}" class="m_top m_right" alt="Bild {$pic->number}" border="0">
      </a>
    {/foreach}
  </p>
{/if}

{/strip}