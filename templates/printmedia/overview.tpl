{strip}
<div class="m_top_20 m_right_20 shadow rounded b_all bc_lgrey">
{foreach from=$printmedia item=p name=p}
  <div class="p_all_10 m_left m_right">
	<p class="bold"><a href="{$domainname}index.php?p=printmedia&amp;id={$smarty.request.id}&amp;pid={$p->id}">{$p->title|sslash}</a></p>
    <p>{$p->date|date_format:"%d.%B %Y"}{if $p->newspaper != ''}, {$p->newspaper}{/if}{if $p->newspage != ''}, Seite {$p->newspage}{/if}{if $p->link != ''}, <a href="{$p->link|sslash}" target="_blank">{$p->shortlink|sslash}</a>{/if}</p>
  </div>
{foreachelse}
  <h2 class="m_top p_left_10 m_left">Keine Eintr&auml;ge vorhanden</h2>
{/foreach}
</div>
{if $navi}<div class="m_top_20 m_left_10m">{$navi}</div>{/if}
{/strip}
