{strip}
<h1>ARCHIV</h1>

<p>Im Artikel-Archiv finden Sie alle auf muchowitsch.de publizierten Artikel in ihrer chronologischen Erscheinungsweise.</p>
<p>&nbsp;</p>
<p>Die im Artikel aufgef&uuml;hrte Datumsangabe gibt den Monat und das Jahr der Ver&ouml;ffentlichung wieder. </p>
<p>&nbsp;</p>
<p>Autoren der Artikel sind:</p>
<p><strong>A.M.</strong> - Andreas Muchowitsch, Unternehmensberater, Dresden</p>
<p><strong>A.R.</strong> - Anika Richter, Steuerberaterin, Zschopau</p>
<p><strong>K.D.</strong> - Dr. Kerstin D&auml;lken, Rechtsanw&auml;ltin, Lingen</p>
<p><strong>F.D.</strong> - Florian D&auml;lken, Rechtsanwalt, Lingen</p>
<p>&nbsp;</p>

<div class="m_top_20 m_right_20 shadow rounded b_all bc_lgrey">
  <div class="shadow_bottom b_bottom bc_lgrey p_all_10">
    <p class="bold m_left">Aktuelles</p>
  </div>
  <div class="bc_lgrey rounded_bottom">
  {foreach from=$news item=n name=n}
    <a name="artikel{$n->id}"></a>
    <div class="p_all_10 m_left m_right {if !$smarty.foreach.n.last} b_bottom_dotted{/if}">
	  <p>{$n->topic|sslash}</p>
      <p>{$n->ctime|date_format:"%m/%Y"} - {$n->author}</p>
    
      <h2>
        <a href="{$domainname}index.php?p=archive&amp;action=details&amp;aid={$n->id}" style="text-decoration: none; color: #075;" title="{$n->title|sslash}">
          {$n->title|sslash|upper}
        </a>
      </h2>
      
      <p>{$n->teaser|sslash}</p>
      <p><a href="{$domainname}index.php?p=archive&amp;action=details&amp;aid={$n->id}" title="mehr zu {$n->title|sslash}">mehr</a>...</p>
      <p>&nbsp;</p>
    </div>
  {/foreach}
  </div>
</div>
{if $navi}<div class="m_top_20 m_left_10m">{$navi}</div>{/if}
{/strip}