{strip}
    <div class="m_bottom_15">
        <a href="{$domainname}index.php?p=archive">Archiv</a>&nbsp;&raquo;&nbsp;{$row->title|sslash}
    </div>

    <div itemscope itemtype="http://schema.org/CreativeWork">
        <p><span itemprop="genre">{$row->topic|sslash}</span></p>
        <p><span itemprop="datePublished">{$row->ctime|date_format:"%d.%m.%Y"}</span> - <span itemprop="author">{$row->author}</span></p>

        <h1><span itemprop="headline">{$row->title|sslash|upper}</span></h1>

        <div itemprop="text">
            <p>{$row->teaser|sslash}</p>
            <p>&nbsp;</p>
            <p>{$row->text|sslash}</p>
            <p>&nbsp;</p>
        </div>
    </div>
{/strip}