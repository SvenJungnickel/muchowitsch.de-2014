{strip}
<h1>Kontakt</h1>

<div class="p_all m_bottom_15">
    <p class="box150 left">Telefon Dresden:</p>
    <p class="left">+49 (0) 3 51 / 4 82 46 29</p>
    <div class="clear"></div>

    <p class="box150 left">Telefon Meppen:</p>
    <p class="left">+49 (0) 59 31 / 5 90 20 41</p>
    <div class="clear"></div>
</div>

<form name="form" action="" method="post" enctype="multipart/form-data">
<input type="hidden" name="send" value="1" />

  {if $error}
    <p class="m_bottom_15 tc_red">Bitte &uuml;berpr&uuml;fen Sie Ihre Eingaben!</p>
  {/if}
  {if $success == 1}
    <p class="p_all m_bottom">Vielen Dank f&uuml;r Ihre E-Mail. Wir werden uns in K&uuml;rze um Ihr Anliegen k&uuml;mmern.</p>
    <p class="p_all"><a href="{$domainname}" class="bold">Zur&uuml;ck zur Startseite</a></p>
  {else}
  
    <div class="p_all">
      <label for="name" class="box150 left pointer{if $error.name == 1} tc_red{/if}">Vor- und Nachname</label>
      <input type="text" class="left{if $error.name == 1} bc_red{/if}" name="name" id="name" value="{$smarty.request.name}" size="50" maxlength="255" tabindex="1" />
      {if $error.name == 1}
        <img src="{$domainname}{$img_path}icons/exclamation.png" alt="exclamation" class="m_left_5 pointer" title="Bitte den vollst&auml;ndigen Namen eingeben!" />
      {/if}
      <div class="clear"></div>
    </div>
  
    <div class="p_all">
      <label for="email" class="box150 left pointer{if $error.email == 1} tc_red{/if}">E-Mail-Adresse</label>
      <input type="text" name="email" class="left{if $error.email == 1} bc_red{/if}" id="email" value="{$smarty.request.email}" size="50" maxlength="255" tabindex="2" />
      {if $error.email == 1}
        <img src="{$domainname}{$img_path}icons/exclamation.png" alt="exclamation" class="m_left_5 pointer" title="Bitte die EMail-Adresse eingeben!" />
      {/if}
      <div class="clear"></div>
    </div>
  
    <div class="p_all">
      <label for="subject" class="box150 left pointer{if $error.subject == 1} tc_red{/if}">Thema</label>
      <input type="text" name="subject" class="left{if $error.subject == 1} bc_red{/if}" id="subject" value="{$smarty.request.subject}" size="50" maxlength="255" tabindex="3" />
      {*<select name="subject" class="left{if $error.subject == 1} bc_red{/if}" id="subject" tabindex="3">
        <option value=""{if $error != '' && $smarty.request.subject == ''} selected="selected"{/if}></option>
        <option value="Anlage EKS"{if ($error != '' || $smarty.get.subject != '') && $smarty.request.subject == 'Anlage EKS'} selected="selected"{/if}>Anlage EKS</option>
        <option value="Arbeitsvermittlung"{if ($error != '' || $smarty.get.subject != '') && $smarty.request.subject == 'Arbeitsvermittlung'} selected="selected"{/if}>Arbeitsvermittlung</option>
        <option value="Coaching"{if ($error != '' || $smarty.get.subject != '') && $smarty.request.subject == 'Coaching'} selected="selected"{/if}>Coaching</option>
        <option value="Controlling"{if ($error != '' || $smarty.get.subject != '') && $smarty.request.subject == 'Controlling'} selected="selected"{/if}>Controlling</option>
        <option value="Existenzgr�ndung"{if ($error != '' || $smarty.get.subject != '') && $smarty.request.subject == 'Existenzgr�ndung'} selected="selected"{/if}>Existenzgr�ndung</option>
        <option value="Informationen"{if ($error != '' || $smarty.get.subject != '') && $smarty.request.subject == 'Informationen'} selected="selected"{/if}>Informationen</option>
        <option value="IT-L�sungen"{if ($error != '' || $smarty.get.subject != '') && $smarty.request.subject == 'IT-L�sungen'} selected="selected"{/if}>IT-L�sungen</option>
        <option value="Krisenmanagement"{if ($error != '' || $smarty.get.subject != '') && $smarty.request.subject == 'Krisenmanagement'} selected="selected"{/if}>Krisenmanagement</option>
        <option value="Nebenberufliche Selbst�ndigkeit"{if ($error != '' || $smarty.get.subject != '') && $smarty.request.subject == 'Nebenberufliche Selbst�ndigkeit'} selected="selected"{/if}>Nebenberufliche Selbst�ndigkeit</option>
        <option value="Selbst�ndigkeit von Jugendlichen"{if ($error != '' || $smarty.get.subject != '') && $smarty.request.subject == 'Selbst�ndigkeit von Jugendlichen'} selected="selected"{/if}>Selbst�ndigkeit von Jugendlichen</option>
        <option value="Seminare"{if ($error != '' || $smarty.get.subject != '') && $smarty.request.subject == 'Seminare'} selected="selected"{/if}>Seminare</option>
        <option value="Stellungnahmen"{if ($error != '' || $smarty.get.subject != '') && $smarty.request.subject == 'Stellungnahmen'} selected="selected"{/if}>Stellungnahmen</option>
        <option value="Webentwicklung"{if ($error != '' || $smarty.get.subject != '') && $smarty.request.subject == 'Webentwicklung'} selected="selected"{/if}>Webentwicklung</option>
        <option value="Sonstiges"{if ($error != '' || $smarty.get.subject != '') && $smarty.request.subject == 'Sonstiges'} selected="selected"{/if}>Sonstiges</option>        
      </select>*}
      {if $error.subject == 1}
        <img src="{$domainname}{$img_path}icons/exclamation.png" alt="exclamation" class="m_left_5 pointer" title="Bitte ein Thema ausw&auml;hlen!" />
      {/if}
      <div class="clear"></div>
    </div>
  
    <div class="p_all">
      <label for="text" class="box150 left pointer{if $error.text == 1} tc_red{/if}">Nachricht</label>
      <textarea name="text" id="text" class="left {if $error.text == 1} bc_red{/if}" rows="8" cols="38" tabindex="4">{$smarty.request.text}</textarea>
      {if $error.text == 1}
        <img src="{$domainname}{$img_path}icons/exclamation.png" alt="exclamation" class="m_left_5 pointer" title="Bitte eine Nachricht eingeben!" />
      {/if}
      <div class="clear"></div>
    </div>

    <div class="p_all">
      <label class="box150 left pointer{if $error.secure == 1} tc_red{/if}">Sicherheitspr&uuml;fung</label>
      <div class="left {if $error.secure == 1} bc_red{/if}">{$captcha}</div>
      <div class="g-recaptcha left{if $error.text == 1} b_all bc_red{/if}" data-sitekey="{$captchaKey}"></div>
      {if $error.secure == 1}
        <img src="{$domainname}{$img_path}icons/exclamation.png" alt="exclamation" class="m_left_5 pointer" title="Sicherheitspr&uuml;fung nicht bestanden!" />
      {/if}
      <div class="clear"></div>
    </div>
  
    <button type="submit" name="submit" value="Senden" class="m_top m_bottom" tabindex="6">Nachricht senden</button>

  {/if}
</form>
{/strip}