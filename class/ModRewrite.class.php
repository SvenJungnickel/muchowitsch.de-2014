<?
/*
 * Klasse zum umschreiben von Links per ModRewrite
 *
 * by Sven Jungnickel
 */

if(isset($prepage)) {
	
	// Impressum
	$prepage = preg_replace("/index.php([?])p=imprint/", "impressum.htm", $prepage);
	
	// Kontaktformular
	$prepage = preg_replace("/index.php([?])p=contact&amp;subject=(.*)/", "kontakt/\\2", $prepage);
	$prepage = preg_replace("/index.php([?])p=contact/", "kontakt.htm", $prepage);
	
	// Archiv
    $prepage = preg_replace("/index.php([?])p=archive&amp;action=details&amp;aid=([0-9]*)/e", "'archiv/'.getPageTitle(\\2,'news').'-\\2.htm'", $prepage);
	$prepage = preg_replace("/index.php([?])p=archive&amp;action=overview&amp;pp=([0-9]*)&amp;page=([0-9]*)/", "archiv/seite\\3-\\2.htm", $prepage);
	$prepage = preg_replace("/index.php([?])p=archive/", "archiv/", $prepage);

	// Suche
    $prepage = preg_replace("/index.php([?])p=search&amp;search=(.*)/", "suche/\\2", $prepage);

    // Glossar
	$prepage = preg_replace("/index.php([?])p=page&amp;id=([0-9]*)&amp;pp=([0-9]*)&amp;page=([0-9]*)&amp;sort=([a-�A-�0-9]*)/e", "getPageTitle(\\2).'\\2-Seite\\4-\\3-\\5.htm'", $prepage);
	$prepage = preg_replace("/index.php([?])p=page&amp;id=([0-9]*)&amp;sort=([a-�A-�0-9]*)/e", "getPageTitle(\\2).'\\2-\\3.htm'", $prepage);
	
	// Presse und Medien
	$prepage = preg_replace("/index.php([?])p=printmedia&amp;id=([0-9]*)&amp;pid=([0-9]*)&amp;pp=([0-9]*)&amp;page=([0-9]*)&amp;sort=([a-�A-�0-9]*)/e", "getPageTitle(\\2).getPageTitle(\\3,'printmedia').'/\\2-\\3-Seite\\5-\\4.htm'", $prepage);
	$prepage = preg_replace("/index.php([?])p=printmedia&amp;id=([0-9]*)&amp;pid=([0-9]*)/e", "getPageTitle(\\2).getPageTitle(\\3,'printmedia').'/\\2-\\3.htm'", $prepage);

	// Inhalte
    $prepage = preg_replace("/index.php([?])p=page&amp;id=([0-9]*)/e", "getPageTitle(\\2).'\\2.htm'", $prepage);
}
?>