<?
/* 
 * Datenbank Klasse zum Verwalten der Datenbank-Abfragen
 *
 * by Sven Jungickel
 */

/*
 * Klasse zum Verwalten der Datenbankergebnisse
 */
class Result {		
	/* Ergebnis */
	public static $result = NULL;		/* public */
	
	/*
	 * Konstruktor
	 */
	function Result(&$result) {
		$this->result = $result;
	}
	
	/*
	 * Ergebnisse als Object zurückgeben
	 */
	public function FetchRow() {
		#return @mysql_fetch_assoc($this->result);
		return @mysql_fetch_object($this->result);
	}
	
	/*
	 * Ergebnisse als Object zurückgeben
	 */
	public function FetchObject() {
		return @mysql_fetch_object($this->result);		
	}
		
	/*
	 * Anzahl Ergebnisse ermitteln
	 */
	public function NumRows() {
		return @mysql_num_rows($this->result);
	}
}
 
/*
 * Klasse zum Verwalten der Datenbankverbindung und -Anfragen
 * Es kann nur eine Instanz erstellt werden
 */
class DB {
	
	/* Instanz des Objektes */
	private static $instance = NULL;	/* static, boolean */
	
	/* Verbindungshandler & -Variablen */
	private static $handle = false;		/* private */
	private $host = '';					/* private, string */
	private $user = '';					/* private, string */
	private $pass = '';					/* private, string */
	private $db = '';					/* private, string */
	
	/* Statistik-Variablen */
	private $total_number_of_queries = 0; /* private, integer */
	private $total_query_time = 0; 		/* private; integer */

	/*
	 * Konstruktor private, damit die Klasse nur aus sich selbst heraus instanziiert werden kann.
	 */
	private function __construct() {}	
	
	/*
	 * Klonen per 'clone()' von außen verbieten.
	 */
	private function __clone() {}
	
	/* 
	 * Überprüft, ob bereits eine Instanz erstellt wurde.
	 * Wenn nicht, dann wird eine neue erstellt.
	 * Wenn doch, wird diese zurück gegeben.
	 */
	public static function getInstance() {
		
		if(self::$instance === NULL) {
			self::$instance = new self;
		}
		return self::$instance;
	}
	
	/*
	 * Verbindungsdaten setzen
	 */
	public function setConnectionData($host, $user, $pass, $db = '') {
		
		if(!$host || !$user) return false;
		else {
			$this->host = $host;
			$this->user = $user;
			$this->pass = $pass;
			if($db != '') $this->db = $db;
			
			return true;
		}
	}
	
	/*
	 * Datenbank auswählen
	 */
	public function selectDb($db) {
	
		if(!$db) return false;
		else {
			$this->Close();
			$this->db = $db;
			return true;
		}
	} 
	
	/*
	 * Verbindung zur Datenbank erstellen
	 */
	private function Connect() {
		if(!$this->handle) {
			$this->handle = @mysql_connect($this->host, $this->user, $this->pass);
			if(!$this->handle) $this->Error();
			
			if($this->db != '') {
				if(!@mysql_select_db($this->db, $this->handle)) $this->Error();
			}
		}
		
		return true;
	}
	
	/*
	 * Fehlerausgabe
	 */
	private function Error($query = '') {
		echo @mysql_errno($this->handle).': '.@mysql_error($this->handle).'<br />';
		if($query != '') echo 'Abfrage: <b>'.$query.'</b><br />';
	}
	
	/*
	 * Abfrage ausführen
	 */
	public function Query($query) {
		
		if(!$query) return false;
		else {
			if(!$this->handle) {
				$this->Connect();
			}
			
			$start = microtime(true);
			
			#if(isset($_SESSION['uid']) && $_SESSION['uid'] != '' && $_SESSION['uid'] != 0 && $_SESSION['ugroup'] == 1) echo $query;
			$result = @mysql_query($query, $this->handle);
						
			$end = microtime(true);
			
			if(!$result) {
				$this->Error($query);
			}
			
			$this->total_query_time += $end - $start;
			$this->total_number_of_queries++;
			
			return new Result($result);
		}
	}
		
	/*
	 * letzte eingetragene ID ermitteln
	 */
	public function InsertId() {
		return @mysql_insert_id($this->handle);
	}
	
	/*
	 * Datenbankverbindung wieder schließen
	 */	
	public function Close() {
		$this->handle = '';
		@mysql_close($this->handle);
	}
	
	/*
	 * statistischen Werte der Abfragen (Anzahl und Dauer) zurückgeben
	 */	
	public function getStatistic() {	
		return array( 'total_query_time' => $this->total_query_time, 'total_number_of_queries' => $this->total_number_of_queries);
	}
}
?>